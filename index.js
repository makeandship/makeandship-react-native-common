/* @flow */

import { AppRegistry, YellowBox } from 'react-native';

import { initCurrentLocale } from './src/utils/date';
initCurrentLocale();

console.log('window.navigator.language', window.navigator.language);

YellowBox.ignoreWarnings([
  'Required dispatch_sync to load constants',
  'RCTBridge required dispatch_sync',
  'currentlyFocusedField is deprecated',
]);

console.log('process.env.NODE_ENV', process.env.NODE_ENV);
console.log('process.env.REACT_APP_API_URL', process.env.REACT_APP_API_URL);
console.log(
  'process.env.REACT_APP_API_SPEC_URL',
  process.env.REACT_APP_API_SPEC_URL
);
console.log(
  'process.env.REACT_APP_KEYCLOAK_URL',
  process.env.REACT_APP_KEYCLOAK_URL
);
console.log(
  'process.env.REACT_APP_KEYCLOAK_REALM',
  process.env.REACT_APP_KEYCLOAK_REALM
);
console.log(
  'process.env.REACT_APP_KEYCLOAK_CLIENT_ID',
  process.env.REACT_APP_KEYCLOAK_CLIENT_ID
);
console.log(
  'process.env.REACT_APP_KEYCLOAK_IDP',
  process.env.REACT_APP_KEYCLOAK_IDP
);
console.log(
  'process.env.REACT_APP_REDIRECT_URI',
  process.env.REACT_APP_REDIRECT_URI
);

const App = __DEV__
  ? require('./example-app-client/App.dev').default
  : require('./example-app-client/App').default;

AppRegistry.registerComponent('ExampleApp', () => App);
