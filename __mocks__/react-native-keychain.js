const keychainMock = {
  SECURITY_LEVEL_ANY: 'MOCK_SECURITY_LEVEL_ANY',
  SECURITY_LEVEL_SECURE_SOFTWARE: 'MOCK_SECURITY_LEVEL_SECURE_SOFTWARE',
  SECURITY_LEVEL_SECURE_HARDWARE: 'MOCK_SECURITY_LEVEL_SECURE_HARDWARE',
  setGenericPassword: jest.fn().mockResolvedValue(),
  getGenericPassword: jest.fn().mockResolvedValue(),
  resetGenericPassword: jest.fn().mockResolvedValue(),
  hasInternetCredentials: () => true,
  setInternetCredentials: jest.fn().mockResolvedValue(),
  getInternetCredentials: jest.fn().mockResolvedValue(),
  resetInternetCredentials: jest.fn().mockResolvedValue(),
};

module.exports = keychainMock;
