# React Native Common

## Setup

### Guide

- Install the app dependencies

  ```
  yarn
  gem install cocoapods
  cd ios && pod install && cd ..
  ```

### Running the example app

Quickstart instructions below. For further guidance, see the [React Native docs](http://facebook.github.io/react-native/docs/running-on-device.html)

#### iOS

- To start the iOS app locally in the simulator on macOS:

  ```
  $ npx react-native run-ios
  ```

- To specify a simulator:

  ```
  $ npx react-native run-ios --simulator="iPhone X"
  $ npx react-native run-ios --simulator="iPhone SE"
  ```

- To start the iOS app on a real device:

  ```
  $ npx react-native run-ios --device "Your Device Name"
  ```

- To run production build on a real device:

  ```
  $ npx react-native run-ios --configuration Release --device "Your Device Name"
  ```

#### Android

- To start the app:

  ```
  $ npx react-native run-android
  ```

### Storybook

The dev app has an additional option 'Toggle Storybook' in the React Native developer menu. You will need to run the Storybook server in order for this to work;

```
$ yarn storybook
```
