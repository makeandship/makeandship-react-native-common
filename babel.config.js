// `transform-remove-console` exclude options not working on React Native project
// https://github.com/babel/minify/issues/950
module.exports = (api) => {
  const envProduction = api.env('production');
  api.cache(true);
  const config = {
    presets: ['module:metro-react-native-babel-preset'],
    plugins: ['inline-dotenv', '@babel/proposal-optional-chaining'],
  };
  if (envProduction) {
    config.plugins.push('transform-remove-console');
  }
  return config;
};
