/* @flow */

import * as React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { connect } from 'react-redux';

import Button from '../../../src/components/ui/Button';

import {
  requestCurrentPosition,
  startWatchingPosition,
  stopWatchingPosition,
  getLatitude,
  getLongitude,
  getWatching,
  getIsRequesting,
  getError,
  getHasError,
  getHasPosition,
} from '../../../src/modules/geolocation/geolocation';
import {
  addCircularRegion,
  removeRegion,
  getAllRegions,
  getIsInsideAnyRegion,
} from '../../../src/modules/geolocation/regions';

class Geolocation extends React.Component<*> {
  onAddRegion = () => {
    const { addCircularRegion } = this.props;
    addCircularRegion(
      'studio',
      { latitude: 51.53390794004831, longitude: -0.12905742064501227 },
      30
    );
    addCircularRegion(
      'home',
      { latitude: 51.5586819565814, longitude: -0.0717511374215681 },
      30
    );
  };

  onRemoveRegion = () => {
    const { removeRegion } = this.props;
    removeRegion('studio');
    removeRegion('home');
  };

  render() {
    const {
      latitude,
      longitude,
      watching,
      allRegions,
      isInsideAnyRegion,
      requestCurrentPosition,
      startWatchingPosition,
      stopWatchingPosition,
      isRequesting,
      hasError,
      hasPosition,
    } = this.props;
    return (
      <View style={styles.container}>
        <Text>Is requesting location: {isRequesting ? 'Yes' : 'No'}</Text>
        <Text>Has position: {hasPosition ? 'Yes' : 'No'}</Text>
        <Text>Has error: {hasError ? 'Yes' : 'No'}</Text>
        <Text>Latitude: {latitude}</Text>
        <Text>Longitude: {longitude}</Text>
        <Text>Regions: {JSON.stringify(allRegions, null, 2)}</Text>
        <Text>Inside any region: {isInsideAnyRegion ? 'Yes' : 'No'}</Text>
        <View style={styles.row}>
          <Button
            label={watching ? 'Stop watching' : 'Start watching'}
            onPress={watching ? stopWatchingPosition : startWatchingPosition}
          />
          <Button label={'Get position'} onPress={requestCurrentPosition} />
        </View>
        <View style={styles.row}>
          <Button label={'Add region'} onPress={this.onAddRegion} />
          <Button label={'Remove region'} onPress={this.onRemoveRegion} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {},
  row: {
    flexDirection: 'row',
  },
});

const withRedux = connect(
  (state) => ({
    latitude: getLatitude(state),
    longitude: getLongitude(state),
    watching: getWatching(state),
    isInsideAnyRegion: getIsInsideAnyRegion(state),
    allRegions: getAllRegions(state),
    isRequesting: getIsRequesting(state),
    error: getError(state),
    hasError: getHasError(state),
    hasPosition: getHasPosition(state),
  }),
  {
    requestCurrentPosition,
    startWatchingPosition,
    stopWatchingPosition,
    addCircularRegion,
    removeRegion,
  }
);

export default withRedux(Geolocation);
