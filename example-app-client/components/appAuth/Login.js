/* @flow */

import * as React from 'react';
import { View } from 'react-native';

import { getConfig as getAuthConfig } from 'makeandship-js-common/src/modules/auth/config';
import { JsonSchemaFormWithValidation } from '../../../src/components/ui/form';

import loginSchema from '../../schemas/login';

import LoadingOverlay from '../ui/LoadingOverlay';

const loginUiSchema = {
  username: {
    'ui:placeholder': 'sam.smith@example.com',
    'ui:autofocus': true,
  },
  password: {
    'ui:widget': 'password',
    'ui:returnkeytype': 'go',
  },
};

const Login = (): React.Element<*> => {
  const authConfig = getAuthConfig();
  const formRef = React.useRef(null);
  const onSubmitEditing = React.useCallback(() => {
    formRef?.current?.submit();
  });
  const [isFetching, setIsFetching] = React.useState(false);
  const [error, setError] = React.useState();
  // const { isFetching, onSubmit, error } = props;
  // TODO: cleaner way to hook up submit action to form component
  loginUiSchema.password['ui:widgetadditionalprops'] = {
    onSubmitEditing,
  };
  const onSubmit = React.useCallback(async (formData) => {
    setError();
    setIsFetching(true);
    try {
      await authConfig.provider.login(formData);
    } catch (e) {
      setError(e);
    }
    setIsFetching(false);
  });
  return (
    <View style={{ flex: 1 }}>
      <JsonSchemaFormWithValidation
        ref={formRef}
        schema={loginSchema}
        uiSchema={loginUiSchema}
        onSubmit={onSubmit}
        isFetching={isFetching}
        error={error}
        keyboardShouldPersistTaps={'always'}
      />
      {isFetching && <LoadingOverlay />}
    </View>
  );
};
export default Login;

// Login.propTypes = {
//   isFetching: PropTypes.bool.isRequired,
//   onSubmit: PropTypes.func.isRequired,
//   error: PropTypes.any,
// };

// const withRedux = connect(
//   state => ({
//     isFetching: getIsFetching(state),
//     error: getError(state),
//   }),
//   dispatch => ({
//     onSubmit(formData) {
//       dispatch(login(formData));
//     },
//   })
// );

// export default withRedux(Login);
