/* @flow */

import * as React from 'react';
import PropTypes from 'prop-types';
import { View, Text, ActivityIndicator, StyleSheet } from 'react-native';
import Theme from '../../theme/Theme';

class LoadingOverlay extends React.PureComponent<*> {
  render() {
    const { title, style } = this.props;
    return (
      <View style={[styles.container, style]}>
        <ActivityIndicator size="large" />
        {title && <Text style={styles.title}>{title}</Text>}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    backgroundColor: 'rgba(244,244,244,0.8)',
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    paddingTop: 12,
    fontSize: Theme.fontSize,
    color: Theme.colorGreyDark,
  },
});

LoadingOverlay.propTypes = {
  style: PropTypes.any,
  title: PropTypes.string,
};

export default LoadingOverlay;
