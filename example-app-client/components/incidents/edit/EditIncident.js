/* @flow */

import * as React from 'react';
import { View, StyleSheet } from 'react-native';
import { connect } from 'react-redux';

import withFormModule from 'makeandship-js-common/src/hoc/withFormModule';
import {
  setFormData,
  makeGetFormData,
} from 'makeandship-js-common/src/modules/form';

import {
  setIncident,
  getIncident,
  deleteIncident,
  requestIncident,
  createIncident,
  updateIncident,
  incidentModule,
} from '../../../modules/incidents/incident';

import {
  JsonSchemaForm,
  SectionTitle,
} from '../../../../src/components/ui/form';

import {
  FileInputBinaryCell,
  PhotoInputBinaryCell,
  ButtonCell,
  SelectCell,
} from '../../../../src/components/ui/form/cells';

const ConnectedJsonSchemaForm = withFormModule(JsonSchemaForm);

const INCIDENT_FORM_ID = 'example/example';

import incidentSchema from '../../../schemas/incidentSchema';

export const incidentUiSchema = {
  status: {
    'ui:widget': SelectCell,
  },
  attachment: {
    'ui:widget': PhotoInputBinaryCell,
  },
  attachments: {
    items: {
      'ui:widget': FileInputBinaryCell,
      'ui:options': {
        addTitle: 'Add attchment',
        removeTitle: 'Remove attachment',
      },
    },
  },
};

const incident = {
  id: '5cd171754c58e20010d56df3',
};

const getIncidentFormData = makeGetFormData(INCIDENT_FORM_ID);

class EditIncident extends React.Component<*> {
  componentDidMount = () => {
    const { requestIncident, navigation } = this.props;
    requestIncident(incident.id);
    navigation.setOptions({
      title: 'Edit incident',
    });
  };

  onChange = (formData) => {
    console.log('onChange', formData);
  };

  onSubmit = () => {
    const { updateIncident, formData } = this.props;
    updateIncident(formData);
  };

  footerCells = () => {
    return [
      <SectionTitle key={'spacer'} title={' '} />,
      <ButtonCell
        key={'submit-button'}
        onPress={this.onSubmit}
        title={'Update incident'}
      />,
    ];
  };

  render() {
    const { incident } = this.props;
    return (
      <View style={styles.container}>
        <ConnectedJsonSchemaForm
          schema={incidentSchema}
          uiSchema={incidentUiSchema}
          formKey={INCIDENT_FORM_ID}
          footerCells={this.footerCells()}
          formData={incident}
          onChange={this.onChange}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

const {
  getIsFetching,
  getIsCreating,
  getIsUpdating,
  getIsDeleting,
  getError,
} = incidentModule.selectors;

const withRedux = connect(
  (state) => {
    const incident = getIncident(state);
    const isFetching = getIsFetching(state);
    const isCreating = getIsCreating(state);
    const isUpdating = getIsUpdating(state);
    const isDeleting = getIsDeleting(state);
    const error = getError(state);
    const formData = getIncidentFormData(state);
    return {
      isFetching,
      isCreating,
      isUpdating,
      isDeleting,
      error,
      incident,
      formData,
    };
  },
  {
    setFormData,
    setIncident,
    deleteIncident,
    requestIncident,
    createIncident,
    updateIncident,
  }
);

export default withRedux(EditIncident);
