/* @flow */

import * as React from 'react';
import { View, StyleSheet } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';

import {
  setTopLevelNavigatorRef,
  onNavigationStateChange,
} from '../../../src/modules/ui/reactNavigation';
import NotificationsContainer from '../../../src/components/notifications/NotificationsContainer';
import DateTimePickerContainer from '../../../src/components/dateTimePicker/DateTimePickerContainer';
import useTheme from '../../../src/theme/useTheme';

import RootStack from '../../navigators/RootStack';

const Root = (): React.Element<*> => {
  const theme = useTheme();
  return (
    <View style={styles.container}>
      <NavigationContainer
        theme={theme}
        ref={setTopLevelNavigatorRef}
        onStateChange={onNavigationStateChange}
      >
        <RootStack />
      </NavigationContainer>
      <DateTimePickerContainer />
      <NotificationsContainer />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default Root;
