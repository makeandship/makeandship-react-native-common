/* @flow */

import * as React from 'react';
import { View, StyleSheet } from 'react-native';
import { connect } from 'react-redux';

import Button from '../../../src/components/ui/Button';

import {
  showActionSheet,
  ACTION_SHEET_CANCEL,
} from '../../../src/modules/ui/actionSheet';
import { showAlert, ALERT_CANCEL } from '../../../src/modules/ui/alert';

class Modules extends React.Component<*> {
  showActionSheet1 = () => {
    const { showActionSheet } = this.props;
    showActionSheet('Please help us improve the app', [
      {
        type: 'EMAIL_ACTION',
        text: 'Email data to team',
      },
      {
        text: 'Not now',
        style: 'cancel',
        type: ACTION_SHEET_CANCEL,
      },
    ]);
  };

  showAlert1 = () => {
    const { showAlert } = this.props;
    showAlert('Help us', 'Please help us improve the app', [
      {
        type: 'EMAIL_ACTION',
        text: 'Email data to team',
      },
      {
        text: 'Not now',
        style: 'cancel',
        type: ALERT_CANCEL,
      },
    ]);
  };

  render() {
    return (
      <View style={styles.container}>
        <Button label={'Action Sheet 1'} onPress={this.showActionSheet1} />
        <Button label={'Alert 1'} onPress={this.showAlert1} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {},
});

const withRedux = connect(null, {
  showActionSheet,
  showAlert,
});

export default withRedux(Modules);
