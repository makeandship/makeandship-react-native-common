/* @flow */

import * as React from 'react';
import { Provider } from 'react-redux';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import 'react-native-gesture-handler';

import ThemeProvider from '../src/theme/ThemeProvider';

import store from './store';
import Root from './components/root/Root';
import Theme from './theme/Theme';

class App extends React.Component<*> {
  render() {
    return (
      <ThemeProvider value={Theme}>
        <Provider store={store}>
          <SafeAreaProvider>
            <Root />
          </SafeAreaProvider>
        </Provider>
      </ThemeProvider>
    );
  }
}
export default App;
