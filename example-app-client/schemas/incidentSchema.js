const incidentSchema = {
  type: 'object',
  $schema: 'http://json-schema.org/draft-07/schema#',
  properties: {
    id: {
      type: 'string',
    },
    status: {
      title: 'Status',
      type: 'string',
      enum: [
        'Draft',
        'Submitted',
        'Under Review',
        'Reviewed',
        'Lorem',
        'ipsum',
        'dolor',
        'sit',
        'amet,',
        'consectetur',
        'adipiscing',
        'elit',
        'Suspendisse',
        'sed',
        'mattis',
        'ante',
        'Integer',
        'diam',
        'eros,',
        'aliquam',
        'vitae',
        'rhoncus',
        'sit',
        'amet,',
        'ultrices',
        'at',
        'nunc',
      ],
    },
    attachment: {
      type: 'string',
    },
    attachments: {
      type: 'array',
      title: 'Attachments',
      items: {
        type: 'string',
      },
    },
  },
};

export default incidentSchema;
