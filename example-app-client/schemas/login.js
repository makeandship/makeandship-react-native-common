/* @flow */

export default {
  title: 'Log in',
  type: 'object',
  properties: {
    username: {
      type: 'string',
      format: 'email',
      title: 'Email',
    },
    password: {
      type: 'string',
      title: 'Password',
    },
    totp: {
      type: 'string',
      title: 'One time code',
    },
  },
  required: ['username', 'password', 'totp'],
};
