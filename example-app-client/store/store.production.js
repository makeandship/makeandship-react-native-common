/* @flow */

import { createStore, applyMiddleware, compose } from 'redux';

import createSagaMiddleware from 'redux-saga';

import rootReducer, { rootSaga } from '../modules';

const sagaMiddleware = createSagaMiddleware();

const middleware = [sagaMiddleware];

const enhancer = compose(applyMiddleware(...middleware));

const store = createStore(rootReducer, enhancer);

sagaMiddleware.run(rootSaga);

export default store;
