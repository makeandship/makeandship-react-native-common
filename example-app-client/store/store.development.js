/* @flow */

import { createStore, applyMiddleware, compose as vanillaCompose } from 'redux';
import { createLogger } from 'redux-logger';
import { composeWithDevTools } from 'remote-redux-devtools';

import createSagaMiddleware from 'redux-saga';

if (window) {
  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ =
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || composeWithDevTools;
}

import {
  initialise,
  login,
  register,
  logout,
  updateToken,
  loadProfile,
  profile,
} from 'makeandship-js-common/src/modules/auth/actions';
import { jsonApiActions } from 'makeandship-js-common/src/modules/api';
import {
  createIncident,
  requestIncident,
  updateIncident,
  deleteIncident,
} from '../modules/incidents';
import {
  requestCurrentPosition,
  startWatchingPosition,
  stopWatchingPosition,
} from '../../src/modules/geolocation/geolocation';
import {
  setFormData,
  clearFormData,
} from 'makeandship-js-common/src/modules/form';
import { showNotification } from 'makeandship-js-common/src/modules/notifications';

import rootReducer, { rootSaga } from '../modules';

import { actions as reactNavigationActions } from '../../src/modules/ui/reactNavigation';

import {
  dateTimePickerSetShowing,
  dateTimePickerSetOptions,
  dateTimePickerShow,
  dateTimePickerConfirm,
  dateTimePickerCancel,
} from '../../src/modules/ui/dateTimePicker';

import {
  imagePickerPickWithPrompt,
  imagePickerPickFromCamera,
  imagePickerPickFromLibrary,
} from '../../src/modules/ui/imagePicker';

const devToolsPresent =
  window && typeof window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ === 'function';

let compose;

if (devToolsPresent) {
  const actionCreators = {
    showNotification,
    navigate: reactNavigationActions.navigate,
    goBack: reactNavigationActions.goBack,
    authInitialise: initialise,
    login,
    register,
    logout,
    updateToken,
    loadProfile,
    profile,
    pauseJsonApi: jsonApiActions.pause,
    resumeJsonApi: jsonApiActions.resume,
    flushJsonApi: jsonApiActions.flush,
    setFormData,
    clearFormData,
    createIncident,
    requestIncident,
    updateIncident,
    deleteIncident,
    dateTimePickerSetShowing,
    dateTimePickerSetOptions,
    dateTimePickerShow,
    dateTimePickerConfirm,
    dateTimePickerCancel,
    imagePickerPickWithPrompt,
    imagePickerPickFromCamera,
    imagePickerPickFromLibrary,
    requestCurrentPosition,
    startWatchingPosition,
    stopWatchingPosition,
  };
  const actionsBlacklist = [];
  compose = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
    name: require('../../package.json').name,
    actionCreators,
    actionsBlacklist,
  });
} else {
  compose = vanillaCompose;
}

const actionsBlacklist = [];

const sagaMiddleware = createSagaMiddleware();

const middleware = [sagaMiddleware];

const logger = createLogger({
  level: 'info',
  collapsed: true,
  predicate: (getState, action) =>
    action && action.type && actionsBlacklist.indexOf(action.type) === -1,
});
middleware.push(logger);

const enhancer = compose(applyMiddleware(...middleware));

const store = createStore(rootReducer, enhancer);

sagaMiddleware.run(rootSaga);

if (module.hot) {
  module.hot.accept(
    '../modules',
    () => store.replaceReducer(require('../modules')) // eslint-disable-line global-require
  );
}

export default store;
