/* @flow */

import { HeaderStyleInterpolators } from '@react-navigation/stack';
import { Platform } from 'react-native';
import { mergeTheme } from '../../src/theme';

const Theme = mergeTheme({
  roundness: 500,
  colors: {
    primary: '#f60',
  },
  padding: {
    button: {
      paddingLeft: 16,
      paddingRight: 16,
      paddingTop: 5,
      paddingBottom: 5,
    },
  },
});

export const stackNavigatorScreenOptions = {
  headerStyle: {
    backgroundColor: Theme.colors.primary,
  },
  headerTintColor: Theme.colors.white,
  headerTitleStyle: {
    ...Theme.fonts.bold,
  },
  headerStyleInterpolator: Platform.select({
    ios: HeaderStyleInterpolators.forUIKit,
    android: undefined,
  }),
};

export default Theme;
