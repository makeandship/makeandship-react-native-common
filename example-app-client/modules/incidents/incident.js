/* @flow */

import { createEntityModule } from 'makeandship-js-common/src/modules/generic';

const incidentModule = createEntityModule('incident', {
  typePrefix: 'incidents/incident/',
  getState: (state) => state.incidents.incident,
  create: {
    operationId: 'incidentsCreate',
  },
  request: {
    operationId: 'incidentsGetById',
  },
  update: {
    operationId: 'incidentsUpdateById',
  },
  delete: {
    operationId: 'incidentsDeleteById',
  },
});

const {
  reducer,
  actionType: incidentActionType,
  actionTypes: incidentActionTypes,
  actions: {
    newEntity: newIncident,
    createEntity: createIncident,
    requestEntity: requestIncident,
    updateEntity: updateIncident,
    deleteEntity: deleteIncident,
    setEntity: setIncident,
  },
  selectors: {
    getEntity: getIncident,
    getError: getIncidentError,
    getIsFetching: getIncidentIsFetching,
  },
  sagas: { entitySaga: incidentSaga },
} = incidentModule;

export {
  incidentActionType,
  incidentActionTypes,
  newIncident,
  createIncident,
  requestIncident,
  updateIncident,
  deleteIncident,
  getIncident,
  setIncident,
  getIncidentIsFetching,
  getIncidentError,
  incidentModule,
  incidentSaga,
};

export default reducer;
