/* @flow */

import { all, fork } from 'redux-saga/effects';
import { combineReducers } from 'redux';

import incident, { incidentSaga } from './incident';

export {
  createIncident,
  requestIncident,
  updateIncident,
  deleteIncident,
  getIncident,
  getIncidentIsFetching,
  getIncidentError,
} from './incident';

const incidentsReducer = combineReducers({
  incident,
});

export default incidentsReducer;

export function* rootIncidentsSaga(): Generator<*, *, *> {
  yield all([fork(incidentSaga)]);
}
