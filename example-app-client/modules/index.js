/* @flow */

import { combineReducers } from 'redux';
import {
  all,
  call,
  put,
  fork,
  takeEvery,
  takeLeading,
} from 'redux-saga/effects';
import type { Saga } from 'redux-saga';
import axios from 'axios';
import * as Keychain from 'react-native-keychain';

import { restartSagaOnError } from 'makeandship-js-common/src/modules/utils';
import {
  reducer as auth,
  saga as authSaga,
  setConfig as setAuthConfig,
  actions as authActions,
} from 'makeandship-js-common/src/modules/auth';
import api, {
  rootApiSaga,
  setConfig as setApiConfig,
  jsonApiActions,
} from 'makeandship-js-common/src/modules/api';
import {
  reducer as query,
  saga as querySaga,
  setConfig as setQueryConfig,
} from 'makeandship-js-common/src/modules/query';
import notifications, {
  rootNotificationsSaga,
  showNotification,
  NotificationCategories,
} from 'makeandship-js-common/src/modules/notifications';
import form from 'makeandship-js-common/src/modules/form';
import { userFromToken } from 'makeandship-js-common/src/utils/jwt';

import { createAxiosFetcher } from 'makeandship-js-common/src/modules/fetchers/axiosFetcher';
import jsendResponseTransformer from 'makeandship-js-common/src/modules/transformers/jsendResponseTransformer';
import { createAxiosAuthInterceptor } from 'makeandship-js-common/src/modules/auth/interceptors/axiosAuthInterceptor';

import { createSilentAuthProvider } from '../../src/modules/appAuth/providers/silentAuthProvider';
import { createAppAuthProvider } from '../../src/modules/appAuth/providers/appAuthProvider';
import { createKeychainStorage } from '../../src/modules/appAuth/storage/keychainStorage';

import system, { systemSaga } from '../../src/modules/system';
import ui, { uiSaga } from '../../src/modules/ui';
import geolocation, { geolocationSaga } from '../../src/modules/geolocation';

import incidents, { rootIncidentsSaga } from './incidents';

// auth provider - use keychain for secure storage at rest
// for access control and security level see https://github.com/oblador/react-native-keychain

const issuer = `${process.env.REACT_APP_KEYCLOAK_URL}/realms/${process.env.REACT_APP_KEYCLOAK_REALM}`;
const clientId = process.env.REACT_APP_KEYCLOAK_CLIENT_ID;
const redirectUrl = process.env.REACT_APP_REDIRECT_URI;
const scopes = ['openid', 'profile'];

const storage = createKeychainStorage({
  issuer,
  clientId,
  accessControl: Keychain.ACCESS_CONTROL.BIOMETRY_ANY_OR_DEVICE_PASSCODE,
  securityLevel: Keychain.SECURITY_LEVEL.ANY,
});

const provider = false
  ? createSilentAuthProvider({
      issuer,
      clientId,
      scopes,
      storage,
    })
  : createAppAuthProvider({
      issuer,
      clientId,
      scopes,
      redirectUrl,
      storage,
    });

// api fetcher using auth provider

const axiosInstance = axios.create({
  baseURL: process.env.REACT_APP_API_URL,
  transformResponse: [jsendResponseTransformer],
});

if (process.env.NODE_ENV !== 'production') {
  // log network requests as curl to help with debugging
  const curlirize = require('axios-curlirize').default;
  curlirize(axiosInstance);
}

const authInterceptor = createAxiosAuthInterceptor(provider);
axiosInstance.interceptors.request.use(authInterceptor);

const fetcher = createAxiosFetcher(axiosInstance);

setAuthConfig({
  provider,
});

setQueryConfig({
  fetcher,
  debug: true,
});

setApiConfig({
  fetcher,
  apiUrl: process.env.REACT_APP_API_URL,
  apiSpecUrl: process.env.REACT_APP_API_SPEC_URL,
  debug: true,
});

const rootReducer = combineReducers({
  system,
  api,
  auth,
  form,
  incidents,
  notifications,
  query,
  ui,
  geolocation,
});

export default rootReducer;

export function* tokenWatcher(): Saga {
  yield takeEvery(
    [
      authActions.initialiseSuccess,
      authActions.updateTokenSuccess,
      authActions.updateTokenError,
      authActions.authLogout,
    ],
    function* () {
      const token = yield call(provider.getToken);
      if (token) {
        const user = userFromToken(token);
        console.log({ user });
      }
    }
  );
}

const sagas = [
  tokenWatcher,
  rootApiSaga,
  authSaga,
  querySaga,
  rootIncidentsSaga,
  rootNotificationsSaga,
  systemSaga,
  uiSaga,
  geolocationSaga,
];

export function* rootSaga(): Saga {
  if (process.env.NODE_ENV !== 'development') {
    yield all(sagas.map(restartSagaOnError).map((saga) => call(saga)));
  } else {
    yield all(sagas.map((saga) => fork(saga)));
  }
  yield put(authActions.initialise());
  yield takeEvery(
    [jsonApiActions.error, authActions.authError],
    function* (action) {
      const content = action.payload?.message;
      if (content) {
        yield put(
          showNotification({
            category: NotificationCategories.ERROR,
            content,
          })
        );
      }
    }
  );
  yield takeLeading(authActions.updateTokenError, function* () {
    // yield call(provider.login);
    yield console.warn('TODO - token expired, ask user to log in again');
  });
}
