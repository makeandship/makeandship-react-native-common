/* @flow */

import * as React from 'react';
import DevMenu from 'react-native-dev-menu';
import AsyncStorage from '@react-native-community/async-storage';
import { SafeAreaProvider } from 'react-native-safe-area-context';

import App from './App';
import Storybook from '../storybook';

export type State = {
  storybook: boolean,
};

const STORAGE_KEY = '@storybook';

export default class DevApp extends React.Component<*, State> {
  state = {
    storybook: false,
  };

  constructor(props: any) {
    super(props);
    AsyncStorage.getItem(STORAGE_KEY).then((value) => {
      try {
        if (value && value !== null) {
          const storybook = JSON.parse(value);
          this.setState({ storybook });
        }
      } catch (error) {
        // ignore error
      }
    });
  }

  setStoryBook = (storybook: boolean) => {
    AsyncStorage.setItem(STORAGE_KEY, JSON.stringify(storybook));
    this.setState({
      storybook,
    });
  };

  toggleStoryBook = () => {
    this.setStoryBook(!this.state.storybook);
  };

  componentDidMount() {
    DevMenu.addItem('Toggle Storybook', this.toggleStoryBook);
  }

  render() {
    return this.state.storybook ? (
      <SafeAreaProvider>
        <Storybook />
      </SafeAreaProvider>
    ) : (
      <App />
    );
  }
}
