/* @flow */

import * as React from 'react';
import { Platform } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import EditIncident from '../components/incidents/edit/EditIncident';
import Geolocation from '../components/geolocation/Geolocation';
import Modules from '../components/modules/Modules';
import Login from '../components/appAuth/Login';

import { SelectCellForm } from '../../src/components/ui/form/cells';
import PhotoInputBinaryCellImageView from '../../src/components/ui/form/cells/PhotoInputBinaryCellImageView';

import Theme, { stackNavigatorScreenOptions } from '../theme/Theme';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const LoginStack = () => (
  <Stack.Navigator screenOptions={stackNavigatorScreenOptions}>
    <Stack.Screen name="Login" component={Login} />
  </Stack.Navigator>
);

const IncidentsStack = () => (
  <Stack.Navigator screenOptions={stackNavigatorScreenOptions}>
    <Stack.Screen name="EditIncident" component={EditIncident} />
  </Stack.Navigator>
);

const GeolocationStack = () => (
  <Stack.Navigator screenOptions={stackNavigatorScreenOptions}>
    <Stack.Screen name="Geolocation" component={Geolocation} />
  </Stack.Navigator>
);

const ModulesStack = () => (
  <Stack.Navigator screenOptions={stackNavigatorScreenOptions}>
    <Stack.Screen name="Modules" component={Modules} />
  </Stack.Navigator>
);

const tabBarOptions = {
  activeTintColor: Theme.colors.primary,
  keyboardHidesTabBar: Platform.select({ ios: false, android: true }),
};

const TabStack = () => (
  <Tab.Navigator tabBarOptions={tabBarOptions}>
    <Tab.Screen name="IncidentsStack" component={IncidentsStack} />
    <Tab.Screen name="LoginStack" component={LoginStack} />
    <Tab.Screen name="GeolocationStack" component={GeolocationStack} />
    <Tab.Screen name="ModulesStack" component={ModulesStack} />
  </Tab.Navigator>
);

const RootStack = () => (
  <Stack.Navigator screenOptions={stackNavigatorScreenOptions}>
    <Stack.Screen
      name="TabStack"
      component={TabStack}
      options={{ headerShown: false }}
    />
    <Stack.Screen
      name="PhotoInputBinaryCellImageView"
      component={PhotoInputBinaryCellImageView}
    />
    <Stack.Screen name="SelectCellForm" component={SelectCellForm} />
  </Stack.Navigator>
);

export default RootStack;
