/* @flow */

const fs = require('fs');
const path = require('path');
const plist = require('plist');
const readline = require('readline');
const execSync = require('child_process').execSync;

// Take current build number from iOS, increment
// Write back into Info.plist and gradle.properties

const IOS_PLIST_PATH = path.join(__dirname, '../ios/ExampleApp/Info.plist');
const IOS_SETTING_PLIST_PATH = path.join(
  __dirname,
  '../ios/ExampleApp/Resources/Settings.bundle/Root.plist'
);
const ANDROID_PROPERTIES_PATH = path.join(
  __dirname,
  '../android/gradle.properties'
);

const DEBUG = false;

export const incrementBuildNumber = () => {
  // reset the plist so we don't keep incrementing
  const resetCommand = `git checkout HEAD -- '${IOS_PLIST_PATH}'`;
  execSync(resetCommand);

  const iosPlistJson = plist.parse(fs.readFileSync(IOS_PLIST_PATH, 'utf8'));

  const currentBuildNumber = parseInt(iosPlistJson['CFBundleVersion']);
  const currentVerionString = iosPlistJson['CFBundleShortVersionString'];

  const newBuildNumber = currentBuildNumber + 1;

  iosPlistJson['CFBundleVersion'] = `${newBuildNumber}`;

  if (DEBUG) {
    console.log(resetCommand);
    console.log(JSON.stringify(iosPlistJson, null, 2));
    console.log(`Current CFBundleShortVersionString=${currentVerionString}`);
    console.log(`Current CFBundleVersion=${currentBuildNumber}`);
    console.log(`Updated CFBundleVersion=${newBuildNumber}`);
  }
  // update iOS Info.plist

  if (!DEBUG) {
    const iosPlistXml = plist.build(iosPlistJson);
    fs.writeFileSync(IOS_PLIST_PATH, iosPlistXml);
  }

  // update iOS Settings bundle display version

  const iosSettingsPlistJson = plist.parse(
    fs.readFileSync(IOS_SETTING_PLIST_PATH, 'utf8')
  );
  if (DEBUG) {
    console.log(JSON.stringify(iosSettingsPlistJson, null, 2));
  }
  iosSettingsPlistJson['PreferenceSpecifiers'][1][
    'DefaultValue'
  ] = `${currentVerionString} (${newBuildNumber})`;

  if (!DEBUG) {
    const iosSettingsPlistXml = plist.build(iosSettingsPlistJson);
    fs.writeFileSync(IOS_SETTING_PLIST_PATH, iosSettingsPlistXml);
  }

  // update gradle.properties

  let androidGradlePropertiesString = '';

  const readInterface = readline.createInterface({
    input: fs.createReadStream(ANDROID_PROPERTIES_PATH),
    // output: process.stdout,
    console: false,
  });

  readInterface.on('line', (line) => {
    const indexOfEquals = line.indexOf('=');
    if (indexOfEquals > 0) {
      const parts = line.split('=');
      if (parts.length === 2) {
        if (parts[0] === 'MYAPP_VERSION_NAME') {
          parts[1] = currentVerionString;
        } else if (parts[0] === 'MYAPP_VERSION_CODE') {
          parts[1] = newBuildNumber;
        }
        line = parts.join('=');
      }
    }
    androidGradlePropertiesString += `${line}\r\n`;
  });

  readInterface.on('close', () => {
    if (DEBUG) {
      console.log(
        'androidGradlePropertiesString',
        androidGradlePropertiesString
      );
    }
    if (!DEBUG) {
      fs.writeFileSync(ANDROID_PROPERTIES_PATH, androidGradlePropertiesString);
    }
  });
};

(async () => {
  try {
    await incrementBuildNumber();
  } catch (error) {
    console.error(error);
  }
})();
