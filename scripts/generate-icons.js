/* @flow */

const fs = require('fs');
const path = require('path');

const SOURCE_DIRECTORY = path.join(__dirname, '../src/static/img/icons');

// rename navigation-bar-back.android@3x.png
// to navigation-bar-back@3x.android.png

const shouldRenameFile = (file) => file.indexOf('android@') !== -1;

const renameIcons = async () => {
  fs.readdirSync(SOURCE_DIRECTORY)
    .filter(shouldRenameFile)
    .forEach((file) => {
      const components = file.split('.');
      const extenstion = components.pop();
      const platformAndScale = components.pop();
      const prefix = components.pop();
      const platformAndScaleComponents = platformAndScale.split('@');
      const scale = platformAndScaleComponents.pop();
      const platform = platformAndScaleComponents.pop();
      const newName = `${prefix}@${scale}.${platform}.${extenstion}`;
      fs.renameSync(
        path.join(SOURCE_DIRECTORY, file),
        path.join(SOURCE_DIRECTORY, newName)
      );
      console.log(`Renamed ${file} -> ${newName}`);
    });
};

// generate list of icons

const fileIncludesPlatform = (file) => file.indexOf('.android') !== -1;

const fileIncludesMultiplier = (file) => file.indexOf('@') !== -1;

const shouldIncludeFile = (file) => {
  const includesMultiplier = fileIncludesMultiplier(file);
  const includesPlatform = fileIncludesPlatform(file);
  return !includesPlatform && !includesMultiplier;
};

const generateIcons = async () => {
  let previousConstName = '';
  let fileContents =
    '/* This file is auto-generated, see the generate-icons script */\n\n';

  fs.readdirSync(SOURCE_DIRECTORY)
    .filter(shouldIncludeFile)
    .forEach((file) => {
      const baseName = file.substr(0, file.indexOf('.'));
      let constName = baseName.toUpperCase();
      constName = constName.replace(/-/g, '_');
      if (constName.substr(0, 5) === 'ICON_') {
        constName = constName.substr(5);
      }
      if (constName !== previousConstName) {
        fileContents =
          fileContents +
          `export const ${constName} = require('./img/icons/${file}');\n`;
        previousConstName = constName;
      }
    });

  fs.writeFileSync(
    path.join(__dirname, '../src/static/icons.js'),
    fileContents,
    'utf8'
  );

  console.log('Wrote src/static/icons.js');
};

(async () => {
  try {
    await renameIcons();
    await generateIcons();
  } catch (error) {
    console.error(error);
  }
})();
