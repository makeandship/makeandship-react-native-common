/* @flow */

import defaultsDeep from '@nodeutils/defaults-deep';

import DefaultTheme from './DefaultTheme';

const mergeTheme = (theme: any, defaulTheme: any = DefaultTheme): any => {
  const mergedTheme = defaultsDeep({}, theme, defaulTheme);
  return mergedTheme;
};

export default mergeTheme;
