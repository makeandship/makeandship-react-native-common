/* @flow */

import { Platform } from 'react-native';

const colors = {
  primary: '#2e7fcf',
  secondary: '#0072c6',
  background: '#f4f4f4',
  surface: '#ffffff',
  card: '#ffffff',
  border: '#b2b2b2',
  white: '#ffffff',
  transparent: 'rgba(0,0,0,0)',

  error: '#FF0000',
  destructive: '#FF0000',

  ragRed: '#FF0000',
  ragAmber: '#F7931E',
  ragGreen: '#009245',

  disabled: '#999',

  greyDark: '#666',
  greyMid: '#999',
  greyMidTranslucent: 'rgba(153,153,153,0.95)',
  dimming: 'rgba(0,0,0,0.7)',

  sectionTitle: '#666',
  placeholder: '#999',

  tabBarIconInactive: Platform.select({
    ios: '#A4AAB3',
    android: '#717171',
  }),

  panel: '#f4f4f4',
  panelOverlay: 'rgba(244,244,244,0.8)',
  panelTranslucent: 'rgba(244,244,244,0.95)',

  text: '#333333',
  title: '#333333',
  subtitle: '#4d4d4d',
};

const fonts = Platform.select({
  ios: {
    regular: {
      fontFamily: 'System',
      fontWeight: '400',
    },
    bold: {
      fontFamily: 'System',
      fontWeight: '700',
    },
    medium: {
      fontFamily: 'System',
      fontWeight: '500',
    },
    light: {
      fontFamily: 'System',
      fontWeight: '300',
    },
    thin: {
      fontFamily: 'System',
      fontWeight: '100',
    },
  },
  android: {
    regular: {
      fontFamily: 'Roboto',
      fontWeight: '400',
    },
    bold: {
      fontFamily: 'Roboto',
      fontWeight: '700',
    },
    medium: {
      fontFamily: 'Roboto',
      fontWeight: '500',
    },
    light: {
      fontFamily: 'Roboto',
      fontWeight: '300',
    },
    thin: {
      fontFamily: 'Roboto',
      fontWeight: '100',
    },
  },
});

const padding = {
  button: {
    paddingLeft: 8,
    paddingRight: 8,
    paddingTop: 5,
    paddingBottom: 5,
  },
  cell: {
    paddingBottom: 8,
    paddingLeading: 16,
    paddingTop: 8,
    paddingTrailing: 12,
  },
};

const spacing = {
  cell: {
    spacingHorizontal: 12,
  },
};

const DefaultTheme: any = {
  dark: false,
  roundness: 4,
  colors,
  fonts,
  padding,
  spacing,
  fontSize: 17,
  fontSizeExtraLarge: 36,
  fontSizeExtraSmall: 11,
  fontSizeLarge: 21,
  fontSizeSmall: 14,
};

export default DefaultTheme;
