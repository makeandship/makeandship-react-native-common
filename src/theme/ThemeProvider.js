/* @flow */

import * as React from 'react';
import ThemeContext from './ThemeContext';

const ThemeProvider = ({
  value,
  children,
}: React.ElementProps<*>): React.Element<*> => {
  return (
    <ThemeContext.Provider value={value}>{children}</ThemeContext.Provider>
  );
};

export default ThemeProvider;
