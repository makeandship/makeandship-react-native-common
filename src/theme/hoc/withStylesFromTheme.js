/* @flow */

import * as React from 'react';
import { StyleSheet } from 'react-native';
import memoize from 'memoize-one';
import hoistNonReactStatic from 'hoist-non-react-statics';

import ThemeContext from '../ThemeContext';

/* eslint-disable no-unused-vars, react/prop-types */

export default function withStylesFromTheme(
  WrappedComponent: React.ElementProps<*>,
  stylesFromTheme: Function,
  WrappedComponentThemeContext = ThemeContext
) {
  const createStyles = memoize((theme) =>
    StyleSheet.create(stylesFromTheme(theme))
  );

  const WithStylesFromTheme = React.forwardRef((props, ref) => {
    const { styles, theme, ...rest } = props;
    return (
      <WrappedComponentThemeContext.Consumer>
        {(theme) => {
          const styles = createStyles(theme);
          return (
            <WrappedComponent
              ref={ref}
              theme={theme}
              styles={styles}
              {...rest}
            />
          );
        }}
      </WrappedComponentThemeContext.Consumer>
    );
  });

  WithStylesFromTheme.displayName = `withStylesFromTheme(${getDisplayName(
    WrappedComponent
  )})`;

  hoistNonReactStatic(WithStylesFromTheme, WrappedComponent);

  return WithStylesFromTheme;
}
/* eslint-enable no-unused-vars, react/prop-types */

function getDisplayName(WrappedComponent) {
  return WrappedComponent.displayName || WrappedComponent.name || 'Component';
}
