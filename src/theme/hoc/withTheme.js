/* @flow */

import * as React from 'react';
import hoistNonReactStatic from 'hoist-non-react-statics';

import ThemeContext from '../ThemeContext';

/* eslint-disable no-unused-vars, react/prop-types */
export default function withTheme(
  WrappedComponent: React.ElementProps<*>,
  WrappedComponentThemeContext = ThemeContext
) {
  const WithTheme = React.forwardRef((props, ref) => {
    const { theme, ...rest } = props;
    return (
      <WrappedComponentThemeContext.Consumer>
        {(theme) => {
          return <WrappedComponent ref={ref} theme={theme} {...rest} />;
        }}
      </WrappedComponentThemeContext.Consumer>
    );
  });

  WithTheme.displayName = `withTheme(${getDisplayName(WrappedComponent)})`;

  hoistNonReactStatic(WithTheme, WrappedComponent);

  return WithTheme;
}
/* eslint-enable no-unused-vars, react/prop-types */

function getDisplayName(WrappedComponent) {
  return WrappedComponent.displayName || WrappedComponent.name || 'Component';
}
