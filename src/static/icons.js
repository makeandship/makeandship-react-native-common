/* This file is auto-generated, see the generate-icons script */

export const BACK = require('./img/icons/icon-back.png');
export const FORM_CELL_DISCLOSURE = require('./img/icons/icon-form-cell-disclosure.png');
export const FORM_CELL_TICK = require('./img/icons/icon-form-cell-tick.png');
export const FORM_RADIO_CHECKED = require('./img/icons/icon-form-radio_checked.png');
export const FORM_RADIO_UNCHECKED = require('./img/icons/icon-form-radio_unchecked.png');
