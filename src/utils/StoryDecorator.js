/* @flow */

import * as React from 'react';
import { View, StyleSheet } from 'react-native';

import { mergeTheme, ThemeProvider } from '../theme';

const Theme = mergeTheme({
  colors: {
    primary: '#f0f',
  },
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#999',
  },
});

export default (story: Function) => (
  <ThemeProvider value={Theme}>
    <View style={styles.container}>{story()}</View>
  </ThemeProvider>
);
