/* @flow */

import * as React from 'react';
import { Provider } from 'react-redux';

import store from '../../example-app-client/store';

export default (story: Function) => (
  <Provider store={store}>{story()}</Provider>
);
