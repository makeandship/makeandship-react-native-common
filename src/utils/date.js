/* @flow */

// https://date-fns.org/docs/I18n

import * as dateFns from 'date-fns';
import { enGB, enUS } from 'date-fns/locale';
import _get from 'lodash.get';
import * as RNLocalize from 'react-native-localize';

const locales = { enGB, enUS };

export const getDefaultLocale = () => {
  try {
    const locales = RNLocalize.getLocales();
    const deviceLocale = locales[0].languageTag;
    const locale = deviceLocale.replace('-', '');
    return locale;
  } catch (e) {
    console.error(e);
  }
  return 'enGB';
};

export const setCurrentLocale = (localeId: string) => {
  window.__localeId__ = localeId;
};

export const getCurrentLocale = () => {
  return _get(locales, window.__localeId__);
};

export const formatDate = (date: Date, formatStr: string = 'PP') => {
  return dateFns.format(date, formatStr, {
    locale: getCurrentLocale(),
  });
};

export const initCurrentLocale = () => {
  setCurrentLocale(getDefaultLocale());
};
