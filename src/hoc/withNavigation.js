/* @flow */

import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import {
  actions as reactNavigationActions,
  selectors as reactNavigationSelectors,
} from '../modules/ui/reactNavigation';

const {
  navigate,
  goBack,
  navigateRoot,
  setParams,
  reset,
} = reactNavigationActions;

const { getCurrentRoute, getPreviousRoute } = reactNavigationSelectors;

export const withNavigationPropTypes = {
  currentRoute: PropTypes.any,
  previousRoute: PropTypes.any,
  navigate: PropTypes.func.isRequired,
  goBack: PropTypes.func.isRequired,
  navigateRoot: PropTypes.func.isRequired,
  setParams: PropTypes.func.isRequired,
  reset: PropTypes.func.isRequired,
};

const withNavigation = connect(
  (state) => ({
    currentRoute: getCurrentRoute(state),
    previousRoute: getPreviousRoute(state),
  }),
  {
    navigate,
    goBack,
    navigateRoot,
    setParams,
    reset,
  }
);

export default withNavigation;
