/* @flow */

import * as React from 'react';
import hoistNonReactStatic from 'hoist-non-react-statics';

let safearea = { right: 0, top: 20, left: 0, bottom: 0 };

function withSafeArea(WrappedComponent: React.ElementProps<*>) {
  class WithSafeArea extends React.Component<*, State> {
    render() {
      return <WrappedComponent safearea={safearea} {...this.props} />;
    }
  }

  WithSafeArea.displayName = `WithSafeArea(${getDisplayName(
    WrappedComponent
  )})`;

  hoistNonReactStatic(WithSafeArea, WrappedComponent);

  return WithSafeArea;
}

function getDisplayName(WrappedComponent) {
  return WrappedComponent.displayName || WrappedComponent.name || 'Component';
}

export default withSafeArea;
