/* @flow */

import * as React from 'react';
import SafeArea from 'react-native-safe-area';
import hoistNonReactStatic from 'hoist-non-react-statics';

let safearea = { right: 0, top: 20, left: 0, bottom: 0 };

type State = {
  safearea: {
    right: number,
    top: number,
    left: number,
    bottom: number,
  },
};

function withSafeArea(WrappedComponent: React.ElementProps<*>) {
  class WithSafeArea extends React.Component<*, State> {
    constructor(props: any) {
      super(props);
      this.state = {
        safearea,
      };
      SafeArea.getSafeAreaInsetsForRootView().then(({ safeAreaInsets }) => {
        this.safeAreaInsetsForRootViewDidChange({ safeAreaInsets });
      });
    }

    componentDidMount() {
      SafeArea.addEventListener(
        'safeAreaInsetsForRootViewDidChange',
        this.safeAreaInsetsForRootViewDidChange
      );
      this.setState({
        safearea,
      });
    }

    componentWillUnmount() {
      SafeArea.removeEventListener(
        'safeAreaInsetsForRootViewDidChange',
        this.safeAreaInsetsForRootViewDidChange
      );
    }

    safeAreaInsetsForRootViewDidChange = ({ safeAreaInsets }: any) => {
      safearea = safeAreaInsets;
      this.setState({ safearea });
    };

    render() {
      return (
        <WrappedComponent safearea={this.state.safearea} {...this.props} />
      );
    }
  }

  WithSafeArea.displayName = `WithSafeArea(${getDisplayName(
    WrappedComponent
  )})`;

  hoistNonReactStatic(WithSafeArea, WrappedComponent);

  return WithSafeArea;
}

function getDisplayName(WrappedComponent) {
  return WrappedComponent.displayName || WrappedComponent.name || 'Component';
}

export default withSafeArea;
