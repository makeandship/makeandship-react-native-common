/* @flow */

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { getNotifications } from 'makeandship-js-common/src/modules/notifications';

import Notifications from './Notifications';

function mapStateToProps(state) {
  return {
    notifications: getNotifications(state),
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Notifications);
