/* @flow */

import React from 'react';
import { storiesOf } from '@storybook/react-native';

import { NotificationCategories } from 'makeandship-js-common/src/modules/notifications';

import Notifications from './Notifications';

const variants = {
  default: {
    notifications: [
      {
        id: '123',
        category: NotificationCategories.MESSAGE,
        content: 'This is a notification of type MESSAGE',
      },
    ],
  },
};

storiesOf('Notifications', module).add('Default', () => (
  <Notifications {...variants.default} />
));
