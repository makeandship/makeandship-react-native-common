/* @flow */

import * as React from 'react';
import PropTypes from 'prop-types';
import { View, Text, Animated, StyleSheet } from 'react-native';

import { NotificationCategories } from 'makeandship-js-common/src/modules/notifications';

import AppIcon from '../ui/AppIcon';
import useTheme from '../../theme/useTheme';

const Notification = ({
  category,
  content,
  hide,
  onHideComplete,
  id,
}: React.ElementProps<*>): React.Element<*> => {
  const theme = useTheme();
  const styles = makeStyles(theme);

  const categoryToIcon = {
    [NotificationCategories.SUCCESS]: 'check-circle',
    [NotificationCategories.MESSAGE]: 'info-circle',
    [NotificationCategories.ERROR]: 'exclamation-circle',
  };

  const categoryToColor = {
    [NotificationCategories.SUCCESS]: theme.colors.ragGreen,
    [NotificationCategories.MESSAGE]: theme.colors.ragAmber,
    [NotificationCategories.ERROR]: theme.colors.ragRed,
  };

  const animatedValue = React.useRef(new Animated.Value(hide ? 1 : 0)).current;

  React.useEffect(() => {
    if (hide) {
      Animated.timing(animatedValue, {
        toValue: 0,
        duration: 300,
        useNativeDriver: false,
      }).start(() => {
        onHideComplete && onHideComplete(id);
      });
    } else {
      Animated.timing(animatedValue, {
        toValue: 1,
        duration: 300,
        useNativeDriver: false,
      }).start();
    }
  }, [hide]);

  const icon = categoryToIcon[category];
  const color = categoryToColor[category];
  const containerStyle = {
    top: animatedValue.interpolate({
      inputRange: [0, 1],
      outputRange: [-44, 0],
    }),
    opacity: animatedValue,
  };
  const notificationContainerStyle = {
    backgroundColor: color,
  };
  return (
    <Animated.View style={[styles.container, containerStyle]}>
      <View style={[styles.notificationContainer, notificationContainerStyle]}>
        <Text style={styles.text}>
          {icon && (
            <Text>
              <AppIcon name={icon} />{' '}
            </Text>
          )}
          <Text style={styles.text}>{content}</Text>
        </Text>
      </View>
    </Animated.View>
  );
};

Notification.propTypes = {
  category: PropTypes.string.isRequired,
  content: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  hide: PropTypes.bool,
  onHideComplete: PropTypes.func,
};

const makeStyles = (theme) =>
  StyleSheet.create({
    container: {
      position: 'absolute',
      left: 0,
      right: 0,
      paddingTop: 8,
      paddingLeft: 8,
      paddingRight: 8,
    },
    notificationContainer: {
      padding: 12,
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: 4,
    },
    text: {
      textAlign: 'center',
      fontSize: theme.fontSize,
      color: 'white',
    },
  });

export default Notification;
