/* @flow */

import * as React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';

import Notification from './Notification';

type State = {
  notificationsToHide: Array<any>,
};

class Notifications extends React.PureComponent<*, State> {
  constructor(props: any) {
    super(props);
    this.state = {
      notificationsToHide: [],
    };
  }

  componentDidUpdate(prevProps: any, prevState: any) {
    if (prevProps.notifications !== this.props.notifications) {
      // what was removed?
      const notificationsToHide = [];
      prevProps.notifications.forEach((notification) => {
        if (!this.props.notifications.includes(notification)) {
          // this was removed; hide it
          if (!prevState.notificationsToHide.includes(notification)) {
            notificationsToHide.push(notification);
          }
        }
      });
      console.log('notificationsToHide', notificationsToHide);
      this.setState({
        notificationsToHide: prevState.notificationsToHide.concat(
          notificationsToHide
        ),
      });
    }
  }

  onHideComplete = (id: string) => {
    this.setState({
      notificationsToHide: this.state.notificationsToHide.filter(
        (notification) => {
          return notification.id !== id;
        }
      ),
    });
  };

  render() {
    const { notifications } = this.props;
    const { notificationsToHide } = this.state;
    return (
      <SafeAreaView pointerEvents={'none'} style={styles.container}>
        <View style={styles.notificationsContainer}>
          {notifications.map((notification, index) => {
            const { id, content, category } = notification;
            return (
              <Notification
                key={index}
                content={content}
                category={category}
                id={id}
              />
            );
          })}
          {notificationsToHide.map((notification, index) => {
            const { id, content, category } = notification;
            return (
              <Notification
                hide
                onHideComplete={this.onHideComplete}
                key={index}
                content={content}
                category={category}
                id={id}
              />
            );
          })}
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
  },
  notificationsContainer: {},
});

Notifications.propTypes = {
  notifications: PropTypes.array.isRequired,
};

export default Notifications;
