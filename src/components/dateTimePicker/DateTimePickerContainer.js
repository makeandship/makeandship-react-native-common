/* @flow */

import { connect } from 'react-redux';

import {
  getDateTimePickerIsVisible,
  getDateTimePickerOptions,
  dateTimePickerConfirm,
  dateTimePickerCancel,
} from '../../modules/ui/dateTimePicker';

import DateTimePicker from 'react-native-modal-datetime-picker';

const withRedux = connect(
  (state, ownProps) => ({
    display: ownProps.display ?? 'spinner',
    isVisible: getDateTimePickerIsVisible(state),
    ...getDateTimePickerOptions(state),
  }),
  {
    onConfirm: dateTimePickerConfirm,
    onCancel: dateTimePickerCancel,
  }
);

export default withRedux(DateTimePicker);
