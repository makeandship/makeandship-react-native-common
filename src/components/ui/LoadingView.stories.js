/* @flow */

import React from 'react';

import { storiesOf } from '@storybook/react-native';

import LoadingView from './LoadingView';

storiesOf('LoadingView', module).add('Default', () => <LoadingView />);
