/* @flow */

import * as React from 'react';
import PropTypes from 'prop-types';
import { Text, View } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';

import useTheme from '../../../theme/useTheme';

const NavigationBar = ({
  leftComponent,
  rightComponent,
  title,
}: React.ElementProps<*>): React.Element<*> => {
  const theme = useTheme();
  const styles = makeStyles(theme);
  return (
    <SafeAreaView edges={['right', 'top', 'left']} style={styles.container}>
      <View style={styles.content}>
        <View style={styles.titleContainer}>
          <Text style={styles.title}>{title}</Text>
        </View>
        {leftComponent && (
          <View style={styles.leftComponentContainer}>{leftComponent}</View>
        )}
        {rightComponent && (
          <View style={styles.rightComponentContainer}>{rightComponent}</View>
        )}
      </View>
    </SafeAreaView>
  );
};

const makeStyles = (theme) => ({
  container: {
    backgroundColor: theme.colors.primary,
  },
  content: {
    minHeight: 44,
    flexDirection: 'row',
  },
  titleContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    ...theme.fonts.bold,
    color: 'white',
  },
  leftComponentContainer: {
    position: 'absolute',
    left: 0,
    bottom: 0,
  },
  rightComponentContainer: {
    position: 'absolute',
    right: 0,
    bottom: 0,
  },
});

NavigationBar.propTypes = {
  styles: PropTypes.any,
  leftComponent: PropTypes.object,
  rightComponent: PropTypes.object,
  title: PropTypes.string,
};

export default NavigationBar;
