/* @flow */

import * as React from 'react';

import NavigationBarButton from './NavigationBarButton';

const NavigationBarCancelButton = (props: React.ElementProps<*>) => (
  <NavigationBarButton title={'Cancel'} {...props} />
);

export default NavigationBarCancelButton;
