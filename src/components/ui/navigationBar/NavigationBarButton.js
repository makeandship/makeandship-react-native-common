/* @flow */

import * as React from 'react';
import PropTypes from 'prop-types';
import { Text, Platform } from 'react-native';

import useTheme from '../../../theme/useTheme';
import NativeTouchableElement from '../NativeTouchableElement';

const NavigationBarButton = ({
  tintColor,
  title,
  bold = false,
  ...rest
}: React.ElementProps<*>): React.Element<*> => {
  const theme = useTheme();
  const styles = makeStyles(theme);
  const style = [
    {
      color: tintColor || theme.colors.primary,
    },
    bold ? styles.navigationBarButtonTextBold : styles.navigationBarButtonText,
  ];
  return (
    <NativeTouchableElement
      style={styles.navigationBarButtonContainer}
      {...rest}
    >
      <Text style={style}>
        {Platform.select({ ios: title, android: title.toUpperCase() })}
      </Text>
    </NativeTouchableElement>
  );
};

const makeStyles = (theme) => ({
  navigationBarButtonContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    minHeight: 44,
  },
  navigationBarButtonText: {
    padding: 8,
    textAlign: 'center',
    ...Platform.select({
      ios: {
        fontSize: 17,
      },
    }),
  },
  navigationBarButtonTextBold: {
    padding: 8,
    textAlign: 'center',
    ...Platform.select({
      ios: {
        ...theme.fonts.bold,
        fontSize: 17,
      },
    }),
  },
  navigationBarIconButtonContainer: {
    width: 44,
    height: 44,
    justifyContent: 'center',
    alignItems: 'center',
  },
  icon: {
    tintColor: theme.colors.primary,
  },
});

NavigationBarButton.propTypes = {
  title: PropTypes.string,
  bold: PropTypes.bool,
};

export default NavigationBarButton;
