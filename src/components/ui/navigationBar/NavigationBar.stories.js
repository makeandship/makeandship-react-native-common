/* @flow */

import React from 'react';

import { storiesOf } from '@storybook/react-native';

import NavigationBar, {
  NavigationBarCancelButton,
  NavigationBarDoneButton,
  NavigationBarBackButton,
} from './index';

import DefaultTheme from '../../../theme/DefaultTheme';
import ThemeProvider from '../../../theme/ThemeProvider';

const onPress = (e) => {
  console.log('onPress', e);
};

const variants = {
  default: {
    title: 'Lorem',
  },
  buttons: {
    title: 'Lorem ipsum',
    leftComponent: <NavigationBarCancelButton onPress={onPress} />,
    rightComponent: <NavigationBarDoneButton onPress={onPress} />,
  },
  back: {
    title: 'Lorem ipsum',
    leftComponent: <NavigationBarBackButton onPress={onPress} />,
    rightComponent: (
      <NavigationBarDoneButton tintColor={'magenta'} onPress={onPress} />
    ),
  },
};

storiesOf('NavigationBar', module)
  .addDecorator((story) => (
    <ThemeProvider value={DefaultTheme}>{story()}</ThemeProvider>
  ))
  .add('Default', () => <NavigationBar {...variants.default} />)
  .add('Buttons', () => <NavigationBar {...variants.buttons} />)
  .add('Back', () => <NavigationBar {...variants.back} />);
