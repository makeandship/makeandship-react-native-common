/* @flow */

import * as React from 'react';
import { Image } from 'react-native';

import useTheme from '../../../theme/useTheme';
import NativeTouchableElement from '../NativeTouchableElement';
import * as icons from '../../../static/icons';

const NavigationBarBackButton = (
  props: React.ElementProps<*>
): React.Element<*> => {
  const theme = useTheme();
  const styles = makeStyles(theme);
  return (
    <NativeTouchableElement style={styles.iconContainer} {...props}>
      <Image style={styles.icon} source={icons.BACK} />
    </NativeTouchableElement>
  );
};

const makeStyles = (theme) => ({
  iconContainer: {
    width: 44,
    height: 44,
    justifyContent: 'center',
    alignItems: 'center',
  },
  icon: {
    tintColor: theme.colors.primary,
  },
});

export default NavigationBarBackButton;
