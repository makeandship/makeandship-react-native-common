/* @flow */

import * as React from 'react';

import NavigationBarButton from './NavigationBarButton';

const NavigationBarCancelButton = (props) => (
  <NavigationBarButton bold title={'Done'} {...props} />
);

export default NavigationBarCancelButton;
