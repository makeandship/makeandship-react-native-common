/* @flow */

import * as React from 'react';
import { Platform } from 'react-native';
import { HeaderBackButton } from '@react-navigation/stack';

import NavigationBarCancelButton from './NavigationBarCancelButton';

const NavigationBarCancelOrBackButton = (props: React.ElementProps<*>) =>
  Platform.select({
    ios: <NavigationBarCancelButton {...props} />,
    android: <HeaderBackButton {...props} />,
  });

export default NavigationBarCancelOrBackButton;
