/* @flow */

import * as React from 'react';
import PropTypes from 'prop-types';
import { View, Text, Image } from 'react-native';

import useTheme from '../../theme/useTheme';

const NoItemsView = ({
  description,
  icon,
  title = 'No items',
}: React.ElementProps<*>): React.Element<*> => {
  const theme = useTheme();
  const styles = makeStyles(theme);

  return (
    <View style={styles.container}>
      {icon && (
        <View style={styles.iconContainer}>
          <Image source={icon} style={styles.icon} />
        </View>
      )}
      <Text style={styles.title}>{title}</Text>
      {description && (
        <View style={styles.descriptionContainer}>
          <Text style={styles.description}>{description}</Text>
        </View>
      )}
    </View>
  );
};

NoItemsView.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  icon: PropTypes.any,
};

const makeStyles = (theme) => ({
  container: {
    flex: 1,
    backgroundColor: theme.colors.background,
    alignItems: 'center',
    justifyContent: 'center',
    paddingLeft: 16,
    paddingRight: 16,
  },
  iconContainer: {
    paddingBottom: 16,
  },
  icon: {
    tintColor: theme.colors.border,
  },
  title: {
    fontSize: theme.fontSizeLarge,
    color: theme.colors.greyDark,
    textAlign: 'center',
  },
  descriptionContainer: {
    paddingTop: 8,
  },
  description: {
    fontSize: theme.fontSize,
    color: theme.colors.greyMid,
    textAlign: 'center',
  },
});

export default NoItemsView;
