/* @flow */

import * as React from 'react';
import PropTypes from 'prop-types';

import { TouchableNativeFeedback, View } from 'react-native';

class NativeTouchableElement extends React.Component<*> {
  render() {
    const { children, borderless } = this.props;
    const background = borderless
      ? TouchableNativeFeedback.SelectableBackgroundBorderless()
      : TouchableNativeFeedback.SelectableBackground();
    // wrap the children in a view on Android
    return (
      <TouchableNativeFeedback {...this.props} background={background}>
        <View {...this.props}>{children}</View>
      </TouchableNativeFeedback>
    );
  }
  static defaultProps = {
    borderless: false,
  };
}

NativeTouchableElement.propTypes = {
  children: PropTypes.any,
  onPress: PropTypes.func,
  borderless: PropTypes.bool,
};

export default NativeTouchableElement;
