/* @flow */

import * as React from 'react';
import { Image, ScrollView } from 'react-native';

import { getConfig } from 'makeandship-js-common/src/modules/auth/config';

import * as PlatformConstants from '../../constants';
import AuthenticatedImage from './AuthenticatedImage';

const AuthenticatedImageZoomable = ({
  uri,
  ...rest
}: React.ElementProps<*>): React.Element<*> | null => {
  const scrollViewRef = React.useRef(null);
  const [size, setSize] = React.useState({ width: 100, height: 100 });
  const [dimensions, setDimensions] = React.useState({
    width: 100,
    height: 100,
  });

  const onLayout = React.useCallback((e: any) => {
    setDimensions(e.nativeEvent.layout);
  }, []);

  React.useEffect(() => {
    const initSize = async () => {
      const config = getConfig();
      const provider = config.provider;
      let token;
      try {
        await provider.updateToken();
        token = await provider.getToken();
      } catch (error) {
        // ignore token refesh error here
        // should be handled by provider callbacks
      }
      const headers = token
        ? {
            Authorization: `Bearer ${token}`,
          }
        : {};
      Image.getSizeWithHeaders(uri, headers, (width, height) => {
        setSize({ width, height });
      });
    };
    initSize();
  }, [uri]);

  React.useEffect(() => {
    if (PlatformConstants.IS_IOS) {
      if (scrollViewRef.current) {
        const scrollResponderRef = scrollViewRef.current.getScrollResponder();
        scrollResponderRef.scrollResponderZoomTo({
          x: 0,
          y: 0,
          width: size.width,
          height: size.height,
          animated: false,
        });
      }
    }
  }, [size.height, size.width]);

  // TODO: Android does not support zoom, so we just fit the image as large as possible

  const { width, height } = React.useMemo(() => {
    let { width, height } = size;
    if (PlatformConstants.IS_ANDROID) {
      // no zoom - size to fit
      const dimensionsAspect = dimensions.width / dimensions.height;
      const imageAspect = size.width / size.height;
      if (imageAspect > dimensionsAspect) {
        // landscape
        width = dimensions.width;
        height = size.height * (dimensions.width / size.width);
      } else {
        // portrait
        width = size.width * (dimensions.height / size.height);
        height = dimensions.height;
      }
    }
    return { width, height };
  }, [dimensions.height, dimensions.width, size]);

  // TODO: this assumes portrait view
  const minimumZoomScale = dimensions.width / size.width;
  return (
    <ScrollView
      ref={scrollViewRef}
      onLayout={onLayout}
      style={{ flex: 1 }}
      contentContainerStyle={{ width, height }}
      minimumZoomScale={minimumZoomScale}
      bouncesZoom
      centerContent
    >
      <AuthenticatedImage uri={uri} style={{ width, height }} {...rest} />
    </ScrollView>
  );
};

AuthenticatedImageZoomable.propTypes = {
  ...AuthenticatedImage.propTypes,
};

export default AuthenticatedImageZoomable;
