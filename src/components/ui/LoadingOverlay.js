/* @flow */

import * as React from 'react';
import PropTypes from 'prop-types';
import { View, Text, ActivityIndicator } from 'react-native';

import useTheme from '../../theme/useTheme';

const LoadingOverlay = ({
  title,
  style,
}: React.ElementProps<*>): React.Element<*> => {
  const theme = useTheme();
  const styles = makeStyles(theme);

  return (
    <View style={[styles.container, style]}>
      <ActivityIndicator size="large" />
      {title && <Text style={styles.title}>{title}</Text>}
    </View>
  );
};

const makeStyles = (theme) => ({
  container: {
    flex: 1,
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    backgroundColor: theme.colors.panelOverlay,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    paddingTop: 12,
    fontSize: theme.fontSize,
    color: theme.colors.greyDark,
  },
});

LoadingOverlay.propTypes = {
  style: PropTypes.any,
  title: PropTypes.string,
};

export default LoadingOverlay;
