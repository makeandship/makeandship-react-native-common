/* @flow */

import * as React from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Keyboard,
  Animated,
  Easing,
  StyleSheet,
  Dimensions,
} from 'react-native';

import withSafeArea from '../../hoc/withSafeArea';

type State = {
  displayHeight: Animated.Value,
  width: number,
  height: number,
};

class AppKeyboardResizingView extends React.Component<*, State> {
  _keyboardWillShowEvent: any;
  _keyboardWillHideEvent: any;
  _keyboardDidShowEvent: any;
  _keyboardDidHideEvent: any;

  constructor(props: any) {
    super(props);
    const width = Dimensions.get('window').width;
    const height = Dimensions.get('window').height;
    const displayHeight = props.includeSafeAreaInsets
      ? height - props.safearea.bottom
      : height;
    this.state = {
      displayHeight: new Animated.Value(displayHeight),
      width,
      height,
    };
  }

  onLayout = (e: any) => {
    const { displayHeight } = this.state;
    const { width, height } = e.nativeEvent.layout;
    const { safearea, includeSafeAreaInsets } = this.props;
    const toValue = includeSafeAreaInsets ? height - safearea.bottom : height;
    displayHeight.setValue(toValue);
    this.setState({
      width,
      height,
    });
  };

  componentDidMount() {
    this._keyboardWillShowEvent = Keyboard.addListener(
      'keyboardWillShow',
      this.keyboardWillShow
    );
    this._keyboardWillHideEvent = Keyboard.addListener(
      'keyboardWillHide',
      this.keyboardWillHide
    );
    this._keyboardDidShowEvent = Keyboard.addListener(
      'keyboardDidShow',
      this.keyboardDidShow
    );
    this._keyboardDidHideEvent = Keyboard.addListener(
      'keyboardDidHide',
      this.keyboardDidHide
    );
  }

  componentWillUnmount() {
    this._keyboardWillShowEvent && this._keyboardWillShowEvent.remove();
    this._keyboardWillHideEvent && this._keyboardWillHideEvent.remove();
    this._keyboardDidShowEvent && this._keyboardDidShowEvent.remove();
    this._keyboardDidHideEvent && this._keyboardDidHideEvent.remove();
  }

  keyboardWillShow = (e: any) => {
    const { displayHeight } = this.state;
    const { duration, easing, endCoordinates } = e;
    Animated.timing(displayHeight, {
      toValue: this.state.height - endCoordinates.height,
      duration: duration,
      easing: Easing[easing],
      useNativeDriver: false,
    }).start();
  };

  keyboardWillHide = (e: any) => {
    const { displayHeight } = this.state;
    const { safearea, includeSafeAreaInsets } = this.props;
    const { easing, duration } = e;
    const toValue = includeSafeAreaInsets
      ? this.state.height - safearea.bottom
      : this.state.height;
    Animated.timing(displayHeight, {
      toValue,
      duration: duration,
      easing: Easing[easing],
      useNativeDriver: false,
    }).start();
  };

  keyboardDidShow = () => {};

  keyboardDidHide = () => {};

  render() {
    // eslint-disable-next-line no-unused-vars
    const { children, safearea } = this.props;
    const { displayHeight } = this.state;
    const style = {
      height: displayHeight,
    };
    return (
      <View style={styles.container} onLayout={this.onLayout}>
        <Animated.View style={style}>{children}</Animated.View>
      </View>
    );
  }

  static defaultProps = {
    includeSafeAreaInsets: false,
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

AppKeyboardResizingView.propTypes = {
  children: PropTypes.node,
  includeSafeAreaInsets: PropTypes.bool.isRequired,
  safearea: PropTypes.object.isRequired,
};

export default withSafeArea(AppKeyboardResizingView);
