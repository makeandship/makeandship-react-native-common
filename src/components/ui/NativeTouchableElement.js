/* @flow */

import * as React from 'react';
import PropTypes from 'prop-types';

import {
  TouchableOpacity,
  TouchableHighlight,
  ColorPropType,
} from 'react-native';

class NativeTouchableElement extends React.Component<*> {
  render() {
    const { children, touchBackgroundColor } = this.props;
    const activeOpacity = 0.5;
    if (touchBackgroundColor) {
      return (
        <TouchableHighlight
          {...this.props}
          activeOpacity={activeOpacity}
          underlayColor={touchBackgroundColor}
        >
          {children}
        </TouchableHighlight>
      );
    }
    return (
      <TouchableOpacity {...this.props} activeOpacity={activeOpacity}>
        {children}
      </TouchableOpacity>
    );
  }
}

NativeTouchableElement.propTypes = {
  children: PropTypes.any,
  onPress: PropTypes.func,
  touchBackgroundColor: ColorPropType,
};

export default NativeTouchableElement;
