/* @flow */

import React from 'react';

import { storiesOf } from '@storybook/react-native';

import { StoryDecorator } from '../../utils';
import Button from './Button';

const variants = {
  default: {
    label: 'Lorem ipsum dolor',
  },
  outline: {
    label: 'Lorem ipsum dolor',
    outline: true,
    bold: false,
  },
  iconBefore: {
    label: 'Lorem ipsum dolor',
    iconBefore: 'chevron-circle-right',
    outline: true,
  },
  iconAfter: {
    label: 'Lorem ipsum dolor',
    iconAfter: 'chevron-circle-right',
    outline: true,
  },
  long: {
    label: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit',
    iconBefore: 'chevron-circle-right',
    iconAfter: 'chevron-circle-right',
    outline: true,
  },
  disabled: {
    label: 'Lorem ipsum dolor',
    outline: true,
    bold: false,
    disabled: true,
  },
};

storiesOf('Button', module)
  .addDecorator(StoryDecorator)
  .add('Default', () => <Button {...variants.default} />)
  .add('Outline light', () => <Button {...variants.outline} />)
  .add('Icon before', () => <Button {...variants.iconBefore} />)
  .add('Icon after', () => <Button {...variants.iconAfter} />)
  .add('Long', () => <Button {...variants.long} />)
  .add('Disabled', () => <Button {...variants.disabled} />);
