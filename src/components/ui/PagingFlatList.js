/* @flow */

import * as React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, FlatList, Dimensions } from 'react-native';

type State = {
  dimensions: {
    x: number,
    y: number,
    width: number,
    height: number,
  },
  index: number,
  isFirst: boolean,
  isLast: boolean,
};

class PagingFlatList extends React.Component<*, State> {
  _scrollViewRef: FlatList<*> | null;
  _scrollWithUserInteraction: boolean = false;

  state = {
    dimensions: {
      x: 0,
      y: 0,
      width: Dimensions.get('window').width,
      height: Dimensions.get('window').height,
    },
    index: 0,
    isFirst: true,
    isLast: false,
  };

  constructor(props: any) {
    super(props);
    if (props.index) {
      this.state.index = props.index;
    }
  }

  getItemLayout = (data, index) => {
    const {
      dimensions: { width },
    } = this.state;
    return {
      length: width,
      offset: width * index,
      index,
    };
  };

  renderItem = (args) => {
    const {
      dimensions: { width, height },
    } = this.state;
    if (!width || !height) {
      return null;
    }
    const itemContainerStyle = {
      width,
      height,
    };
    return (
      <View style={itemContainerStyle}>{this.props.renderItem(args)}</View>
    );
  };

  onLayout = (e: any) => {
    const dimensions = e.nativeEvent.layout;
    this.setState({
      dimensions,
    });
  };

  scrollViewRef = (ref: FlatList<*> | null) => {
    this._scrollViewRef = ref;
  };

  onScrollBeginDrag = () => {
    this._scrollWithUserInteraction = true;
  };

  render() {
    /* eslint-disable no-unused-vars */
    const {
      ref,
      style,
      getItemLayout,
      renderItem,
      onMomentumScrollEnd,
      onScrollBeginDrag,
      onScrollEndDrag,
      contentOffset,
      pagingEnabled,
      onPageIndexChange,
      ...rest
    } = this.props;
    /* eslint-enable no-unused-vars */
    const {
      dimensions: { width },
    } = this.state;
    const initialContentOffset = { x: this.state.index * width, y: 0 };
    return (
      <FlatList
        onLayout={this.onLayout}
        ref={this.scrollViewRef}
        style={styles.container}
        getItemLayout={this.getItemLayout}
        renderItem={this.renderItem}
        onMomentumScrollEnd={this.onMomentumScrollEnd}
        onScrollBeginDrag={this.onScrollBeginDrag}
        onScrollEndDrag={this.onScrollEndDrag}
        contentOffset={initialContentOffset}
        pagingEnabled
        {...rest}
      />
    );
  }

  scrollToIndex = (newIndex: number, animated: boolean = true) => {
    const {
      dimensions: { width },
    } = this.state;
    const index = this.sanitizeIndex(newIndex);
    this._scrollViewRef &&
      this._scrollViewRef.scrollToOffset({
        offset: width * index,
        animated,
      });
  };

  sanitizeIndex = (index) => {
    const { data } = this.props;
    index = Math.min(index, data.length - 1);
    index = Math.max(index, 0);
    return index;
  };

  onMomentumScrollEnd = (e: any) => {
    // scroll visually completed, so update state and fire onChange event
    // as appropriate
    const {
      dimensions: { width },
    } = this.state;
    if (!e.nativeEvent.contentOffset) {
      e.nativeEvent.contentOffset = {
        x: e.nativeEvent.position * width,
      };
    }
    const index = Math.floor(e.nativeEvent.contentOffset.x / width);
    if (!this._scrollWithUserInteraction) {
      // don't update until we reach the index provided in props
      // in case it is changing rapidly e.g. user repeatedly clicking
      // a 'next' button
      if (index !== this.props.index) {
        return;
      }
    }
    this._scrollWithUserInteraction = false;
    if (index === this.state.index) {
      return;
    }
    const { onPageIndexChange } = this.props;
    const { data } = this.props;
    const isLast = index === data.length - 1;
    const isFirst = index === 0;
    const state = {
      index,
      isFirst,
      isLast,
    };
    this.setState(state);
    onPageIndexChange && onPageIndexChange(state);
  };

  componentDidUpdate = (prevProps: any) => {
    if (this.props.index !== prevProps.index) {
      // scroll visually - state will update once finished
      this.scrollToIndex(this.props.index);
    }
  };

  static defaultProps = {
    keyExtractor: (item, index) => `${index}`,
    horizontal: true,
    index: 0,
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

PagingFlatList.propTypes = {
  keyExtractor: PropTypes.func,
  horizontal: PropTypes.bool,
  data: PropTypes.array.isRequired,
  renderItem: PropTypes.func.isRequired,
  onPageIndexChange: PropTypes.func,
  index: PropTypes.number,
};

export default PagingFlatList;
