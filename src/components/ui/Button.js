/* @flow */

import * as React from 'react';
import PropTypes from 'prop-types';
import { View, Text } from 'react-native';

import NativeTouchableElement from './NativeTouchableElement';
import AppIcon from './AppIcon';
import useTheme from '../../theme/useTheme';

const Button = ({
  tintColor,
  textColor,
  onPress,
  label,
  outline = false,
  iconBefore,
  iconAfter,
  disabled,
  bold = true,
}: React.ElementProps<*>): React.Element<*> => {
  const theme = useTheme();
  const styles = makeStyles(theme);
  tintColor = tintColor || theme.colors.primary;
  textColor = textColor || theme.colors.white;
  const containerStyle = {};
  const textStyle = {};
  if (outline) {
    containerStyle.borderColor = tintColor;
    containerStyle.borderWidth = 1;
    textStyle.color = tintColor;
  } else {
    containerStyle.backgroundColor = tintColor;
    textStyle.color = textColor;
  }
  if (disabled) {
    containerStyle.opacity = 0.3;
  }
  const content = (
    <View style={[styles.container, containerStyle]}>
      <Text style={[bold ? styles.textBold : styles.text, textStyle]}>
        {iconBefore && (
          <Text>
            <AppIcon name={iconBefore} />{' '}
          </Text>
        )}
        {label}
        {iconAfter && (
          <Text>
            {' '}
            <AppIcon name={iconAfter} />
          </Text>
        )}
      </Text>
    </View>
  );
  if (disabled) {
    return content;
  }
  return (
    <NativeTouchableElement onPress={onPress}>{content}</NativeTouchableElement>
  );
};

const makeStyles = (theme) => ({
  container: {
    ...theme.padding.button,
    minHeight: 44,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: theme.roundness,
  },
  text: {
    textAlign: 'center',
    fontSize: theme.fontSize,
  },
  textBold: {
    ...theme.fonts.bold,
    textAlign: 'center',
    fontSize: theme.fontSize,
  },
});

Button.propTypes = {
  onPress: PropTypes.func,
  tintColor: PropTypes.string,
  textColor: PropTypes.string,
  outline: PropTypes.bool,
  label: PropTypes.string,
  iconBefore: PropTypes.string,
  iconAfter: PropTypes.string,
  disabled: PropTypes.bool,
};

export default Button;
