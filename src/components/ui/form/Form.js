/* @flow */

import * as React from 'react';
import PropTypes from 'prop-types';
import { FlatList, Text } from 'react-native';
import { KeyboardAwareFlatList } from 'react-native-keyboard-aware-scroll-view';

import useTheme from '../../../theme/useTheme';

import SectionTitle from './SectionTitle';
import SectionFooter from './SectionFooter';
import FormFooter from './FormFooter';
import TextInputCell from './cells/TextInputCell';
import SectionSeparator from './SectionSeparator';

const Form = ({
  __debug__,
  cells,
  keyboardAware = true,
  formData,
  ...rest
}: React.ElementProps<*>): React.Element<*> => {
  const theme = useTheme();
  const styles = makeStyles(theme);

  const textInputCellRefs = React.useRef({});

  const keyExtractor = (item: any, index: number) => `${index}`;

  const renderItem = ({ item }: { item: any }) => {
    return item;
  };

  const addPropsToCells = (cells: Array<React.Element<*>>): Array<any> => {
    const cellsWithSections = [];
    let lastInSection = false;
    let lastInForm = false;
    textInputCellRefs.current = {};
    cells.forEach((cell, index) => {
      const CellComponent = cell.type;

      let additionalProps = {};

      // separator style
      lastInSection = false;
      lastInForm = index === cells.length - 1;
      // determine if it's the last non-title cell
      if (index < cells.length - 1) {
        const nextElement = cells[index + 1];
        const NextCellComponent = nextElement.type;
        if (
          SectionTitle === NextCellComponent ||
          SectionFooter === NextCellComponent ||
          SectionSeparator === NextCellComponent
        ) {
          lastInSection = true;
        }
      } else {
        lastInSection = true;
      }
      if (
        SectionTitle === CellComponent ||
        SectionSeparator === CellComponent ||
        lastInSection
      ) {
        if (SectionFooter === CellComponent && lastInForm) {
          // no separator
        } else {
          // full width separator
          additionalProps = { ...additionalProps, separatorFullWidth: true };
        }
      } else {
        // default cell separator
      }

      // keyboard 'next' behaviour
      if (TextInputCell === CellComponent) {
        additionalProps = {
          ...additionalProps,
          onFocusFunc: (focus) => {
            textInputCellRefs.current[`${index}`] = focus;
          },
        };
        if (!cell.props.returnKeyType || cell.props.returnKeyType === 'next') {
          const originalOnSubmitEditing = cell.props.onSubmitEditing;
          additionalProps = {
            ...additionalProps,
            onSubmitEditing: (e) => {
              if (
                !cell.props.returnKeyType ||
                cell.props.returnKeyType === 'next'
              ) {
                const allIndexes = Object.keys(textInputCellRefs.current);
                const currentIndex = allIndexes.indexOf(`${index}`);
                const nextIndex = currentIndex + 1;
                const nextIndexRowIndex = allIndexes[nextIndex];
                if (nextIndexRowIndex) {
                  const focus =
                    textInputCellRefs.current[`${nextIndexRowIndex}`];
                  focus && focus();
                }
              }
              // call the original if set
              originalOnSubmitEditing && originalOnSubmitEditing(e);
            },
            blurOnSubmit: false,
          };
        }
      }

      if (Object.keys(additionalProps).length > 0) {
        cell = React.cloneElement(cell, additionalProps);
      }
      cellsWithSections.push(cell);
      if (lastInForm) {
        cellsWithSections.push(<FormFooter />);
      }
    });
    return cellsWithSections;
  };

  const cellsWithProps = addPropsToCells(cells);
  const Component = keyboardAware ? KeyboardAwareFlatList : FlatList;
  return (
    <React.Fragment>
      {__DEV__ && __debug__ && <Text>{JSON.stringify(formData, null, 2)}</Text>}
      <Component
        style={styles.container}
        data={cellsWithProps}
        renderItem={renderItem}
        keyExtractor={keyExtractor}
        {...rest}
      />
    </React.Fragment>
  );
};

const makeStyles = (theme) => ({
  container: {
    flex: 1,
    backgroundColor: theme.colors.background,
  },
});

Form.propTypes = {
  cells: PropTypes.arrayOf(PropTypes.node).isRequired,
  keyboardAware: PropTypes.bool,
};

export default Form;
