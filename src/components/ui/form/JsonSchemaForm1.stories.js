/* @flow */

import * as React from 'react';

import { storiesOf } from '@storybook/react-native';

import { ConnectedStorybook, StoryDecorator } from '../../../utils';

import {
  FormCell,
  TextInputCell,
  FileInputBinaryCell,
  ButtonCell,
  SelectCell,
  SegmentedCell,
} from './cells';

import SectionTitle from './SectionTitle';
import JsonSchemaForm from './JsonSchemaForm';
import JsonSchemaFormWithValidation from './JsonSchemaFormWithValidation';

import incidentSchema from './static/incident';

class CustomPassword extends React.PureComponent<*> {
  render() {
    const { title, value } = this.props;
    return <FormCell title={`${title} (CustomPassword)`} detailText={value} />;
  }
}

const dynamicSchema = {
  title: 'Person',
  type: 'object',
  properties: {
    'Do you have any pets?': {
      type: 'string',
      enum: ['No', 'Yes: One', 'Yes: More than one'],
      default: 'Yes: One',
    },
    helpText: {
      title: 'A null field',
      description:
        'Null fields like this are great for adding extra information',
      type: 'null',
    },
  },
  required: ['Do you have any pets?'],
  dependencies: {
    'Do you have any pets?': {
      oneOf: [
        {
          properties: {
            'Do you have any pets?': {
              enum: ['No'],
            },
          },
        },
        {
          properties: {
            'Do you have any pets?': {
              enum: ['Yes: One'],
            },
            'How old is your pet?': {
              type: 'number',
            },
          },
          required: ['How old is your pet?'],
        },
        {
          properties: {
            'Do you have any pets?': {
              enum: ['Yes: More than one'],
            },
            'Do you want to get rid of any?': {
              type: 'boolean',
            },
          },
          required: ['Do you want to get rid of any?'],
        },
      ],
    },
  },
};

const dynamicUiSchema = {};

const arraySchema = {
  type: 'object',
  title: 'Array',
  definitions: {
    Thing: {
      type: 'object',
      // title: 'Thing',
      properties: {
        name: {
          title: 'Name',
          description: 'The name of the thing',
          type: 'string',
          default: 'Default name',
        },
      },
    },
  },
  properties: {
    listOfThings: {
      type: 'array',
      title: 'A list of things',
      items: {
        $ref: '#/definitions/Thing',
      },
    },
  },
};

const arrayUiSchema = {
  listOfThings: {
    name: {
      'ui:widget': ({
        title,
        ...rest
      }: React.ElementProps<*>): React.Element<*> => (
        <TextInputCell title={`Custom widget (${title})`} {...rest} />
      ),
    },
  },
};

const loginSchema = {
  $schema: 'http://json-schema.org/draft-07/schema#',
  title: 'Log in',
  type: 'object',
  properties: {
    email: {
      type: 'string',
      format: 'email',
      title: 'Email',
    },
    password: {
      type: 'string',
      title: 'Password',
    },
  },
  required: ['email', 'password'],
};

const loginUiSchema = {
  email: {
    'ui:placeholder': 'sam.smith@example.com',
    'ui:autofocus': true,
  },
  password: {
    'ui:widget': 'password',
    'ui:returnkeytype': 'go',
    'ui:widgetadditionalprops': {
      onSubmitEditing: () => {
        console.log('onSubmitEditing');
      },
    },
  },
};

const enumSchema = {
  title: 'Person',
  type: 'object',
  properties: {
    'Do you have any pets?': {
      type: 'string',
      enum: ['No', 'Yes: One', 'Yes: More than one'],
      default: 'No',
    },
    'Do you want to get rid of any?': {
      type: 'boolean',
    },
    involved: {
      type: 'array',
      items: {
        type: 'object',
        properties: {
          where: {
            type: 'object',
            title: 'Where',
            properties: {
              site: {
                title: 'Site',
                type: 'string',
                enum: [
                  'CXH',
                  'EXTORG',
                  'HH',
                  'MOUHOS',
                  'QCCH',
                  'SATLOC',
                  'SMH',
                  'HILHOS',
                  'TRUSTW',
                  'WESMID',
                  'WEH',
                ],
                enumNames: [
                  'Charing Cross Hospital',
                  'External Healthcare Provider',
                  'Hammersmith Hospital',
                  'Mount Vernon Hospital',
                  "Queen Charlotte's and Chelsea Hospital",
                  'Satellite Location',
                  "St. Mary's Hospital",
                  'The Hillingdon Hospital',
                  'TrustWide',
                  'West Middlesex University Hospital',
                  'Western Eye Hospital',
                ],
              },
            },
          },
        },
      },
    },
    attachments: {
      type: 'array',
      items: {
        type: 'string',
        format: 'binary',
      },
    },
  },
};

const enumReadonlyUiSchema = {
  'ui:readonly': true,
};

const enumSelectUiSchema = {
  'Do you have any pets?': {
    'ui:widget': SelectCell,
  },
  involved: {
    items: {
      where: {
        site: {
          'ui:widget': SelectCell,
        },
      },
    },
  },
  attachments: {
    items: {
      'ui:widget': FileInputBinaryCell,
    },
  },
};

const enumSegmentedUiSchema = {
  'Do you have any pets?': {
    'ui:widget': SegmentedCell,
  },
  involved: {
    items: {
      where: {
        site: {
          'ui:widget': SegmentedCell,
        },
      },
    },
  },
  attachments: {
    items: {
      'ui:widget': FileInputBinaryCell,
    },
  },
};

const variants = {
  login: {
    schema: loginSchema,
    uiSchema: loginUiSchema,
    submitTitle: 'Log in',
    keyboardShouldPersistTaps: 'always',
  },
  loginWithInitialErrors: {
    schema: loginSchema,
    uiSchema: loginUiSchema,
    formData: {
      email: 'exists@example.com',
    },
    error: {
      errors: {
        email: 'is already in use',
      },
    },
    onSubmit: (formData) => {
      console.log('onSubmit - passed validation:', formData);
    },
    onErrors: (errors) => {
      console.log('onErrors', errors);
      return errors;
    },
    onChange: (formData) => {
      console.log('onChange', formData);
      return formData;
    },
    footerCells: ({ submit }) => {
      return [
        <SectionTitle key={'spacer'} title={'Custom submit button'} />,
        <ButtonCell
          key={'submit-button'}
          onPress={submit}
          title={'Custom submit button'}
        />,
      ];
    },
    submitTitle: 'Log in',
    keyboardShouldPersistTaps: 'always',
  },
  loginWithAsyncData: {
    schema: loginSchema,
    uiSchema: loginUiSchema,
    formData: {
      email: 'exists@example.com',
    },
    asyncFormData: {
      email: 'exists@example.com.async',
    },
    asyncError: {
      errors: {
        email: 'is already in use (async)',
      },
    },
    submitTitle: 'Log in',
  },
  loginWithCustomComponent: {
    schema: loginSchema,
    uiSchema: {
      email: {
        // eslint-disable-next-line
        'ui:widget': ({ title, value, ...rest }) => (
          <FormCell
            title={`${title} (custom)`}
            detailText={`${value} (custom)`}
            {...rest}
          />
        ),
      },
      password: {
        'ui:widget': CustomPassword,
      },
    },
    formData: {
      email: 'exists@example.com',
      password: 'Password value',
    },
    error: {
      errors: {
        email: 'is already in use',
      },
    },
    submitTitle: 'Log in',
    keyboardShouldPersistTaps: 'always',
  },
  array: {
    schema: arraySchema,
    uiSchema: arrayUiSchema,
    formData: {
      listOfThings: [{ name: 'Lorem' }, { name: 'Ipsum' }],
    },
    keyboardShouldPersistTaps: 'always',
  },
  incident: {
    schema: incidentSchema,
    uiSchema: {},
  },
  incidentMinimumTitleDepth: {
    schema: incidentSchema,
    minimumTitleDepth: 2,
    uiSchema: {},
  },
  incidentReadonly: {
    schema: incidentSchema,
    uiSchema: {
      'ui:readonly': true,
    },
  },
  incidentDisabled: {
    schema: incidentSchema,
    uiSchema: {
      'ui:disabled': true,
    },
  },
  dynamic: {
    __debug__: true,
    schema: dynamicSchema,
    uiSchema: dynamicUiSchema,
  },
  readonly: {
    schema: enumSchema,
    uiSchema: enumReadonlyUiSchema,
  },
  select: {
    schema: enumSchema,
    uiSchema: enumSelectUiSchema,
  },
  segmented: {
    schema: enumSchema,
    uiSchema: enumSegmentedUiSchema,
  },
};

export type State = {
  formData: any,
  error: any,
};

export class JsonSchemaFormWithPreservingFormData extends React.Component<
  *,
  State
> {
  constructor(props: any) {
    super(props);
    this.state = { formData: props.formData || {}, error: props.error || {} };
  }
  onChange = (formData: any) => {
    this.setState({ formData });
    console.log('formData:', JSON.stringify(formData, null, 2));
  };
  render() {
    /* eslint-disable no-unused-vars */
    const { formData, error, onChange, ...rest } = this.props;
    /* eslint-enable no-unused-vars */
    return (
      <JsonSchemaForm
        onChange={this.onChange}
        formData={this.state.formData}
        error={this.state.error}
        {...rest}
      />
    );
  }
}

export class JsonSchemaFormWithAsyncData extends React.Component<*, State> {
  constructor(props: any) {
    super(props);
    this.state = { formData: props.formData || {}, error: props.error || {} };
  }
  onSubmit = (formData: any) => {
    this.setState({ formData });
    console.log('formData:', JSON.stringify(formData, null, 2));
    setTimeout(() => {
      console.log('Setting async data');
      this.setState({
        formData: this.props.asyncFormData,
        error: this.props.asyncError,
      });
    }, 500);
  };
  render() {
    /* eslint-disable no-unused-vars */
    const { formData, error, onSubmit, ...rest } = this.props;
    /* eslint-enable no-unused-vars */
    return (
      <JsonSchemaFormWithValidation
        onSubmit={this.onSubmit}
        formData={this.state.formData}
        error={this.state.error}
        {...rest}
      />
    );
  }
}

storiesOf('JsonSchemaForm 1', module)
  .addDecorator(StoryDecorator)
  .addDecorator(ConnectedStorybook)
  .add('Login with validation', () => (
    <JsonSchemaFormWithValidation {...variants.login} />
  ))
  .add('Login with custom component', () => (
    <JsonSchemaFormWithValidation {...variants.loginWithCustomComponent} />
  ))
  .add('Login with initial state', () => (
    <JsonSchemaFormWithValidation {...variants.loginWithInitialErrors} />
  ))
  .add('Login with async state', () => (
    <JsonSchemaFormWithAsyncData {...variants.loginWithAsyncData} />
  ))
  .add('Incident with validation', () => (
    <JsonSchemaFormWithValidation {...variants.incident} />
  ))
  .add('Incident with minimum title depth', () => (
    <JsonSchemaFormWithValidation {...variants.incidentMinimumTitleDepth} />
  ))
  .add('Incident readonly', () => (
    <JsonSchemaFormWithValidation {...variants.incidentReadonly} />
  ))
  .add('Incident disabled', () => (
    <JsonSchemaFormWithValidation {...variants.incidentDisabled} />
  ))
  .add('Array', () => (
    <JsonSchemaFormWithPreservingFormData {...variants.array} />
  ))
  .add('Dynamic', () => (
    <JsonSchemaFormWithPreservingFormData {...variants.dynamic} />
  ))
  .add('Readonly', () => (
    <JsonSchemaFormWithPreservingFormData {...variants.readonly} />
  ))
  .add('Select', () => (
    <JsonSchemaFormWithPreservingFormData {...variants.select} />
  ))
  .add('Segmented', () => (
    <JsonSchemaFormWithPreservingFormData {...variants.segmented} />
  ));
