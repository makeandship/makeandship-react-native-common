/* @flow */

import * as React from 'react';
import PropTypes from 'prop-types';
import { Text } from 'react-native';

import useTheme from '../../../theme/useTheme';

const SectionFooter = ({ title }: React.ElementProps<*>): React.Element<*> => {
  const theme = useTheme();
  const styles = makeStyles(theme);
  return <Text style={styles.sectionFooterText}>{title}</Text>;
};

const makeStyles = (theme) => ({
  sectionFooterText: {
    paddingLeft: theme.padding.cell.paddingLeading,
    paddingRight: theme.padding.cell.paddingLeading,
    paddingTop: 8,
    paddingBottom: 8,
    backgroundColor: theme.colors.background,
    textAlign: 'center',
    fontSize: theme.fontSizeExtraSmall,
    color: theme.colors.sectionTitle,
  },
});

SectionFooter.propTypes = {
  title: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
};

export default SectionFooter;
