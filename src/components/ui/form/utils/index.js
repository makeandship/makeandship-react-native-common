/* @flow */

import { isString } from 'makeandship-js-common/src/utils/is';

export const hasString = (s) => isString(s) && s.length > 0;
