/* @flow */

import * as React from 'react';

import { storiesOf } from '@storybook/react-native';

import { ConnectedStorybook, StoryDecorator } from '../../../utils';

import {
  SelectCell,
  SelectCellWithTitle,
  SwitchCell,
  SegmentedCell,
} from './cells';

import { JsonSchemaFormWithPreservingFormData } from './JsonSchemaForm1.stories';

const incidentSchemaStep = {
  type: 'object',
  $schema: 'http://json-schema.org/draft-07/schema#',
  definitions: {
    SafeguardingInformation: {
      type: 'object',
      title: 'Safeguarding Information',
      properties: {
        children: {
          title: "Is this Children's safeguarding?",
          type: 'string',
          enum: ['Y', 'N'],
          enumNames: ['Yes', 'No'],
        },
      },
      dependencies: {
        children: {
          oneOf: [
            {
              properties: {
                children: {
                  enum: ['N'],
                },
              },
            },
            {
              properties: {
                children: {
                  enum: ['Y'],
                },
                safeguardingForm: {
                  title: "Has a Children's safeguarding form been completed?",
                  type: 'string',
                  enum: ['Y', 'N'],
                  enumNames: ['Yes', 'No'],
                },
              },
            },
          ],
        },
      },
    },
  },
  properties: {
    safeguardingInformation: {
      $ref: '#/definitions/SafeguardingInformation',
    },
  },
};

const schema = {
  type: 'object',
  properties: {
    searchTitle: {
      type: 'null',
      title: 'Search',
    },
    s: {
      type: 'string',
      title: 'Keyword search',
    },
    filterTitle: {
      type: 'null',
      title: 'Filter',
    },
    drug_class: {
      title: 'Drug class',
      type: 'string',
      enum: [
        'Cromoglicate and related therapy',
        'ICS',
        'ICS + LABA',
        'ICS + LABA + LAMA',
        'LABA',
        'LAMA',
        'LAMA + LABA',
        'SABA',
        'SAMA',
      ],
    },
    drug_name: {
      title: 'Drug name',
      type: 'string',
      enum: [
        'Aclidinium bromide',
        'Aclidinium bromide + Formoterol',
        'Beclometasone',
        'Beclometasone + Formoterol',
        'Beclometasone + Formoterol + Glycopyrronium',
        'Budesonide',
        'Budesonide + Formoterol',
        'Ciclesonide',
        'Fluticasone',
        'Fluticasone + Formoterol',
        'Fluticasone + Salmeterol',
        'Fluticasone + Vilanterol',
        'Formoterol',
        'Glycopyrronium',
        'Indacaterol',
        'Indacaterol + Glycopyrronium',
        'Ipratropium',
        'Mometasone',
        'Nedocromil',
        'Olodaterol',
        'Salbutamol',
        'Salmeterol',
        'Sodium cromoglicate',
        'Terbutaline',
        'Tiotropium',
        'Tiotropium + Olodaterol',
        'Umeclidinium bromide',
        'Umeclidinium bromide + Vilanterol',
      ],
    },
    device_type: {
      title: 'Device type',
      type: 'string',
      enum: [
        'Dry powder inhaler (DPI)',
        'Multi-dose solution for inhalation',
        'Pressurised aerosol inhaler (MDI)',
      ],
    },
    licenceTitle: {
      type: 'null',
      title: 'Licence',
    },
    paediatric_asthma_licence: {
      title: 'Paediatric Asthma Licence',
      type: 'boolean',
    },
    adolescent_asthma_licence: {
      title: 'Adolescent Asthma Licence',
      type: 'boolean',
    },
    adult_asthma_licence: {
      title: 'Adult Asthma Licence',
      type: 'boolean',
    },
    copd_licence: {
      title: 'COPD Licence',
      type: 'boolean',
    },
    featuresTitle: {
      type: 'null',
      title: 'Features',
    },
    dose_counter: {
      title: 'Dose counter',
      type: 'boolean',
    },
    device_activation_mechanism: {
      title: 'Breath activated device',
      type: 'boolean',
    },
  },
};

const uiSchema = {
  s: {
    'ui:returnkeytype': 'go',
    'ui:widgetadditionalprops': {
      placeholder: 'Keyword search',
      autoCapitalize: 'none',
      autoCorrect: false,
    },
  },
  drug_class: {
    'ui:widget': SelectCell,
  },
  drug_name: {
    'ui:widget': SelectCell,
  },
  device_type: {
    'ui:widget': SelectCell,
  },
  paediatric_asthma_licence: {
    'ui:widget': SwitchCell,
  },
  adolescent_asthma_licence: {
    'ui:widget': SwitchCell,
  },
  adult_asthma_licence: {
    'ui:widget': SwitchCell,
  },
  copd_licence: {
    'ui:widget': SwitchCell,
  },
  dose_counter: {
    'ui:widget': SwitchCell,
  },
  device_activation_mechanism: {
    'ui:widget': SwitchCell,
  },
};

const compactUiSchema = {
  s: {
    'ui:returnkeytype': 'go',
    'ui:widgetadditionalprops': {
      placeholder: 'Keyword search',
      autoCapitalize: 'none',
      autoCorrect: false,
    },
  },
  drug_class: {
    'ui:widget': SelectCellWithTitle,
  },
  drug_name: {
    'ui:widget': SelectCellWithTitle,
  },
  device_type: {
    'ui:widget': SelectCellWithTitle,
  },
  paediatric_asthma_licence: {
    'ui:widget': SwitchCell,
  },
  adolescent_asthma_licence: {
    'ui:widget': SwitchCell,
  },
  adult_asthma_licence: {
    'ui:widget': SwitchCell,
  },
  copd_licence: {
    'ui:widget': SwitchCell,
  },
  dose_counter: {
    'ui:widget': SwitchCell,
  },
  device_activation_mechanism: {
    'ui:widget': SwitchCell,
  },
};

const doseSchema = {
  type: 'object',
  properties: {
    doseTitle: {
      type: 'null',
      title: 'Dose',
    },
    puffs: {
      type: 'string',
      title: 'Puffs',
      enum: ['1', '2', '3', '4'],
    },
    when: {
      type: 'string',
      title: 'When',
      enum: ['Regular', 'As required'],
    },
  },
  dependencies: {
    when: {
      oneOf: [
        {
          properties: {
            when: {
              enum: ['Regular'],
            },
          },
        },
        {
          properties: {
            when: { enum: ['As required'] },
            perDay: {
              type: 'string',
              title: 'Per day',
              enum: ['1', '2', '3', '4'],
            },
          },
        },
      ],
    },
    perDay: {
      oneOf: [
        {
          properties: {
            perDay: {
              enum: ['1'],
            },
            remindersTitle: {
              type: 'null',
              title: 'Reminders',
            },
            reminder1: {
              type: 'string',
              format: 'time',
              title: 'First dose',
            },
          },
        },
        {
          properties: {
            perDay: {
              enum: ['2'],
            },
            remindersTitle: {
              type: 'null',
              title: 'Reminders',
            },
            reminder1: {
              type: 'string',
              format: 'time',
              title: 'First dose',
            },
            reminder2: {
              type: 'string',
              format: 'time',
              title: 'Second dose',
            },
          },
        },
        {
          properties: {
            perDay: {
              enum: ['3'],
            },
            remindersTitle: {
              type: 'null',
              title: 'Reminders',
            },
            reminder1: {
              type: 'string',
              format: 'time',
              title: 'First dose',
            },
            reminder2: {
              type: 'string',
              format: 'time',
              title: 'Second dose',
            },
            reminder3: {
              type: 'string',
              format: 'time',
              title: 'Third dose',
            },
          },
        },
        {
          properties: {
            perDay: {
              enum: ['4'],
            },
            remindersTitle: {
              type: 'null',
              title: 'Reminders',
            },
            reminder1: {
              type: 'string',
              format: 'time',
              title: 'First dose',
            },
            reminder2: {
              type: 'string',
              format: 'time',
              title: 'Second dose',
            },
            reminder3: {
              type: 'string',
              format: 'time',
              title: 'Third dose',
            },
            reminder4: {
              type: 'string',
              format: 'time',
              title: 'Fourth dose',
            },
          },
        },
      ],
    },
  },
};

const doseUiSchema = {
  puffs: {
    'ui:widget': SegmentedCell,
  },
  when: {
    'ui:widget': SegmentedCell,
  },
  perDay: {
    'ui:widget': SegmentedCell,
  },
};

const variants = {
  default: {
    schema: schema,
    uiSchema: uiSchema,
  },
  compact: {
    schema: schema,
    uiSchema: compactUiSchema,
  },
  dose: {
    schema: doseSchema,
    uiSchema: doseUiSchema,
  },
  dynamicUsingRef: {
    schema: incidentSchemaStep,
  },
};

storiesOf('JsonSchemaForm 2', module)
  .addDecorator(StoryDecorator)
  .addDecorator(ConnectedStorybook)
  .add('Default select and switch', () => (
    <JsonSchemaFormWithPreservingFormData {...variants.default} />
  ))
  .add('Compact select and switch', () => (
    <JsonSchemaFormWithPreservingFormData {...variants.compact} />
  ))
  .add('Segmented and time', () => (
    <JsonSchemaFormWithPreservingFormData __debug__ {...variants.dose} />
  ))
  .add('Dynamic using ref', () => (
    <JsonSchemaFormWithPreservingFormData
      __debug__
      {...variants.dynamicUsingRef}
    />
  ));
