/* @flow */

import * as React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';

import DateTimePicker from 'react-native-modal-datetime-picker';

import { formatDate } from '../../../../utils/date';
import FormCell from './FormCell';

type State = {
  isVisible: boolean,
};

class DateTimeCell extends React.PureComponent<*, State> {
  state = {
    isVisible: false,
  };

  onDatePicked = (date: Date) => {
    const { onChange } = this.props;
    this.setState({ isVisible: false });
    onChange(date);
  };

  onPress = () => {
    this.setState({ isVisible: true });
  };

  onCancel = () => {
    this.setState({ isVisible: false });
  };

  formatValue = (value: Date) => {
    if (!value) {
      return;
    }
    const { formatValue, mode } = this.props;
    if (formatValue) {
      return formatValue(value);
    }
    // https://date-fns.org/v2.0.0-alpha.9/docs/format
    let format;
    if (mode === 'datetime') {
      format = 'P HH:mm';
    } else if (mode === 'date') {
      format = 'P';
    } else {
      format = 'HH:mm';
    }
    return formatDate(value, format);
  };

  render() {
    const { isVisible } = this.state;
    /* eslint-disable no-unused-vars */
    const {
      value,
      onChange,
      minuteInterval,
      minimumDate,
      maximumDate,
      mode,
      formatValue,
      display,
      readonly,
      ...rest
    } = this.props;
    /* eslint-enable no-unused-vars */
    const detailText = this.formatValue(value);
    if (readonly) {
      return <FormCell readonly detailText={detailText} {...rest} />;
    }
    return (
      <View>
        <FormCell
          onPress={this.onPress}
          disclosure
          detailText={detailText}
          {...rest}
        />
        <DateTimePicker
          isVisible={isVisible}
          onConfirm={this.onDatePicked}
          onCancel={this.onCancel}
          confirmTextIOS={'OK'}
          headerTextIOS={this.props.title}
          minuteInterval={minuteInterval}
          minimumDate={minimumDate}
          maximumDate={maximumDate}
          mode={mode}
          date={value}
          display={display}
        />
      </View>
    );
  }

  static defaultProps = {
    minuteInterval: 1,
    mode: 'datetime',
    display: 'spinner',
  };

  static rendersTitle = true;
}

DateTimeCell.propTypes = {
  onChange: PropTypes.func.isRequired,
  minuteInterval: PropTypes.number,
  value: PropTypes.instanceOf(Date),
  minimumDate: PropTypes.instanceOf(Date),
  maximumDate: PropTypes.instanceOf(Date),
  mode: PropTypes.oneOf(['date', 'time', 'datetime']),
  formatValue: PropTypes.func,
  display: PropTypes.string,
};

export default DateTimeCell;
