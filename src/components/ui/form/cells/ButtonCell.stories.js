/* @flow */

import React from 'react';

import { storiesOf } from '@storybook/react-native';

import { StoryDecorator } from '../../../../utils';
import ButtonCell from './ButtonCell';

const onPress = (e) => {
  console.log('onPress', e);
};

const variants = {
  default: {
    title: 'Lorem',
    onPress,
  },
  destructive: {
    title: 'Lorem ipsum',
    onPress,
    destructive: true,
  },
  disabled: {
    title: 'Lorem',
    onPress,
    disabled: true,
  },
};

storiesOf('ButtonCell', module)
  .addDecorator(StoryDecorator)
  .add('Default', () => <ButtonCell {...variants.default} />)
  .add('Destructive', () => <ButtonCell {...variants.destructive} />)
  .add('Disabled', () => <ButtonCell {...variants.disabled} />);
