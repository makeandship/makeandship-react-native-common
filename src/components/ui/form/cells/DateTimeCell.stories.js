/* @flow */

import React from 'react';

import { storiesOf } from '@storybook/react-native';

import { StoryDecorator } from '../../../../utils';
import DateTimeCell from './DateTimeCell';

const onChange = (value) => {
  console.log('onChange', value);
};

const variants = {
  default: {
    title: 'Lorem',
    onChange,
  },
  value: {
    title: 'Lorem ipsum',
    onChange,
    value: new Date(),
  },
  limited: {
    title: 'Lorem ipsum',
    onChange,
    value: new Date(),
    maximumDate: new Date(),
  },
  date: {
    title: 'Lorem ipsum',
    onChange,
    value: new Date(),
    mode: 'date',
  },
  time: {
    title: 'Lorem ipsum',
    onChange,
    value: new Date(),
    mode: 'time',
  },
};

storiesOf('DateTimeCell', module)
  .addDecorator(StoryDecorator)
  .add('Default', () => <DateTimeCell {...variants.default} />)
  .add('With value', () => <DateTimeCell {...variants.value} />)
  .add('Limited maximum', () => <DateTimeCell {...variants.limited} />)
  .add('Date only', () => <DateTimeCell {...variants.date} />)
  .add('Time only', () => <DateTimeCell {...variants.time} />);
