/* @flow */

import React from 'react';
import { View, Text, Image } from 'react-native';
import { storiesOf } from '@storybook/react-native';

import { StoryDecorator } from '../../../../utils';
import * as icons from '../../../../static/icons';
import FormCell from './FormCell';

const onPress = () => {
  console.log('onPress');
};

const styles = {
  textStyle1: {
    color: 'red',
    fontSize: 24,
  },
  textStyle2: {
    color: 'blue',
    fontSize: 12,
  },
  iconStyle: {
    tintColor: 'red',
  },
  customComponentStyle: {
    flex: 1,
    height: 100,
    backgroundColor: 'yellow',
  },
  accessoryStyle: {
    width: 24,
    height: 24,
    backgroundColor: 'red',
    borderRadius: 500,
  },
  largeAccessoryStyle: {
    width: 72,
    height: 72,
    backgroundColor: 'red',
    borderRadius: 500,
  },
};

const variants = {
  default: {
    title: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit',
    disclosure: true,
    onPress,
  },
  disabled: {
    title: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit',
    disclosure: true,
    disabled: true,
    onPress,
  },
  readonly: {
    title: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit',
    disclosure: true,
    readonly: true,
    onPress,
  },
  detailText: {
    title: 'Lorem',
    detailText: 'ipsum',
    disclosure: true,
    onPress,
  },
  detailTextLong: {
    title: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit',
    detailText: 'Lorem ipsum dolor sit amet, consectetur',
    error: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit',
    disclosure: true,
    onPress,
  },
  detailSubtitleTextLong: {
    title: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit',
    subtitle: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit',
    detailText: 'Lorem ipsum dolor sit amet, consectetur',
    error: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit',
    disclosure: true,
    onPress,
  },
  styledText: {
    title: (
      <Text>
        Lorem ipsum dolor <Text style={styles.textStyle1}>sit amet,</Text>{' '}
        <Text style={styles.textStyle2}>consectetur adipisicing elit</Text>
      </Text>
    ),
    disclosure: true,
    onPress,
  },
  leftIcon: {
    title: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit',
    leftAccessory: (
      <Image style={styles.iconStyle} source={icons.FORM_RADIO_UNCHECKED} />
    ),
    disclosure: true,
    onPress,
  },
  largeLeftAccessory: {
    title: 'Lorem ipsum',
    subtitle: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit',
    leftAccessory: <View style={styles.largeAccessoryStyle} />,
    disclosure: true,
    onPress,
  },
  leftAndRightAccessory: {
    title: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit',
    leftAccessory: <View style={styles.accessoryStyle} />,
    rightAccessory: <View style={styles.accessoryStyle} />,
    disclosure: true,
    onPress,
  },
  customComponent: {
    title: 'Custom',
    component: <View style={styles.customComponentStyle} />,
    leftAccessory: (
      <Image style={styles.iconStyle} source={icons.FORM_RADIO_UNCHECKED} />
    ),
    disclosure: true,
    error: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit',
    onPress,
  },
};

storiesOf('FormCell', module)
  .addDecorator(StoryDecorator)
  .add('Default', () => (
    <View>
      <FormCell {...variants.default} />
      <FormCell {...variants.default} disabled />
      <FormCell {...variants.default} readonly />
    </View>
  ))
  .add('Detail text', () => <FormCell {...variants.detailText} />)
  .add('Long detail text', () => <FormCell {...variants.detailTextLong} />)
  .add('Long subtitle and detail text', () => (
    <FormCell {...variants.detailSubtitleTextLong} />
  ))
  .add('Styled text', () => <FormCell {...variants.styledText} />)
  .add('Left icon', () => <FormCell {...variants.leftIcon} />)
  .add('Large left accessory', () => (
    <FormCell {...variants.largeLeftAccessory} />
  ))
  .add('Left and right accessory', () => (
    <FormCell {...variants.leftAndRightAccessory} />
  ))
  .add('Custom component', () => <FormCell {...variants.customComponent} />);
