/* @flow */

import PropTypes from 'prop-types';
import * as React from 'react';
import { View, Image, Text, StyleSheet } from 'react-native';

import { isReactComponent } from 'makeandship-js-common/src/utils/is';

import { withStylesFromTheme } from '../../../../theme';
import NativeTouchableElement from '../../NativeTouchableElement';

import * as icons from '../../../../static/icons';

import { hasString } from '../utils';

class FormCell extends React.PureComponent<*> {
  withOnPress = (wrapped) => {
    const { styles, theme, onPress } = this.props;
    return (
      <NativeTouchableElement
        style={styles.touchableWrap}
        touchBackgroundColor={theme.colorCellTap}
        onPress={onPress}
      >
        {wrapped}
      </NativeTouchableElement>
    );
  };

  render() {
    let {
      styles,
      leftAccessory,
      rightAccessory,
      onPress,
      title,
      subtitle,
      component,
      disclosure,
      separatorFullWidth,
      error,
      readonly,
      disabled,
    } = this.props;
    let { detailText } = this.props;

    const hasTitle = hasString(title) || isReactComponent(title);
    const hasDetailText = hasString(detailText) || isReactComponent(detailText);

    const titleStyle = disabled ? styles.titleDisabled : styles.title;
    const detailTextStyle = disabled
      ? styles.detailTextDisabled
      : styles.detailText;

    // if (readonly) {
    //   disclosure = false;
    // }

    const hasRightAccessory = !!rightAccessory;

    const separatorStyle = error
      ? styles.withErrorSeparator
      : styles.withSeparator;
    const containerStyle = separatorFullWidth
      ? [styles.container, separatorStyle]
      : styles.container;
    const contentWrapWithSeparatorContainerStyle = separatorFullWidth
      ? styles.contentWrapWithSeparatorContainer
      : [styles.contentWrapWithSeparatorContainer, separatorStyle];
    const content = (
      <View style={containerStyle}>
        {leftAccessory && (
          <View style={styles.leftAccessoryContainer}>{leftAccessory}</View>
        )}
        <View style={contentWrapWithSeparatorContainerStyle}>
          <View style={styles.contentAndErrorContainer}>
            <View style={styles.contentContainerContent}>
              <View style={styles.contentContainer}>
                {hasTitle && (
                  <View style={styles.textContainer}>
                    <Text selectable={readonly} style={titleStyle}>
                      {title}
                    </Text>
                  </View>
                )}
                {hasTitle && hasDetailText && (
                  <View style={styles.componentSpacer} />
                )}
                {hasDetailText && (
                  <View style={styles.detailTextContainer}>
                    <Text selectable={readonly} style={detailTextStyle}>
                      {detailText}
                    </Text>
                  </View>
                )}
                {(hasTitle || hasDetailText) && component && (
                  <View style={styles.componentSpacer} />
                )}
                {component}
              </View>
            </View>
            {subtitle && <Text style={styles.subtitleText}>{subtitle}</Text>}
            {error && <Text style={styles.errorText}>{error}</Text>}
          </View>
          {hasRightAccessory && (
            <View style={styles.rightAccessoryContainer}>{rightAccessory}</View>
          )}
          {disclosure && (
            <View style={styles.disclosureIconContainer}>
              <Image
                style={styles.disclosureIcon}
                source={icons.FORM_CELL_DISCLOSURE}
              />
            </View>
          )}
        </View>
      </View>
    );
    if (!onPress || disabled) {
      return content;
    }
    return this.withOnPress(content);
  }

  static defaultProps = {
    separatorFullWidth: false,
    component: false,
    disabled: false,
    readonly: false,
  };
}

const stylesFromTheme = (theme) => ({
  touchableWrap: {},
  container: {
    flexDirection: 'row',
    backgroundColor: theme.colors.white,
    alignItems: 'stretch',
    paddingLeft: theme.padding.cell.paddingLeading,
  },
  contentWrapWithSeparatorContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    paddingRight: theme.padding.cell.paddingTrailing,
    minHeight: 44,
  },
  contentAndErrorContainer: {
    flex: 1,
  },
  contentContainerContent: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  contentContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  textContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: theme.padding.cell.paddingTop,
    paddingBottom: theme.padding.cell.paddingBottom,
    minWidth: 44,
    flexShrink: 1,
  },
  detailTextContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    paddingTop: theme.padding.cell.paddingTop,
    paddingBottom: theme.padding.cell.paddingBottom,
    flexShrink: 1,
  },
  componentSpacer: {
    width: theme.spacing.cell.spacingHorizontal,
    height: 12,
  },
  title: {
    fontSize: theme.fontSize,
    color: theme.colors.text,
  },
  titleReadonly: {
    fontSize: theme.fontSize,
    color: theme.colors.text,
  },
  titleDisabled: {
    fontSize: theme.fontSize,
    color: theme.colors.disabled,
  },
  detailText: {
    textAlign: 'right',
    fontSize: theme.fontSize,
    color: theme.colors.greyMid,
  },
  detailTextDisabled: {
    textAlign: 'right',
    fontSize: theme.fontSize,
    color: theme.colors.disabled,
  },
  errorText: {
    paddingBottom: theme.padding.cell.paddingBottom,
    fontSize: theme.fontSizeSmall,
    color: theme.colors.error,
  },
  subtitleText: {
    paddingBottom: theme.padding.cell.paddingBottom,
    fontSize: theme.fontSizeSmall,
    color: theme.colors.greyMid,
  },
  disclosureIcon: {
    tintColor: theme.colors.greyMid,
  },
  leftAccessoryContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: theme.padding.cell.paddingTop,
    paddingBottom: theme.padding.cell.paddingBottom,
    paddingRight: theme.padding.cell.paddingLeading,
  },
  rightAccessoryContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: theme.padding.cell.paddingTop,
    paddingBottom: theme.padding.cell.paddingBottom,
    paddingLeft: theme.spacing.cell.spacingHorizontal,
  },
  disclosureIconContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: theme.padding.cell.paddingTop,
    paddingBottom: theme.padding.cell.paddingBottom,
    paddingLeft: theme.spacing.cell.spacingHorizontal,
  },
  withSeparator: {
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: theme.colors.border,
  },
  withErrorSeparator: {
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: theme.colors.error,
  },
});

FormCell.propTypes = {
  onPress: PropTypes.func,
  leftAccessory: PropTypes.node,
  rightAccessory: PropTypes.node,
  disclosure: PropTypes.bool,
  title: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  subtitle: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  detailText: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  component: PropTypes.node,
  separatorFullWidth: PropTypes.bool,
  error: PropTypes.string,
  disabled: PropTypes.bool,
  readonly: PropTypes.bool,
};

export default withStylesFromTheme(FormCell, stylesFromTheme);
