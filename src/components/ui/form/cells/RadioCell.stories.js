/* @flow */

import React from 'react';
import { View } from 'react-native';

import { storiesOf } from '@storybook/react-native';

import { StoryDecorator } from '../../../../utils';
import RadioCell from './RadioCell';

const onPress = (value) => {
  console.log('onPress', value);
};

const variants = {
  default: {
    title: 'Lorem',
    onPress,
  },
  checked: {
    title: 'Lorem ipsum',
    onPress,
    checked: true,
  },
};

storiesOf('RadioCell', module)
  .addDecorator(StoryDecorator)
  .add('Default', () => (
    <View>
      <RadioCell {...variants.default} />
      <RadioCell {...variants.default} disabled />
      <RadioCell {...variants.default} readonly />
    </View>
  ))
  .add('Checked', () => (
    <View>
      <RadioCell {...variants.checked} />
      <RadioCell {...variants.checked} disabled />
      <RadioCell {...variants.checked} readonly />
    </View>
  ));
