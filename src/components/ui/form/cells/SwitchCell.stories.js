/* @flow */

import React from 'react';

import { storiesOf } from '@storybook/react-native';

import { StoryDecorator } from '../../../../utils';
import SwitchCell from './SwitchCell';

const onChange = (value) => {
  console.log('onChange', value);
};

const variants = {
  default: {
    title: 'Lorem',
    onChange,
    value: false,
  },
  checked: {
    title: 'Lorem ipsum',
    onChange,
    value: true,
  },
};

storiesOf('SwitchCell', module)
  .addDecorator(StoryDecorator)
  .add('Default', () => <SwitchCell {...variants.default} />)
  .add('On', () => <SwitchCell {...variants.checked} />);
