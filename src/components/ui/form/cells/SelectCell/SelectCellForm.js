/* @flow */

import * as React from 'react';
import { useDispatch } from 'react-redux';
import { StyleSheet } from 'react-native';
import _isEqual from 'lodash.isequal';
import Fuse from 'fuse.js';

import { setFormDataForKeyPath } from 'makeandship-js-common/src/modules/form';
import { isString } from 'makeandship-js-common/src/utils/is';

import { actions as reactNavigationActions } from '../../../../../modules/ui/reactNavigation';

import Form from '../../Form';
import RadioCell from '../RadioCell';
import TextInputCell from '../TextInputCell';
import SectionSeparator from '../../SectionSeparator';
import AppIcon from '../../../AppIcon';
import useTheme from '../../../../../theme/useTheme';

const SelectCellForm = ({
  navigation,
  route,
}: React.ElementProps<*>): React.Element<*> | null => {
  if (!route?.params) {
    return null;
  }
  const dispatch = useDispatch();
  const theme = useTheme();
  const styles = makeStyles(theme);
  const {
    backRoute,
    formKey,
    keyPath,
    value,
    options,
    title = 'Select',
    fuseOptions = {
      ignoreLocation: true,
      useExtendedSearch: true,
    },
    placeholder = 'Filter',
    onChange,
  } = route.params;
  const [filter, setFilter] = React.useState();

  React.useLayoutEffect(() => {
    navigation &&
      navigation.setOptions({
        title,
        headerBackTitleVisible: false,
      });
  }, [navigation, title]);

  const fuse = React.useMemo(() => {
    return new Fuse(options, { keys: ['label'], ...fuseOptions });
  }, [options, fuseOptions]);

  const optionCells = React.useMemo(() => {
    let filteredOptions = options;
    if (isString(filter) && filter.length > 0) {
      const results = fuse.search(filter);
      filteredOptions = results.map((result) => result.item);
    }
    const cells = filteredOptions.flatMap(
      ({ value: cellValue, label }, index) => {
        const onPress = () => {
          if (onChange) {
            onChange(cellValue);
          }
          dispatch(setFormDataForKeyPath(formKey, keyPath, cellValue));
          if (backRoute) {
            dispatch(reactNavigationActions.navigate(backRoute));
          } else {
            dispatch(reactNavigationActions.goBack());
          }
        };
        const checked = _isEqual(value, cellValue);
        return (
          <RadioCell
            key={`${index}`}
            title={label}
            checked={checked}
            onPress={onPress}
          />
        );
      }
    );
    return cells;
  }, [filter, fuse]);

  const cells = [
    <TextInputCell
      key={'filter'}
      placeholder={placeholder}
      value={filter}
      onChange={setFilter}
      autoCorrect={false}
      autoCapitalize={'none'}
      clearButtonMode={'always'}
      returnKeyType={'done'}
      leftAccessory={<AppIcon style={styles.searchIcon} name="search" />}
    />,
    <SectionSeparator key={'separator'} />,
    ...optionCells,
  ];
  return (
    <Form
      cells={cells}
      keyboardShouldPersistTaps={'always'}
      keyboardDismissMode={'on-drag'}
      initialNumToRender={20}
    />
  );
};

const makeStyles = (theme) =>
  StyleSheet.create({
    searchIcon: {
      color: theme.colors.placeholder,
    },
  });

export default SelectCellForm;
