/* @flow */

import * as React from 'react';

import SelectCell from './SelectCell';

const SelectCellWithTitle = (props: any) => (
  <SelectCell includeTitle {...props} />
);

SelectCellWithTitle.rendersTitle = true;

export default SelectCellWithTitle;
