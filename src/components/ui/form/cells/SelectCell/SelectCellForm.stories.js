/* @flow */

import React from 'react';
import { storiesOf } from '@storybook/react-native';

import { ConnectedStorybook, StoryDecorator } from '../../../../../utils';

import SelectCellForm from './SelectCellForm';

const options = [
  {
    label: 'Option 1 Lorem ipsum dolor sit amet, consectetur adipiscing elit',
    value: 'option-1',
  },
  {
    label: 'Option 2 Phasellus at magna vel odio placerat pharetra eget eget',
    value: 'option-2',
  },
  {
    label: 'Option 3 Aenean blandit pulvinar enim ut tristique',
    value: 'option-3',
  },
];

const variants = {
  default: {
    navigation: {
      state: {
        params: {
          formKey: 'test/test',
          keyPath: 'test.test',
          value: 'option-2',
          options,
        },
      },
    },
  },
};

storiesOf('SelectCellForm', module)
  .addDecorator(StoryDecorator)
  .addDecorator(ConnectedStorybook)
  .add('Default', () => <SelectCellForm {...variants.default} />);
