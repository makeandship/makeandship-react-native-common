/* @flow */

import * as React from 'react';
import _isEqual from 'lodash.isequal';

import { isString } from 'makeandship-js-common/src/utils/is';

import FormCell from '../FormCell';
import withNavigation from '../../../../../hoc/withNavigation';

class SelectCell extends React.PureComponent<*> {
  onPress = () => {
    const {
      currentRoute,
      navigate,
      formKey,
      keyPath,
      value,
      schema,
      title,
      fuseOptions,
      placeholder,
      onChange,
    } = this.props;
    const { enum: schemaEnum, enumNames } = schema;
    const options = schemaEnum.map((value, index) => {
      const label = enumNames ? enumNames[index] : value;
      return {
        label,
        value,
      };
    });
    navigate({
      name: 'SelectCellForm',
      params: {
        backRoute: currentRoute,
        formKey,
        keyPath,
        value,
        options,
        title,
        fuseOptions,
        placeholder,
        onChange,
      },
    });
  };
  render() {
    /* eslint-disable no-unused-vars */
    const {
      navigate,
      formKey,
      keyPath,
      value,
      schema,
      includeTitle,
      readonly,
      title: passedTitle,
      ...rest
    } = this.props;
    /* eslint-enable no-unused-vars */
    const { enum: schemaEnum, enumNames } = schema;
    let valueTitle = includeTitle ? undefined : 'Select';
    if (value) {
      if (isString(value)) {
        valueTitle = value;
      }
      if (enumNames) {
        const valueIndex = schemaEnum.findIndex((element) =>
          _isEqual(element, value)
        );
        if (valueIndex !== -1) {
          valueTitle = enumNames[valueIndex];
        }
      }
    }
    return (
      <FormCell
        title={includeTitle ? passedTitle : valueTitle}
        detailText={includeTitle ? valueTitle : undefined}
        onPress={readonly ? null : this.onPress}
        disclosure={!readonly}
        readonly={readonly}
        {...rest}
      />
    );
  }
  static defaultProps = {
    includeTitle: false,
  };
}

export default withNavigation(SelectCell);
