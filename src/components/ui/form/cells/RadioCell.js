/* @flow */

import PropTypes from 'prop-types';

import * as React from 'react';
import { Text, Image } from 'react-native';

import * as PlatformConstants from '../../../../constants';
import * as icons from '../../../../static/icons';
import FormCell from './FormCell';

import { withTheme } from '../../../../theme';
// TODO: disabled and readonly states

import { hasString } from '../utils';

class RadioCell extends React.PureComponent<*> {
  render() {
    const { theme, checked, title, ...rest } = this.props;
    const tintColor = theme.colors.primary;
    let rightAccessory = null;
    let leftAccessory = null;
    const iconTinted = { tintColor };
    const titleTinted = { color: tintColor };
    if (PlatformConstants.IS_IOS) {
      if (checked) {
        rightAccessory = (
          <Image style={iconTinted} source={icons.FORM_CELL_TICK} />
        );
      }
    } else {
      let icon;
      if (checked) {
        icon = icons.FORM_RADIO_CHECKED;
      } else {
        icon = icons.FORM_RADIO_UNCHECKED;
      }
      leftAccessory = <Image style={iconTinted} source={icon} />;
    }
    const titleComponent =
      hasString(title) && checked ? (
        <Text style={titleTinted}>{title}</Text>
      ) : (
        title
      );
    return (
      <FormCell
        title={titleComponent}
        leftAccessory={leftAccessory}
        rightAccessory={rightAccessory}
        {...rest}
      />
    );
  }
}

RadioCell.propTypes = {
  checked: PropTypes.bool,
};

export default withTheme(RadioCell);
