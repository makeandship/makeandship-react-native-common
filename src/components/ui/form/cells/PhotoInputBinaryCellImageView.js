/* @flow */

import * as React from 'react';

import AuthenticatedImageZoomable from '../../AuthenticatedImageZoomable';

const PhotoInputBinaryCellImageView = ({
  navigation,
  route,
}: React.ElementProps<*>): React.Element<*> | null => {
  if (!route?.params) {
    return null;
  }
  const { uri, title = 'Photo' } = route.params;

  React.useLayoutEffect(() => {
    navigation &&
      navigation.setOptions({
        title,
        headerBackTitleVisible: false,
      });
  }, [navigation, title]);

  return <AuthenticatedImageZoomable uri={uri} />;
};

export default PhotoInputBinaryCellImageView;
