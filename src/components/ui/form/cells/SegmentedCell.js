/* @flow */

import * as React from 'react';
import PropTypes from 'prop-types';
import { Text, View } from 'react-native';

import { isReactComponent } from 'makeandship-js-common/src/utils/is';

import NativeTouchableElement from '../../NativeTouchableElement';
import { withStylesFromTheme } from '../../../../theme';

import { hasString } from '../utils';

import FormCell from './FormCell';

// TODO: disabled and readonly states

class SegmentedCell extends React.PureComponent<*> {
  static rendersTitle = true;

  static LabelPlacement = {
    Inline: 'Inline',
    Below: 'Below',
  };

  onSegmentPress = (choice: any) => {
    const { onChange } = this.props;
    onChange(choice);
  };

  render() {
    const {
      styles,
      theme,
      title,
      titleRight,
      value,
      schema,
      labelPlacement = SegmentedCell.LabelPlacement.Inline,
      ...rest
    } = this.props;
    const tintColor = theme.colors.primary;
    const hasTitle = hasString(title) || isReactComponent(title);
    const hasTitleRight = hasString(titleRight) || isReactComponent(titleRight);
    const { enum: schemaEnum, enumNames } = schema;
    var segments = [];
    const segmentContentSelected = {
      backgroundColor: tintColor,
    };
    const segmentText = {
      fontSize: theme.fontSizeSmall,
      color: tintColor,
    };
    schemaEnum.forEach((choice, index) => {
      const selected = choice === value;
      const segmentContentStyle = selected
        ? [styles.segmentContent, segmentContentSelected]
        : styles.segmentContent;
      const segmentTextStyle = selected
        ? [segmentText, styles.segmentTextSelected]
        : segmentText;
      const label = enumNames ? enumNames[index] : choice;
      const nonBreakingTitle = label.replace(/ /g, '\u00a0');
      const key = `SEGMENT_${index}`;
      const segment = (
        <View style={styles.segment} key={key}>
          <NativeTouchableElement
            style={styles.segmentTouchContainer}
            touchBackgroundColor={theme.colorCellTap}
            onPress={() => this.onSegmentPress(choice)}
          >
            <View style={segmentContentStyle}>
              <Text style={segmentTextStyle}>{nonBreakingTitle}</Text>
            </View>
          </NativeTouchableElement>
        </View>
      );
      segments.push(segment);
    });
    let component = null;
    if (labelPlacement === SegmentedCell.LabelPlacement.Inline) {
      component = (
        <View style={styles.containerInline}>
          {hasTitle && (
            <View style={styles.titleContainer}>
              <Text style={styles.title}>{title}</Text>
            </View>
          )}
          <View style={styles.segmentedControlContainer}>
            <View style={styles.segmentedControl}>{segments}</View>
          </View>
          {hasTitleRight && (
            <View style={styles.titleRightContainer}>
              <Text style={styles.titleRight}>{titleRight}</Text>
            </View>
          )}
        </View>
      );
    } else {
      component = (
        <View style={styles.containerBelow}>
          <View style={styles.segmentedControlContainer}>
            <View style={styles.segmentedControl}>{segments}</View>
          </View>
          {(hasTitle || hasTitleRight) && (
            <View style={styles.titleContainerBelow}>
              {hasTitle && (
                <View style={styles.titleContainer}>
                  <Text style={styles.title}>{title}</Text>
                </View>
              )}
              {hasTitleRight && (
                <View style={styles.titleRightContainer}>
                  <Text style={styles.titleRight}>{titleRight}</Text>
                </View>
              )}
            </View>
          )}
        </View>
      );
    }
    return <FormCell component={component} {...rest} />;
  }
}

const stylesFromTheme = (theme) => ({
  containerInline: {
    flexDirection: 'row',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingTop: theme.padding.cell.paddingTop,
    paddingBottom: theme.padding.cell.paddingBottom,
  },
  containerBelow: {
    flexDirection: 'column',
    flex: 1,
    paddingTop: theme.padding.cell.paddingTop,
    paddingBottom: theme.padding.cell.paddingBottom,
  },
  titleContainerBelow: {
    paddingTop: theme.padding.cell.paddingTop,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  titleContainer: {
    flex: -1,
  },
  segmentedControlContainer: {
    flex: -1,
  },
  titleRightContainer: {
    flex: -1,
  },
  title: {
    fontSize: theme.fontSize,
    color: theme.colorText,
  },
  titleRight: {
    fontSize: theme.fontSize,
    color: theme.colors.text,
    textAlign: 'right',
  },
  segmentedControl: {
    backgroundColor: theme.colors.background,
    borderRadius: 16,
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'center',
    justifyContent: 'center',
  },
  segment: {
    minWidth: 36,
  },
  segmentTouchContainer: {
    borderRadius: 16,
  },
  segmentContent: {
    borderRadius: 16,
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 6,
    paddingHorizontal: 12,
  },
  segmentTextSelected: {
    color: 'white',
  },
});

SegmentedCell.propTypes = {
  title: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  titleRight: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  schema: PropTypes.any.isRequired,
  onChange: PropTypes.func.isRequired,
  value: PropTypes.any,
};

export default withStylesFromTheme(SegmentedCell, stylesFromTheme);
