/* @flow */

import React from 'react';

import { storiesOf } from '@storybook/react-native';
import { Image } from 'react-native';

import { StoryDecorator } from '../../../../utils';
import * as icons from '../../../../static/icons';
import TextInputCell from './TextInputCell';

const onChange = (value) => {
  console.log('onChange', value);
};

const styles = {
  iconStyle: {
    tintColor: 'red',
  },
};

// TODO: add variants with malformed input values

const variants = {
  default: {
    title: 'Lorem',
    placeholder: 'Type something',
    onChange,
  },
  leftIcon: {
    title: 'Lorem ipsum',
    placeholder: 'Type something',
    value: 'Some value',
    onChange,
    leftAccessory: (
      <Image style={styles.iconStyle} source={icons.FORM_RADIO_UNCHECKED} />
    ),
  },
  multiline: {
    title: 'Lorem',
    placeholder: 'Type something',
    value: 'Some value',
    numberOfLines: 5,
    onChange,
  },
  malformed1: {},
  malformed2: { readonly: true },
  malformed3: { readonly: true, title: '' },
  malformed4: { readonly: true, title: '', value: '' },
};

storiesOf('TextInputCell', module)
  .addDecorator(StoryDecorator)
  .add('Default', () => <TextInputCell {...variants.default} />)
  .add('Default disabled', () => (
    <TextInputCell {...variants.default} disabled />
  ))
  .add('Default readonly', () => (
    <TextInputCell {...variants.default} readonly />
  ))
  .add('Multiline', () => <TextInputCell {...variants.multiline} />)
  .add('Multiline disabled', () => (
    <TextInputCell {...variants.multiline} disabled />
  ))
  .add('Multiline readonly', () => (
    <TextInputCell {...variants.multiline} readonly />
  ))
  .add('Left icon', () => <TextInputCell {...variants.leftIcon} />)
  .add('Malformed 1', () => <TextInputCell {...variants.malformed1} />)
  .add('Malformed 2', () => <TextInputCell {...variants.malformed2} />)
  .add('Malformed 3', () => <TextInputCell {...variants.malformed3} />)
  .add('Malformed 4', () => <TextInputCell {...variants.malformed4} />);
