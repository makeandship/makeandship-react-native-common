/* @flow */

import PropTypes from 'prop-types';
import * as React from 'react';
import { Switch } from 'react-native';

import { withTheme } from '../../../../theme';

import FormCell from './FormCell';

// TODO: disabled and readonly states

class SwitchCell extends React.PureComponent<*> {
  render() {
    const { theme, value, onChange, ...rest } = this.props;
    const tintColor = theme.colors.primary;
    const rightAccessory = (
      <Switch
        onValueChange={onChange}
        value={value}
        trackColor={{ true: tintColor, false: null }}
      />
    );
    return <FormCell rightAccessory={rightAccessory} {...rest} />;
  }
  static rendersTitle = true;
}

SwitchCell.propTypes = {
  onChange: PropTypes.func.isRequired,
  value: PropTypes.bool,
};

export default withTheme(SwitchCell);
