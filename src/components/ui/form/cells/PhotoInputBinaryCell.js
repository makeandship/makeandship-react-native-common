/* @flow */

import * as React from 'react';

import FileInputBinaryCell from './FileInputBinaryCell';

const PhotoInputBinaryCell = (props: any) => (
  <FileInputBinaryCell allowDocuments={false} {...props} />
);

export default PhotoInputBinaryCell;
