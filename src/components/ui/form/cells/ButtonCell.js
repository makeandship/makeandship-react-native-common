/* @flow */

import PropTypes from 'prop-types';
import * as React from 'react';
import { Text, View } from 'react-native';

import { withStylesFromTheme } from '../../../../theme';

import FormCell from './FormCell';

class ButtonCell extends React.PureComponent<*> {
  render() {
    const { styles, theme, title, destructive, ...rest } = this.props;
    const { readonly, disabled } = this.props;
    let style = [styles.title, { color: theme.colors.primary }];
    if (destructive) {
      style = [styles.title, styles.titleDestructive];
    }
    if (readonly) {
      style = styles.titleReadonly;
    }
    if (disabled) {
      style = styles.titleDisabled;
    }
    const titleComponent = <Text style={style}>{title}</Text>;
    const component = <View style={styles.container}>{titleComponent}</View>;
    return <FormCell component={component} {...rest} />;
  }
}

const stylesFromTheme = (theme) => ({
  container: {
    flex: 1,
    alignItems: 'center',
    paddingTop: theme.padding.cell.paddingTop,
    paddingBottom: theme.padding.cell.paddingTop,
  },
  title: {
    fontSize: theme.fontSize,
    textAlign: 'center',
  },
  titleDestructive: {
    color: theme.colors.destructive,
  },
  titleReadonly: {
    fontSize: theme.fontSize,
    color: theme.colors.text,
  },
  titleDisabled: {
    fontSize: theme.fontSize,
    color: theme.colors.disabled,
  },
});

ButtonCell.propTypes = {
  destructive: PropTypes.bool,
};

export default withStylesFromTheme(ButtonCell, stylesFromTheme);
