/* @flow */

import React from 'react';
import { View } from 'react-native';
import { storiesOf } from '@storybook/react-native';

import { ConnectedStorybook, StoryDecorator } from '../../../../utils';

import FileInputBinaryCell from './FileInputBinaryCell';

const value = {
  filename: '500.jpeg',
  mimetype: 'image/jpeg',
  url: 'https://picsum.photos/id/237/500/500',
  size: '50958',
};

const thumbnailsValue = {
  url: 'https://picsum.photos/id/237/500/500',
  filename: '500.jpeg',
  mimetype: 'image/jpeg',
  size: 6232691,
  thumbnails: [
    {
      url: 'https://picsum.photos/id/237/128/128',
      height: 128,
      width: 128,
      filename: '500.jpeg',
      mimetype: 'image/jpeg',
      size: 6,
    },
    {
      url: 'https://picsum.photos/id/237/256/256',
      height: 256,
      width: 256,
      filename: '500.jpeg',
      mimetype: 'image/jpeg',
      size: 6,
    },
    {
      url: 'https://picsum.photos/id/237/512/512',
      height: 512,
      width: 512,
      filename: '500.jpeg',
      mimetype: 'image/jpeg',
      size: 6,
    },
    {
      url: 'https://picsum.photos/id/237/1024/1024',
      height: 1024,
      width: 1024,
      filename: '500.jpeg',
      mimetype: 'image/jpeg',
      size: 6,
    },
  ],
};

const variants = {
  default: {
    formKey: 'test/test',
    keyPath: 'test.test',
    value,
  },
  photos: {
    formKey: 'test/test',
    keyPath: 'test.test',
    value,
    allowDocuments: false,
  },
  thumbnails: {
    formKey: 'test/test',
    keyPath: 'test.test',
    value: thumbnailsValue,
  },
  empty: {},
  invalid: {
    value: [{}, {}, {}],
  },
};

storiesOf('FileInputBinaryCell', module)
  .addDecorator(StoryDecorator)
  .addDecorator(ConnectedStorybook)
  .add('Default', () => (
    <View>
      <FileInputBinaryCell {...variants.default} />
      <FileInputBinaryCell {...variants.default} disabled />
      <FileInputBinaryCell {...variants.default} readonly />
    </View>
  ))
  .add('Photos', () => (
    <View>
      <FileInputBinaryCell {...variants.photos} />
      <FileInputBinaryCell {...variants.photos} disabled />
      <FileInputBinaryCell {...variants.photos} readonly />
    </View>
  ))
  .add('Thumbnails', () => (
    <View>
      <FileInputBinaryCell {...variants.thumbnails} />
      <FileInputBinaryCell {...variants.thumbnails} disabled />
      <FileInputBinaryCell {...variants.thumbnails} readonly />
    </View>
  ))
  .add('Empty', () => (
    <View>
      <FileInputBinaryCell {...variants.empty} />
      <FileInputBinaryCell {...variants.empty} disabled />
      <FileInputBinaryCell {...variants.empty} readonly />
    </View>
  ))
  .add('Invalid', () => (
    <View>
      <FileInputBinaryCell {...variants.invalid} />
      <FileInputBinaryCell {...variants.invalid} disabled />
      <FileInputBinaryCell {...variants.invalid} readonly />
    </View>
  ));
