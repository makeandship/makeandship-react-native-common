/* @flow */

import * as React from 'react';
import PropTypes from 'prop-types';
import { TextInput } from 'react-native';

import { withStylesFromTheme } from '../../../../theme';

import FormCell from './FormCell';

class TextInputCell extends React.PureComponent<*> {
  _textInputRef: ?TextInput;

  textInputRef = (ref: ?TextInput) => {
    const { onFocusFunc } = this.props;
    this._textInputRef = ref;
    onFocusFunc && onFocusFunc(this.focus);
  };

  focus = () => {
    this._textInputRef && this._textInputRef.focus();
  };

  onTextInputChangeText = (e: any) => {
    const { onChange } = this.props;
    onChange(e);
  };

  render() {
    const {
      styles,
      theme,
      value,
      placeholder,
      autoCapitalize,
      autoCorrect,
      returnKeyType,
      keyboardType,
      clearButtonMode,
      blurOnSubmit,
      onSubmitEditing,
      numberOfLines,
      secureTextEntry,
      autoFocus,
      ...rest
    } = this.props;
    const { readonly, disabled } = this.props;
    if (readonly || disabled) {
      if (this.props.title && this.props.title.length) {
        return <FormCell detailText={value} {...rest} />;
      } else {
        return <FormCell {...rest} title={value} />;
      }
    }
    const multiline = numberOfLines > 1;
    const style = multiline
      ? [styles.textInput, { minHeight: 21 * numberOfLines }]
      : styles.textInput;
    let component = (
      <TextInput
        ref={this.textInputRef}
        style={style}
        onChangeText={this.onTextInputChangeText}
        defaultValue={value}
        underlineColorAndroid={theme.colorTransparent}
        placeholderTextColor={theme.colorPlaceholder}
        clearButtonMode={clearButtonMode}
        placeholder={placeholder}
        autoCapitalize={autoCapitalize}
        autoCorrect={autoCorrect}
        keyboardType={keyboardType}
        blurOnSubmit={blurOnSubmit}
        onSubmitEditing={onSubmitEditing}
        returnKeyType={returnKeyType}
        multiline={multiline}
        numberOfLines={numberOfLines}
        secureTextEntry={secureTextEntry}
        autoFocus={autoFocus}
      />
    );
    return <FormCell component={component} {...rest} />;
  }
  static defaultProps = {
    numberOfLines: 1,
    returnKeyType: 'next',
  };
}

const stylesFromTheme = (theme) => ({
  textInput: {
    fontSize: theme.fontSize,
    color: theme.colorText,
    flex: 1,
    paddingLeft: 0,
    paddingRight: 0,
    paddingTop: theme.padding.cell.paddingTop,
    paddingBottom: theme.padding.cell.paddingBottom,
  },
});

TextInputCell.propTypes = {
  ...TextInput.propTypes,
  onChange: PropTypes.func,
  value: PropTypes.string,
};

export default withStylesFromTheme(TextInputCell, stylesFromTheme);
