/* @flow */

import React from 'react';
import { View } from 'react-native';
import { storiesOf } from '@storybook/react-native';

import { ConnectedStorybook, StoryDecorator } from '../../../../utils';

import PhotoInputBinaryCell from './PhotoInputBinaryCell';

const value = {
  filename: '500.jpeg',
  mimetype: 'image/jpeg',
  url: 'https://picsum.photos/id/237/500/500',
  size: '50958',
};

const thumbnailsValue = {
  url: 'https://picsum.photos/id/237/500/500',
  filename: '500.jpeg',
  mimetype: 'image/jpeg',
  size: 6232691,
  thumbnails: [
    {
      url: 'https://picsum.photos/id/237/128/128',
      height: 128,
      width: 128,
      filename: '500.jpeg',
      mimetype: 'image/jpeg',
      size: 6,
    },
    {
      url: 'https://picsum.photos/id/237/256/256',
      height: 256,
      width: 256,
      filename: '500.jpeg',
      mimetype: 'image/jpeg',
      size: 6,
    },
    {
      url: 'https://picsum.photos/id/237/512/512',
      height: 512,
      width: 512,
      filename: '500.jpeg',
      mimetype: 'image/jpeg',
      size: 6,
    },
    {
      url: 'https://picsum.photos/id/237/1024/1024',
      height: 1024,
      width: 1024,
      filename: '500.jpeg',
      mimetype: 'image/jpeg',
      size: 6,
    },
  ],
};

const variants = {
  default: {
    formKey: 'test/test',
    keyPath: 'test.test',
    value,
  },
  photos: {
    formKey: 'test/test',
    keyPath: 'test.test',
    value,
    allowDocuments: false,
  },
  thumbnails: {
    formKey: 'test/test',
    keyPath: 'test.test',
    value: thumbnailsValue,
  },
  empty: {},
  invalid: {
    value: [{}, {}, {}],
  },
};

storiesOf('PhotoInputBinaryCell', module)
  .addDecorator(StoryDecorator)
  .addDecorator(ConnectedStorybook)
  .add('Default', () => (
    <View>
      <PhotoInputBinaryCell {...variants.default} />
      <PhotoInputBinaryCell {...variants.default} disabled />
      <PhotoInputBinaryCell {...variants.default} readonly />
    </View>
  ))
  .add('Thumbnails', () => (
    <View>
      <PhotoInputBinaryCell {...variants.thumbnails} />
      <PhotoInputBinaryCell {...variants.thumbnails} disabled />
      <PhotoInputBinaryCell {...variants.thumbnails} readonly />
    </View>
  ))
  .add('Empty', () => (
    <View>
      <PhotoInputBinaryCell {...variants.empty} />
      <PhotoInputBinaryCell {...variants.empty} disabled />
      <PhotoInputBinaryCell {...variants.empty} readonly />
    </View>
  ))
  .add('Invalid', () => (
    <View>
      <PhotoInputBinaryCell {...variants.invalid} />
      <PhotoInputBinaryCell {...variants.invalid} disabled />
      <PhotoInputBinaryCell {...variants.invalid} readonly />
    </View>
  ));
