/* @flow */

import * as React from 'react';
import PropTypes from 'prop-types';
import { View, Text } from 'react-native';
import { connect } from 'react-redux';
import filesize from 'filesize';

import mimetypes from 'makeandship-js-common/src/utils/mimetypes.json';
import {
  isString,
  isPlainObject,
  isFile,
} from 'makeandship-js-common/src/utils/is';
import { isDelete } from 'makeandship-js-common/src/components/ui/form/fields/FileInputBinary/utils';

import { withStylesFromTheme } from '../../../../theme';
import FormCell from './FormCell';
import ButtonCell from './ButtonCell';
import AppIcon from '../../AppIcon';

import {
  formDataPickFile,
  getUri,
} from '../../../../modules/ui/formDataFilePicker';
import { imagePickerView } from '../../../../modules/ui/imagePicker';

import AuthenticatedImage from '../../AuthenticatedImage';

type State = {
  userSelectedFile: boolean,
};

class FileInputBinaryCell extends React.Component<*, State> {
  _originalValue: any;

  componentDidMount = () => {
    if (!isFile(this.props.value) && !isDelete(this.props.value)) {
      this._originalValue = this.props.value;
    }
  };

  componentDidUpdate = (prevProps) => {
    if (prevProps.value !== this.props.value) {
      if (isFile(this.props.value)) {
        this.setState({
          userSelectedFile: true,
        });
      } else if (!isDelete(this.props.value)) {
        this._originalValue = this.props.value;
        this.setState({
          userSelectedFile: false,
        });
      }
    }
  };

  onPress = () => {
    const {
      formDataPickFile,
      imagePickerView,
      formKey,
      keyPath,
      allowDocuments,
      readonly,
      value,
      onChange,
    } = this.props;
    console.log('onChange', onChange);
    if (readonly) {
      if (!allowDocuments) {
        const uri = getUri(value);
        imagePickerView({ uri });
      }
    } else {
      formDataPickFile({
        formKey,
        keyPath,
        originalValue: this._originalValue,
        allowDocuments,
        onChange,
      });
    }
  };

  render() {
    /* eslint-disable no-unused-vars */
    const {
      title: discardedTitle,
      styles,
      value,
      onPress,
      rightAccessory,
      allowDocuments = true,
      ...rest
    } = this.props;
    /* eslint-enable no-unused-vars */
    const isDeleted = isDelete(value);
    const uri = getUri(value);
    let isImage = false;
    let rendered = null;
    let description = null;
    let title = this.props.readonly ? (
      'File'
    ) : (
      <Text style={styles.buttonTitle}>
        <AppIcon name="folder-open" /> Add photo or file
      </Text>
    );
    if (!allowDocuments) {
      title = this.props.readonly ? (
        'Photo'
      ) : (
        <Text style={styles.buttonTitle}>
          <AppIcon name="camera" /> Add photo
        </Text>
      );
    }
    if (!value) {
      return this.props.readonly ? (
        <FormCell title={title} {...rest} />
      ) : (
        <ButtonCell
          title={title}
          subtitle={description}
          onPress={this.onPress}
          {...rest}
        />
      );
    }
    if (value) {
      if (isDeleted) {
        title = (
          <Text>
            <AppIcon name="trash" /> Save to remove
          </Text>
        );
        return (
          <ButtonCell
            title={title}
            subtitle={description}
            onPress={this.onPress}
            {...rest}
          />
        );
      } else if (isString(value)) {
        rendered = <Text>{value}</Text>;
      } else if (isFile(value)) {
        const { name, type, size } = value;
        const filetype = mimetypes[type] || 'Unknown type';
        const prettySize = filesize(size, { fullform: true });
        isImage = filetype.match('image.*');
        title = (
          <Text>
            <AppIcon name={allowDocuments ? 'file-o' : 'file-picture-o'} />{' '}
            {name}
          </Text>
        );
        description = (
          <Text>
            <AppIcon name="check-circle" /> Save to upload &middot; {filetype}{' '}
            &middot; {prettySize}
          </Text>
        );
      } else if (isPlainObject(value)) {
        const { filename, mimetype, size } = value;
        const filetype = mimetypes[mimetype] || 'Unknown type';
        const prettySize = filesize(size, { fullform: true });
        isImage = filetype.match('image.*');
        title = (
          <Text>
            <AppIcon name={allowDocuments ? 'file-o' : 'file-picture-o'} />{' '}
            {filename}
          </Text>
        );
        description = (
          <Text>
            {filetype} &middot; {prettySize}
          </Text>
        );
      } else {
        rendered = <Text>No preview</Text>;
      }
    }
    if (isImage) {
      rendered = (
        <View style={styles.imageContainer}>
          <AuthenticatedImage uri={uri} style={styles.image} />
        </View>
      );
    }
    return (
      <FormCell
        title={title}
        subtitle={description}
        onPress={this.onPress}
        rightAccessory={rendered}
        {...rest}
      />
    );
  }
}

const stylesFromTheme = (theme) => ({
  imageContainer: {
    paddingTop: theme.cellPaddingTop,
    paddingBottom: theme.cellPaddingBottom,
  },
  image: {
    width: 88,
    height: 88,
    borderRadius: 3,
  },
});

FileInputBinaryCell.propTypes = {
  formKey: PropTypes.string,
  keyPath: PropTypes.string,
  formDataPickFile: PropTypes.func,
  error: PropTypes.any,
  value: PropTypes.any,
  setFormDataForKeyPath: PropTypes.func,
};

const withRedux = connect(null, {
  formDataPickFile,
  imagePickerView,
});

export default withRedux(
  withStylesFromTheme(FileInputBinaryCell, stylesFromTheme)
);
