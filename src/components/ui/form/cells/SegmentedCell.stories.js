/* @flow */

import React from 'react';

import { storiesOf } from '@storybook/react-native';

import { StoryDecorator } from '../../../../utils';
import SegmentedCell from './SegmentedCell';

const onChange = (value) => {
  console.log('onChange', value);
};

const variants = {
  default: {
    title: 'Lorem',
    onChange,
    value: '2',
    schema: {
      enum: ['1', '2', '3'],
      enumNames: ['One', 'Two', 'Three'],
    },
  },
  longList: {
    title: 'Lorem ipsum',
    onChange,
    value: '3',
    schema: {
      enum: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10'],
      enumNames: [
        'One',
        'Two',
        'Three',
        'Four',
        'Five',
        'Six',
        'Seven',
        'Eight',
        'Nine',
        'Ten',
      ],
    },
  },
  zeroToTenPlain: {
    onChange,
    value: '5',
    schema: {
      enum: ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10'],
    },
    noWrap: true,
  },
  zeroToTenLabelled: {
    onChange,
    value: '5',
    title: 'Lorem ipsum dolor sit amet',
    titleRight: 'Lorem ipsum dolor sit amet',
    schema: {
      enum: ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10'],
    },
  },
};

storiesOf('SegmentedCell', module)
  .addDecorator(StoryDecorator)
  .add('Default', () => <SegmentedCell {...variants.default} />)
  .add('Long list', () => <SegmentedCell {...variants.longList} />)
  .add('0-10 plain', () => <SegmentedCell {...variants.zeroToTenPlain} />)
  .add('0-10 labelled', () => <SegmentedCell {...variants.zeroToTenLabelled} />)
  .add('0-10 labelled below', () => (
    <SegmentedCell
      {...variants.zeroToTenLabelled}
      labelPlacement={SegmentedCell.LabelPlacement.Below}
    />
  ));
