/* @flow */

import * as React from 'react';
import { StyleSheet, View } from 'react-native';

import useTheme from '../../../theme/useTheme';

const SectionSeparator = (): React.Element<*> => {
  const theme = useTheme();
  const styles = makeStyles(theme);
  return <View style={styles.container} />;
};

const makeStyles = (theme) => ({
  container: {
    height: 8,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: theme.colors.border,
  },
});

export default SectionSeparator;
