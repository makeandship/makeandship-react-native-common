/* @flow */

import * as React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import * as dateFns from 'date-fns';
import produce from 'immer';
import { connect } from 'react-redux';

import {
  retrieveSchema,
  getDefaultFormState,
} from 'react-jsonschema-form/lib/utils';

import {
  isFunction,
  isReactComponentClass,
  isArray,
  isString,
  isUndefined,
} from 'makeandship-js-common/src/utils/is';

import Form from './Form';
import SectionTitle from './SectionTitle';
import RadioCell from './cells/RadioCell';
import TextInputCell from './cells/TextInputCell';
import DateTimeCell from './cells/DateTimeCell';
import ButtonCell from './cells/ButtonCell';
import FormCell from './cells/FormCell';
import { hasString } from './utils';

import { IS_IOS } from '../../../constants';

const DEFAULT = 'DEFAULT';
const HIDDEN = 'HIDDEN';

class JsonSchemaForm extends React.Component<*> {
  updateFormData = (keyPath?: string, value: any) => {
    if (!keyPath) {
      return;
    }
    const { formData, onChange } = this.props;
    const updatedFormData = produce(formData || {}, (draft) => {
      _.set(draft, keyPath, value);
    });
    onChange && onChange(updatedFormData);
  };

  propsToPass = () => {
    /* eslint-disable no-unused-vars */
    const {
      headerCells,
      footerCells,
      onChange,
      schema,
      error,
      cells,
      ...rest
    } = this.props;
    /* eslint-enable no-unused-vars */
    return rest;
  };

  cellsForSchema = ({
    schema,
    schemaKeyPath,
    uiSchemaKeyPath,
    parentSchema,
    disabled = false,
    readonly = false,
    depth = 0,
  }: {
    schema: any,
    schemaKeyPath?: string,
    uiSchemaKeyPath?: string,
    parentSchema?: any,
    disabled?: boolean,
    readonly?: boolean,
    depth: number,
  }) => {
    const { formData, uiSchema, error, minimumTitleDepth } = this.props;

    const errors = error ? error.errors : {};

    const propsToPass = this.propsToPass();
    let cells = [];

    let required = false;

    const keyPathElements = _.toPath(schemaKeyPath);
    const hasSchemaKeyPath = keyPathElements.length > 0;
    if (parentSchema && parentSchema.required && hasSchemaKeyPath) {
      const lastKey = keyPathElements[keyPathElements.length - 1];
      required = parentSchema.required.includes(lastKey);
    }

    const formDataValue = _.get(formData, schemaKeyPath);
    const errorValue = _.get(errors, schemaKeyPath);

    let uiSchemaValue;
    if (uiSchemaKeyPath) {
      uiSchemaValue = _.get(uiSchema, uiSchemaKeyPath);
    } else {
      // no schemaKeyPath for the root, e.g. when uiSchema = {'ui:readonly': true}
      uiSchemaValue = uiSchema;
    }

    const retreivedSchema = retrieveSchema(
      schema,
      this.props.schema.definitions,
      hasSchemaKeyPath ? formDataValue : formData
    );

    let hidden = false;

    const widgetUiSchemaValue = _.get(uiSchemaValue, 'ui:widget');
    const optionsUiSchemaValue = _.get(uiSchemaValue, 'ui:options');
    const placeholderUiSchemaValue = _.get(uiSchemaValue, 'ui:placeholder');
    const autofocusUiSchemaValue = _.get(uiSchemaValue, 'ui:autofocus');
    const returnKeyTypeUiSchemaValue = _.get(uiSchemaValue, 'ui:returnkeytype');
    const widgetAdditionalPropsUiSchemaValue = _.get(
      uiSchemaValue,
      'ui:widgetadditionalprops'
    );
    const disabledUiSchemaValue = _.get(uiSchemaValue, 'ui:disabled');
    const readonlyUiSchemaValue = _.get(uiSchemaValue, 'ui:readonly');
    if (widgetUiSchemaValue === 'hidden') {
      hidden = true;
    }
    if (disabledUiSchemaValue) {
      disabled = true;
    }
    if (readonlyUiSchemaValue) {
      readonly = true;
    }

    if (hidden) {
      return cells;
    }

    // title onlyy if specified

    let title;
    if (depth >= minimumTitleDepth && retreivedSchema.title) {
      title = retreivedSchema.title;
      if (required && title) {
        title = `${title} *`;
      }
    }
    let description = retreivedSchema.description;

    if (
      retreivedSchema.type !== 'array' &&
      !isUndefined(widgetUiSchemaValue) &&
      !isString(widgetUiSchemaValue)
    ) {
      // also pass any additional props, e.g. from withFormModule()
      const props = {
        ...propsToPass,
        title,
        description,
        schema: retreivedSchema,
        keyPath: schemaKeyPath,
        parentSchema: retreivedSchema,
        disabled,
        readonly,
        onChange: (value) => {
          this.updateFormData(schemaKeyPath, value);
        },
        value: formDataValue,
        error: errorValue,
        ...widgetAdditionalPropsUiSchemaValue,
      };
      // show title unless the cell has a static prop of rendersTitle
      if (!widgetUiSchemaValue.rendersTitle) {
        if (hasString(title) || hasString(description)) {
          cells.push(<SectionTitle title={title} description={description} />);
        }
      }
      // HoC are not isReactComponentClass(), but also not isFunction()
      // TODO: find a better way to check for HoC
      if (
        isReactComponentClass(widgetUiSchemaValue) ||
        !isFunction(widgetUiSchemaValue)
      ) {
        const Widget = widgetUiSchemaValue;
        cells.push(<Widget {...props} />);
        return cells;
      } else {
        // allow the function to return an array of cells, 'default' or 'hidden'
        const result = widgetUiSchemaValue({
          ...props,
          default: DEFAULT,
          hidden: HIDDEN,
        });
        if (result === HIDDEN) {
          return cells;
        }
        if (result !== DEFAULT) {
          cells.push(widgetUiSchemaValue(props));
          return cells;
        }
      }
    }

    let renderTitle = true;

    if (retreivedSchema.type === 'object') {
      // iterate object properties
      Object.entries(retreivedSchema.properties).forEach((entry) => {
        const [key, value] = entry;
        const keyPath = schemaKeyPath ? `${schemaKeyPath}.${key}` : `${key}`;
        let uiKeyPath = uiSchemaKeyPath
          ? `${uiSchemaKeyPath}.${key}`
          : `${key}`;
        if (value.type === 'array') {
          // traverse into 'items'
          uiKeyPath = `${uiKeyPath}.items`;
        }
        cells = cells.concat(
          this.cellsForSchema({
            schema: value,
            schemaKeyPath: keyPath,
            uiSchemaKeyPath: uiKeyPath,
            parentSchema: retreivedSchema,
            disabled,
            readonly,
            depth: depth + 1,
          })
        );
      });
    } else if (retreivedSchema.type === 'array') {
      const itemsSchema = retrieveSchema(
        retreivedSchema.items,
        this.props.schema.definitions
      );
      const { addTitle = 'Add', removeTitle = 'Remove' } =
        optionsUiSchemaValue || {};
      // iterate form data array values
      if (isArray(formDataValue) && schemaKeyPath) {
        formDataValue.forEach((formDataArrayValue, index) => {
          const formDataArrayValueKeyPath = `${schemaKeyPath}[${index}]`;
          // don't add array indices to uiSchemaKeyPath
          let uiKeyPath = uiSchemaKeyPath || '';
          if (itemsSchema.type === 'array') {
            // traverse into 'items'
            uiKeyPath = `${uiKeyPath}.items`;
          }
          cells = cells.concat(
            this.cellsForSchema({
              schema: itemsSchema,
              schemaKeyPath: formDataArrayValueKeyPath,
              uiSchemaKeyPath: uiKeyPath,
              parentSchema: retreivedSchema,
              disabled,
              readonly,
              depth: depth + 1,
            })
          );
          if (!readonly) {
            cells.push(
              <ButtonCell
                title={removeTitle}
                destructive
                disabled={disabled}
                onPress={() => {
                  const newFormDataValue = formDataValue.filter(
                    (value, valueIndex) => index !== valueIndex
                  );
                  this.updateFormData(schemaKeyPath, newFormDataValue);
                }}
              />
            );
          }
        });
        formDataValue.length > 0 && cells.push(<SectionTitle />);
      }
      // TODO: inject default values if specified - computeDefaults() from react-jsonschema-form?
      // TODO: display a more descriptive title based on the schema title etc.
      if (!readonly) {
        cells.push(
          <ButtonCell
            title={addTitle}
            disabled={disabled}
            onPress={() => {
              const newValue = itemsSchema.type == 'object' ? {} : '';
              const newFormDataValue = (formDataValue || []).concat(newValue);
              this.updateFormData(schemaKeyPath, newFormDataValue);
            }}
          />
        );
      }
    } else if (retreivedSchema.enum) {
      // radios
      const enumTitles = retreivedSchema.enumNames || retreivedSchema.enum;
      if (readonly) {
        cells.push(
          <FormCell
            title={formDataValue || '–'}
            disabled={disabled}
            readonly={readonly}
          />
        );
      } else {
        retreivedSchema.enum.forEach((entry, index) => {
          const isLast = index === retreivedSchema.enum.length - 1;
          const error = isLast ? errorValue : undefined;
          const checked = formDataValue === entry;
          const title = enumTitles[index];
          cells.push(
            <RadioCell
              title={title}
              checked={checked}
              onPress={() => {
                this.updateFormData(schemaKeyPath, entry);
              }}
              error={error}
              disabled={disabled}
              readonly={readonly}
            />
          );
        });
      }
    } else if (retreivedSchema.type === 'string') {
      if (retreivedSchema.format === 'data-url') {
        // TODO: file picker
        console.warn(
          'data-url is not implemented - specify FileInputBinaryCell as ui:widget instead',
          JSON.stringify(retreivedSchema, null, 2)
        );
      } else if (
        retreivedSchema.format === 'date' ||
        retreivedSchema.format === 'date-time' ||
        retreivedSchema.format === 'time'
      ) {
        const value = formDataValue
          ? dateFns.parseISO(formDataValue)
          : undefined;
        cells.push(
          <DateTimeCell
            title={title}
            value={value}
            error={errorValue}
            mode={
              retreivedSchema.format === 'date-time'
                ? 'datetime'
                : retreivedSchema.format
            }
            onChange={(value) => {
              this.updateFormData(schemaKeyPath, dateFns.formatISO(value));
            }}
            disabled={disabled}
            readonly={readonly}
            {...widgetAdditionalPropsUiSchemaValue}
          />
        );
        renderTitle = !DateTimeCell.rendersTitle;
      } else {
        // regular string input
        let keyboardType = 'default';
        let autoCorrect = true;
        let autoCapitalize = 'sentences';
        if (retreivedSchema.format === 'email') {
          keyboardType = 'email-address';
          autoCorrect = false;
          autoCapitalize = 'none';
        }
        if (IS_IOS && retreivedSchema.format === 'uri') {
          keyboardType = 'url';
          autoCorrect = false;
          autoCapitalize = 'none';
        }
        let numberOfLines = 1;
        if (widgetUiSchemaValue === 'textarea') {
          numberOfLines =
            (optionsUiSchemaValue && optionsUiSchemaValue.rows) || 5;
        }
        let secureTextEntry = false;
        if (widgetUiSchemaValue === 'password') {
          secureTextEntry = true;
        }
        cells.push(
          <TextInputCell
            value={formDataValue}
            error={errorValue}
            placeholder={placeholderUiSchemaValue}
            autoFocus={autofocusUiSchemaValue}
            returnKeyType={returnKeyTypeUiSchemaValue}
            keyboardType={keyboardType}
            numberOfLines={numberOfLines}
            secureTextEntry={secureTextEntry}
            autoCorrect={autoCorrect}
            autoCapitalize={autoCapitalize}
            onChange={(value) => {
              this.updateFormData(schemaKeyPath, value);
            }}
            disabled={disabled}
            readonly={readonly}
            {...widgetAdditionalPropsUiSchemaValue}
          />
        );
      }
    } else if (
      retreivedSchema.type === 'number' ||
      retreivedSchema.type === 'integer'
    ) {
      // number input, set value as number on change
      cells.push(
        <TextInputCell
          value={formDataValue ? `${formDataValue}` : ''}
          error={errorValue}
          placeholder={placeholderUiSchemaValue}
          autoFocus={autofocusUiSchemaValue}
          autoCorrect={false}
          returnKeyType={returnKeyTypeUiSchemaValue}
          onChange={(value) => {
            if (!value || !value.length || value.endsWith('.')) {
              // store as a string while entering decimal
              this.updateFormData(schemaKeyPath, value);
            } else {
              // store as a number
              this.updateFormData(schemaKeyPath, parseFloat(value));
            }
          }}
          keyboardType={'numeric'}
          disabled={disabled}
          readonly={readonly}
          {...widgetAdditionalPropsUiSchemaValue}
        />
      );
    } else if (retreivedSchema.type === 'boolean') {
      // boolean input, set value as boolean on change
      if (readonly) {
        cells.push(
          <FormCell
            title={
              formDataValue === true
                ? 'Yes'
                : formDataValue === false
                ? 'No'
                : '–'
            }
            disabled={disabled}
            readonly={readonly}
          />
        );
      } else {
        cells.push(
          <RadioCell
            title={'Yes'}
            error={errorValue}
            checked={formDataValue === true}
            onPress={() => {
              this.updateFormData(schemaKeyPath, true);
            }}
            disabled={disabled}
            readonly={readonly}
          />
        );
        cells.push(
          <RadioCell
            title={'No'}
            error={errorValue}
            checked={formDataValue === false}
            onPress={() => {
              this.updateFormData(schemaKeyPath, false);
            }}
            disabled={disabled}
            readonly={readonly}
          />
        );
      }
    } else if (retreivedSchema.type === 'null') {
      // no additional cell
    } else {
      console.log(
        'Not yet implemented:',
        JSON.stringify(retreivedSchema, null, 2)
      );
    }

    if ((hasString(title) && renderTitle) || hasString(description)) {
      cells.unshift(<SectionTitle title={title} description={description} />);
    }

    return cells;
  };

  render() {
    /* eslint-disable no-unused-vars */
    const {
      schema,
      headerCells,
      footerCells,
      keyboardShouldPersistTaps,
      keyboardDismissMode,
      uiSchema,
      onChange,
      error,
      formData,
      ...rest
    } = this.props;
    /* eslint-enable no-unused-vars */
    const cells = [
      ...headerCells,
      ...this.cellsForSchema({ schema }),
      ...footerCells,
    ];
    return (
      <Form
        formData={formData}
        cells={cells}
        keyboardShouldPersistTaps={keyboardShouldPersistTaps}
        keyboardDismissMode={keyboardDismissMode}
        {...rest}
      />
    );
  }

  static defaultProps = {
    headerCells: [],
    footerCells: [],
    minimumTitleDepth: 0,
  };
}

JsonSchemaForm.propTypes = {
  cells: PropTypes.any,
  schema: PropTypes.any.isRequired,
  uiSchema: PropTypes.any,
  formData: PropTypes.any,
  onChange: PropTypes.func,
  error: PropTypes.shape({
    errors: PropTypes.object,
  }),
  headerCells: PropTypes.arrayOf(PropTypes.node),
  footerCells: PropTypes.arrayOf(PropTypes.node),
  minimumTitleDepth: PropTypes.number,
};

const withRedux = connect((state, ownProps) => {
  const schema = ownProps.schema;
  const passedFormData = ownProps.formData;
  // apply schema defaults
  const formData = getDefaultFormState(
    schema,
    passedFormData,
    schema.definitions
  );
  return {
    passedFormData,
    formData,
  };
});

export default withRedux(JsonSchemaForm);
