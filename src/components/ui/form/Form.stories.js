/* @flow */

import React from 'react';

import { storiesOf } from '@storybook/react-native';
import { Image } from 'react-native';

import * as icons from '../../../static/icons';
import Form, { SectionTitle } from './';
import { FormCell } from './cells';

const variants = {
  default: {
    cells: [
      <SectionTitle
        key={'0'}
        title={'Lorem ipsum'}
        description={'Lorem ipsum dolor'}
      />,
      <FormCell key={'1'} title={'Lorem'} disclosure />,
      <FormCell
        leftAccessory={<Image source={icons.FORM_RADIO_UNCHECKED} />}
        key={'2'}
        title={'Lorem'}
        error={'Error lorem ipsum dolor'}
        disclosure
      />,
      <FormCell key={'3'} title={'Lorem'} disclosure />,
      <SectionTitle
        key={'0'}
        title={'Lorem ipsum'}
        description={'Lorem ipsum dolor'}
      />,
      <FormCell
        leftAccessory={<Image source={icons.FORM_RADIO_UNCHECKED} />}
        key={'2'}
        title={'Lorem'}
        disclosure
      />,
    ],
  },
};

storiesOf('Form', module).add('Default', () => <Form {...variants.default} />);
