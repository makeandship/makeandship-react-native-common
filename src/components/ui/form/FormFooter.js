/* @flow */

import * as React from 'react';
import { View, StyleSheet } from 'react-native';

class FormFooter extends React.PureComponent<*> {
  render() {
    return <View style={styles.formFooter} />;
  }
}

const styles = StyleSheet.create({
  formFooter: {
    minHeight: 32,
  },
});

export default FormFooter;
