/* @flow */

import PropTypes from 'prop-types';
import * as React from 'react';
import { Text, StyleSheet, View } from 'react-native';

import useTheme from '../../../theme/useTheme';
import { hasString } from './utils';

const SectionTitle = ({
  title,
  description,
  translucent,
}: React.ElementProps<*>): React.Element<*> => {
  const theme = useTheme();
  const styles = makeStyles(theme);

  const style = translucent
    ? [styles.container, styles.containerTranslucent]
    : styles.container;
  return (
    <View style={style}>
      {hasString(title) && (
        <Text style={styles.titleText}>{title.toUpperCase()}</Text>
      )}
      {hasString(description) && (
        <Text style={styles.descriptionText}>{description}</Text>
      )}
    </View>
  );
};

const makeStyles = (theme) => ({
  container: {
    paddingLeft: theme.padding.cell.paddingLeading,
    paddingRight: theme.padding.cell.paddingTrailing,
    paddingTop: 4,
    paddingBottom: theme.padding.cell.paddingBottom,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: theme.colors.border,
  },
  containerTranslucent: {
    backgroundColor: theme.colors.panelTranslucent,
  },
  titleText: {
    paddingTop: 4,
    fontSize: theme.fontSizeExtraSmall,
    color: theme.colors.sectionTitle,
  },
  descriptionText: {
    paddingTop: 4,
    fontSize: theme.fontSize,
    color: theme.colors.sectionTitle,
  },
});

SectionTitle.propTypes = {
  title: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  description: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  translucent: PropTypes.bool,
};

export default SectionTitle;
