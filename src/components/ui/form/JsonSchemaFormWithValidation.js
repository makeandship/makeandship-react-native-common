/* @flow */

import * as React from 'react';
import PropTypes from 'prop-types';
import { Keyboard } from 'react-native';

import {
  isFormDataEqual,
  validateWithSchema,
} from 'makeandship-js-common/src/components/ui/form/utils';
import { isFunction } from 'makeandship-js-common/src/utils/is';

import JsonSchemaForm from './JsonSchemaForm';
import SectionTitle from './SectionTitle';
import ButtonCell from './cells/ButtonCell';

type State = {
  formData: any,
  error?: any,
};

class JsonSchemaFormWithValidation extends React.Component<*, State> {
  constructor(props: any) {
    super(props);
    this.state = { formData: props.formData || {} };
  }

  componentDidUpdate = (prevProps: any) => {
    if (!isFormDataEqual(this.props.formData, prevProps.formData)) {
      this.setState({
        formData: this.props.formData,
      });
    }
    if (!isFormDataEqual(this.props.error, prevProps.error)) {
      this.setState({
        error: this.props.error,
      });
    }
  };

  onChange = (formData: any) => {
    const { onChange } = this.props;
    if (onChange) {
      formData = onChange(formData) || formData;
    }
    this.setState({ formData });
  };

  submit = () => {
    const {
      dismissKeyboardOnSubmit,
      validationEnabled,
      schema,
      onErrors,
      onSubmit,
      validate,
    } = this.props;
    let shouldSubmit = true;
    dismissKeyboardOnSubmit && Keyboard.dismiss();
    if (validationEnabled) {
      let { valid, errors } = validateWithSchema(schema, this.state.formData);
      if (validate) {
        errors = validate(this.state.formData, errors || {});
      }
      if (!valid) {
        if (onErrors) {
          errors = onErrors(errors) || errors;
        }
        const error = { errors };
        this.setState({ error });
        shouldSubmit = false;
      }
    }
    if (shouldSubmit) {
      this.setState({ error: undefined });
      onSubmit && onSubmit(this.state.formData);
    }
  };

  getFooterCells = () => {
    let { footerCells, submitTitle } = this.props;
    if (!footerCells) {
      footerCells = [
        <SectionTitle key={'spacer'} title={' '} />,
        <ButtonCell
          key={'submit-button'}
          onPress={this.submit}
          title={submitTitle}
        />,
      ];
    } else if (isFunction(footerCells)) {
      footerCells = footerCells({ submit: this.submit });
    }
    return footerCells;
  };

  render() {
    /* eslint-disable no-unused-vars */
    let {
      onChange,
      formData,
      error,
      submitTitle,
      footerCells,
      ...rest
    } = this.props;
    /* eslint-enable no-unused-vars */
    return (
      <JsonSchemaForm
        onChange={this.onChange}
        formData={this.state.formData}
        error={this.state.error}
        footerCells={this.getFooterCells()}
        {...rest}
      />
    );
  }
  static defaultProps = {
    submitTitle: 'Submit',
    dismissKeyboardOnSubmit: true,
    validationEnabled: true,
  };
}

JsonSchemaFormWithValidation.propTypes = {
  ...JsonSchemaForm.propTypes,
  submitTitle: PropTypes.string,
  onErrors: PropTypes.func,
  onSubmit: PropTypes.func,
  validate: PropTypes.func,
  footerCells: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.arrayOf(PropTypes.node),
  ]),
  dismissKeyboardOnSubmit: PropTypes.bool,
};

export default JsonSchemaFormWithValidation;
