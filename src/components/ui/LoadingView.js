/* @flow */

import * as React from 'react';
import { View, ActivityIndicator } from 'react-native';

import useTheme from '../../theme/useTheme';

const LoadingView = (): React.Element<*> => {
  const theme = useTheme();
  const styles = makeStyles(theme);
  return (
    <View style={styles.container}>
      <ActivityIndicator />
    </View>
  );
};

const makeStyles = (theme) => ({
  container: {
    flex: 1,
    backgroundColor: theme.colors.background,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default LoadingView;
