/* @flow */

import React from 'react';
import { View, Text } from 'react-native';

import { storiesOf } from '@storybook/react-native';

import LoadingOverlay from './LoadingOverlay';

storiesOf('LoadingOverlay', module).add('Default', () => (
  <View style={{ flex: 1, backgroundColor: 'yellow' }}>
    <Text>LoadingOverlay</Text>
    <LoadingOverlay title={'Loading'} />
  </View>
));
