/* @flow */

import React from 'react';
import { StyleSheet, View, Text, TextInput, Animated } from 'react-native';

import { storiesOf } from '@storybook/react-native';

import { StoryDecorator } from '../../utils';
import Button from './Button';
import PagingFlatList from './PagingFlatList';

const data = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];

const renderItem = ({ index }: React.ElementProps<*>) => (
  <View style={styles.itemContainer}>
    <Text style={styles.text}>{index}</Text>
    <TextInput style={styles.textInput} placeholder={`${index}`} />
  </View>
);

const variants = {
  default: {
    renderItem,
    data,
  },
  initialOffset: {
    renderItem,
    data,
    index: 4,
  },
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  itemContainer: {
    flex: 1,
    backgroundColor: 'magenta',
    borderWidth: 3,
    borderColor: 'yellow',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    color: 'yellow',
    fontSize: 72,
    textAlign: 'center',
  },
  textInput: {
    fontSize: 72,
    // FIXME: textAlign: 'center' here causes Android to break control of paging index...
    // textAlign: 'center',
  },
  controlsContainer: {
    flexDirection: 'row',
  },
  animatedContainer: {
    flex: 1,
    backgroundColor: 'magenta',
  },
});

type State = {
  index: number,
  animatedHeight: Animated.Value,
};

const ANIMATED_HEIGHT_MAX = 64;

class PagingFlatListWithControls extends React.Component<*, State> {
  state = {
    index: 0,
    animatedHeight: new Animated.Value(ANIMATED_HEIGHT_MAX),
  };

  constructor(props: any) {
    super(props);
    if (props.index) {
      this.state.index = props.index;
    }
  }

  onChange = ({ index }) => {
    console.log('onChange', index);
    this.setState({
      index,
    });
  };

  previous = () => {
    this.setState({
      index: Math.max(0, this.state.index - 1),
    });
  };

  next = () => {
    this.setState({
      index: this.state.index + 1,
    });
  };

  componentDidMount() {
    Animated.loop(
      Animated.sequence([
        Animated.timing(this.state.animatedHeight, {
          toValue: 0,
          duration: 1000,
          useNativeDriver: false,
        }),
        Animated.timing(this.state.animatedHeight, {
          toValue: ANIMATED_HEIGHT_MAX,
          duration: 1000,
          useNativeDriver: false,
        }),
      ])
    ).start();
  }

  render() {
    /* eslint-disable no-unused-vars */
    const { index: discardedIndex, ...rest } = this.props;
    /* eslint-enable no-unused-vars */
    const { index, animatedHeight } = this.state;
    const animatedContainerStyle = {
      height: animatedHeight,
    };
    return (
      <View style={styles.container}>
        <PagingFlatList index={index} onChange={this.onChange} {...rest} />
        <Animated.View style={animatedContainerStyle} />
        <View style={styles.controlsContainer}>
          <Button label={'Previous'} onPress={this.previous} />
          <Text>{index}</Text>
          <Button label={'Next'} onPress={this.next} />
        </View>
      </View>
    );
  }
}

storiesOf('PagingFlatList', module)
  .addDecorator(StoryDecorator)
  .add('Default', () => <PagingFlatListWithControls {...variants.default} />)
  .add('Initial offset', () => (
    <PagingFlatListWithControls {...variants.initialOffset} />
  ));
