/* @flow */

import PropTypes from 'prop-types';

import * as React from 'react';
import { View, StyleSheet } from 'react-native';

class AppKeyboardResizingView extends React.Component<*> {
  render() {
    const { children } = this.props;
    return <View style={styles.container}>{children}</View>;
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

AppKeyboardResizingView.propTypes = {
  children: PropTypes.node,
};

export default AppKeyboardResizingView;
