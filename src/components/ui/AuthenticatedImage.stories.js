/* @flow */

import React from 'react';
import { storiesOf } from '@storybook/react-native';

import AuthenticatedImage from './AuthenticatedImage';

const variants = {
  default: {
    uri: 'https://picsum.photos/id/237/1024/1024',
  },
};

storiesOf('AuthenticatedImage', module).add('Default', () => (
  <AuthenticatedImage {...variants.default} style={{ flex: 1 }} />
));
