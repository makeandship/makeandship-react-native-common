/* @flow */

import * as React from 'react';
import { StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

import useTheme from '../../theme/useTheme';

const AppIcon = (props: React.ElementProps<*>): React.Element<*> => {
  const theme = useTheme();
  const flat = StyleSheet.flatten(props.style);
  const size = flat?.fontSize || theme.fontSize;
  return <Icon size={size} {...props} />;
};

AppIcon.propTypes = {
  ...Icon.propTypes,
};

export default AppIcon;
