/* @flow */

import React from 'react';

import { storiesOf } from '@storybook/react-native';

import * as icons from '../../static/icons';
import NoItemsView from './NoItemsView';

const variants = {
  default: {
    icon: icons.BACK,
    title: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit',
    description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit',
  },
};

storiesOf('NoItemsView', module).add('Default', () => (
  <NoItemsView {...variants.default} />
));
