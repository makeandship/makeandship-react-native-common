/* @flow */

import * as React from 'react';
import PropTypes from 'prop-types';
import { Image } from 'react-native';

import { getConfig } from 'makeandship-js-common/src/modules/auth/config';

const AuthenticatedImage = ({
  uri,
  placeholder,
  ...rest
}: React.ElementProps<*>): React.Element<*> | null => {
  const [source, setSource] = React.useState();
  React.useEffect(() => {
    const initSource = async () => {
      const config = getConfig();
      const provider = config.provider;
      let token;
      try {
        await provider.updateToken();
        token = await provider.getToken();
      } catch (error) {
        // ignore token refesh error here
        // should be handled by provider callbacks
      }
      const source = token
        ? {
            uri,
            headers: {
              Authorization: `Bearer ${token}`,
            },
          }
        : { uri };
      setSource(source);
    };
    initSource();
  }, [uri]);
  if (source) {
    return <Image source={source} {...rest} />;
  }
  if (!placeholder || !placeholder.uri) {
    return null;
  }
  return <Image source={placeholder} {...rest} />;
};

AuthenticatedImage.propTypes = {
  ...Image.propTypes,
  placeholder: PropTypes.object,
};

export default AuthenticatedImage;
