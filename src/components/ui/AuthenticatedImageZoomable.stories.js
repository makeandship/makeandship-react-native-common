/* @flow */

import React from 'react';
import { storiesOf } from '@storybook/react-native';

import AuthenticatedImageZoomable from './AuthenticatedImageZoomable';

const variants = {
  default: {
    uri: 'https://picsum.photos/id/237/1024/1024',
  },
  small: {
    uri: 'https://picsum.photos/id/237/160/160',
  },
  landscape: {
    uri: 'https://picsum.photos/id/237/1024/768',
  },
  portrait: {
    uri: 'https://picsum.photos/id/237/768/1024',
  },
};

storiesOf('AuthenticatedImageZoomable', module)
  .add('Default', () => <AuthenticatedImageZoomable {...variants.default} />)
  .add('Small', () => <AuthenticatedImageZoomable {...variants.small} />)
  .add('Landscape', () => (
    <AuthenticatedImageZoomable {...variants.landscape} />
  ))
  .add('Portrait', () => <AuthenticatedImageZoomable {...variants.portrait} />);
