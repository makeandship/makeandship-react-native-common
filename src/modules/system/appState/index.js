/* @flow */

import { channel, buffers } from 'redux-saga';
import { apply, put, select, all, fork, take } from 'redux-saga/effects';
import { AppState } from 'react-native';

export const typePrefix = 'system/appState/';
export const SET_STATE = `${typePrefix}SET_STATE`;
export const ENTER_FOREGROUND = `${typePrefix}ENTER_FOREGROUND`;
export const ENTER_BACKGROUND = `${typePrefix}ENTER_BACKGROUND`;

const stateChan = channel(buffers.expanding(10));

export const getAppState = (state) => state.system.appState;

// Action creators

export const setState = (state) => ({
  type: SET_STATE,
  state,
});

export const enterForeground = () => ({
  type: ENTER_FOREGROUND,
});

export const enterBackground = () => ({
  type: ENTER_BACKGROUND,
});

// Reducer

const initialState = 'undetermined';

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case SET_STATE:
      return action.state;
    default:
      return state;
  }
}

// Sagas

export function* stateChanWorker() {
  while (true) {
    const next = yield take(stateChan);
    const current = yield select(getAppState);

    if (current.match(/inactive|background/) && next === 'active') {
      yield put(enterForeground());
    }

    if (current.match(/inactive|active/) && next === 'background') {
      yield put(enterBackground());
    }

    yield put(setState(next));
  }
}

export function* init() {
  yield apply(AppState, AppState.addEventListener, [
    'change',
    (next) => stateChan.put(next),
  ]);
}

export function* appStateSaga() {
  const sagas = [stateChanWorker, init];
  yield all(sagas.map((saga) => fork(saga)));
}
