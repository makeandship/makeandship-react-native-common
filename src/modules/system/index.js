/* @flow */

import { combineReducers } from 'redux';
import { all, fork } from 'redux-saga/effects';

import appState, { appStateSaga } from './appState';

const reducer = combineReducers({
  appState,
});

export default reducer;

const sagas = [appStateSaga];

export function* systemSaga(): Generator<*, *, *> {
  yield all(sagas.map((saga) => fork(saga)));
}
