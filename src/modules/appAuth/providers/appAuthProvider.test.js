/* @flow */

import { createAppAuthProvider } from './appAuthProvider';

describe('appAuthProvider', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });
  const credentials = {
    accessToken: 'foo',
    refreshToken: 'bar',
  };
  const fetcher = jest.fn(() => 'data');
  const authorize = jest.fn(() => credentials);
  const refresh = jest.fn(() => credentials);
  describe('when initialising', () => {
    describe('without existing credentials', () => {
      const storage = {
        set: jest.fn(),
        get: jest.fn(),
      };
      it('calls get on storage but does not refresh', async () => {
        const provider = createAppAuthProvider({
          issuer: 'issuer',
          clientId: 'clientId',
          redirectUrl: 'redirectUrl',
          storage,
          fetcher,
          authorize,
          refresh,
        });
        await provider.init();
        expect(storage.get).toHaveBeenCalled();
        expect(refresh).not.toHaveBeenCalled();
      });
    });
    describe('with existing credentials', () => {
      const storage = {
        set: jest.fn(),
        get: jest.fn(() => 'refreshToken'),
      };
      it('calls get on storage followed by refresh and set', async () => {
        const provider = createAppAuthProvider({
          issuer: 'issuer',
          clientId: 'clientId',
          redirectUrl: 'redirectUrl',
          storage,
          fetcher,
          authorize,
          refresh,
        });
        await provider.init();
        expect(storage.get).toHaveBeenCalled();
        expect(refresh).toHaveBeenCalled();
        expect(storage.set).toHaveBeenCalledWith(credentials.refreshToken);
        expect(await provider.getToken()).toEqual(credentials.accessToken);
      });
    });
  });
  describe('when login is called', () => {
    const storage = {
      set: jest.fn(),
      get: jest.fn(),
    };
    it('calls get on storage followed by refresh and set', async () => {
      const provider = createAppAuthProvider({
        issuer: 'issuer',
        clientId: 'clientId',
        redirectUrl: 'redirectUrl',
        storage,
        fetcher,
        authorize,
        refresh,
      });
      await provider.init();
      await provider.login();
      expect(authorize).toHaveBeenCalledWith({
        additionalParameters: undefined,
        clientId: 'clientId',
        issuer: 'issuer',
        redirectUrl: 'redirectUrl',
        scopes: ['openid', 'profile'],
        usePKCE: true,
      });
      expect(storage.set).toHaveBeenCalledWith(credentials.refreshToken);
      expect(await provider.getToken()).toEqual(credentials.accessToken);
    });
  });
});
