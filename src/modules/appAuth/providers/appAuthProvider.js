/* @flow */

import * as RNAppAuth from 'react-native-app-auth';
import qs from 'qs';
import { Linking } from 'react-native';

import axiosFetcher from 'makeandship-js-common/src/modules/fetchers/axiosFetcher';
import { delay } from 'makeandship-js-common/src/modules/utils';
import { isTokenExpired } from 'makeandship-js-common/src/utils/jwt';
import { isString } from 'makeandship-js-common/src/utils/is';

import { createKeychainStorage } from '../storage/keychainStorage';

// auth using web login

export const createAppAuthProvider = ({
  issuer,
  clientId,
  redirectUrl,
  usePKCE = true,
  scopes = ['openid', 'profile'],
  storage,
  fetcher = axiosFetcher,
  authorize = RNAppAuth.authorize,
  refresh = RNAppAuth.refresh,
} = {}) => {
  const provider = {};
  if (!storage) {
    storage = createKeychainStorage({ issuer, clientId });
  }

  let _ready = false;
  let _accessToken = false;

  const init = async () => {
    const refreshToken = await storage.get();
    _ready = true;
    if (isString(refreshToken)) {
      await updateToken();
    }
    return getIsAuthenticated();
  };

  const ready = async () => {
    if (_ready) {
      return;
    }
    while (true) {
      if (_ready) {
        return;
      }
      await delay(0);
    }
  };

  const getIsAuthenticated = async () => {
    return !!_accessToken;
  };

  const login = async (additionalParameters: any) => {
    try {
      const config = {
        issuer,
        clientId,
        redirectUrl,
        usePKCE,
        scopes,
        additionalParameters,
      };
      const credentials = await authorize(config);
      if (credentials) {
        _accessToken = credentials.accessToken;
        await storage.set(credentials.refreshToken);
        provider.onAuthSuccess && provider.onAuthSuccess();
      }
    } catch (error) {
      if (provider.onAuthError) {
        provider.onAuthError(error);
      } else {
        throw error;
      }
    }
  };

  const logout = async () => {
    _accessToken = false;
    await storage.reset();
    provider.onAuthLogout && provider.onAuthLogout();
  };

  const getToken = async () => {
    await ready();
    return _accessToken;
  };

  const updateToken = async () => {
    await ready();
    const refreshToken = await storage.get();
    const hasAccessToken = !!_accessToken;
    const hasRefeshToken = !!refreshToken;
    let shouldRefresh = false;

    if (hasAccessToken) {
      if (isTokenExpired(_accessToken)) {
        // expired access token
        shouldRefresh = true;
      }
    } else {
      if (hasRefeshToken) {
        // have refresh token but no access token
        shouldRefresh = true;
      }
    }
    if (shouldRefresh) {
      try {
        const config = {
          issuer,
          clientId,
          redirectUrl,
          usePKCE,
          scopes,
        };
        const refreshedCredentials = await refresh(config, {
          refreshToken,
        });
        if (refreshedCredentials) {
          _accessToken = refreshedCredentials.accessToken;
          await storage.set(refreshedCredentials.refreshToken);
        }
        provider.onUpdateTokenSuccess && provider.onUpdateTokenSuccess();
      } catch (error) {
        if (provider.onUpdateTokenError) {
          provider.onUpdateTokenError(error);
        } else {
          throw error;
        }
      }
    }
  };

  const register = () => {
    const query = qs.stringify({
      client_id: clientId,
      redirect_uri: redirectUrl,
      response_type: 'code',
    });
    const url = `${issuer}/protocol/openid-connect/registrations?${query}`;
    Linking.openURL(url);
  };

  const profile = () => {
    const query = qs.stringify({
      referrer: clientId,
      referrer_uri: redirectUrl,
    });
    const url = `${issuer}/account?${query}`;
    Linking.openURL(url);
  };

  const loadUserProfile = async () => {
    await updateToken();
    if (!_accessToken) {
      throw 'Not authenticated';
    }
    const url = `${issuer}/protocol/openid-connect/userinfo`;
    const options = {
      headers: {
        Accept: 'application/json',
        Authorization: `Bearer ${_accessToken}`,
      },
    };
    const data = await fetcher(url, options);
    return data;
  };

  Object.assign(provider, {
    init,
    getIsAuthenticated,
    ready,
    login,
    logout,
    getToken,
    updateToken,
    register,
    profile,
    loadUserProfile,
  });

  return provider;
};
