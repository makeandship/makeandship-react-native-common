/* @flow */

import qs from 'qs';

import { delay } from 'makeandship-js-common/src/modules/utils';
import axiosFetcher from 'makeandship-js-common/src/modules/fetchers/axiosFetcher';
import { isTokenExpired } from 'makeandship-js-common/src/utils/jwt';

import { createKeychainStorage } from '../storage/keychainStorage';

// auth without using web login
// allows login using native UI in app

export const createSilentAuthProvider = ({
  issuer,
  clientId,
  scopes = ['openid', 'profile'],
  storage,
  fetcher = axiosFetcher,
} = {}) => {
  const provider = {};
  if (!storage) {
    storage = createKeychainStorage({ issuer, clientId });
  }

  let _ready = false;

  const callTokenEndpoint = async (parameters) => {
    const url = `${issuer}/protocol/openid-connect/token`;
    const body = qs.stringify({
      ...parameters,
      client_id: clientId,
      scopes,
    });
    const options = {
      body,
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
    };
    const data = await fetcher(url, options);
    return data;
  };

  const init = async () => {
    const credentials = await storage.get();
    _ready = true;
    return !!credentials;
  };

  const ready = async () => {
    if (_ready) {
      return;
    }
    while (true) {
      if (_ready) {
        return;
      }
      await delay(0);
    }
  };

  const getIsAuthenticated = async () => {
    const credentials = await storage.get();
    return !!credentials;
  };

  const setCredentials = async (credentials: any) => {
    await storage.set(credentials);
  };

  const login = async (parameters) => {
    try {
      const credentials = await callTokenEndpoint({
        ...parameters,
        grant_type: 'password',
      });
      if (credentials) {
        await storage.set(credentials);
        provider.onAuthSuccess && provider.onAuthSuccess();
      }
    } catch (error) {
      provider.onAuthError && provider.onAuthError(error);
      throw error;
    }
  };

  const logout = async () => {
    await storage.reset();
    provider.onAuthLogout && provider.onAuthLogout();
  };

  const getToken = async () => {
    const credentials = await storage.get();
    if (credentials) {
      return credentials.access_token;
    }
  };

  const updateToken = async () => {
    await ready();
    const credentials = await storage.get();
    if (credentials) {
      if (isTokenExpired(credentials.access_token)) {
        try {
          const refreshedCredentials = await callTokenEndpoint({
            grant_type: 'refresh_token',
            refresh_token: credentials.refresh_token,
          });
          if (refreshedCredentials) {
            await storage.set(refreshedCredentials);
          }
          provider.onUpdateTokenSuccess && provider.onUpdateTokenSuccess();
        } catch (error) {
          provider.onUpdateTokenError && provider.onUpdateTokenError(error);
          throw error;
        }
      } else {
        // access token still valid - not refreshing
      }
    }
  };

  Object.assign(provider, {
    init,
    getIsAuthenticated,
    ready,
    setCredentials,
    login,
    logout,
    getToken,
    updateToken,
  });

  return provider;
};
