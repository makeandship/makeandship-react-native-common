/* @flow */

import * as Keychain from 'react-native-keychain';
import { createKeychainStorage, chunkString } from './keychainStorage';

const veryLongToken =
  '0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef';

describe('chunkString', () => {
  describe('when valid', () => {
    it('returns an array of string chunks', async () => {
      expect(chunkString('0123456789abcdef', 4)).toMatchInlineSnapshot(`
        Array [
          "0123",
          "4567",
          "89ab",
          "cdef",
        ]
      `);
    });
  });
});

describe('keychainStorage', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });
  describe('when not set', () => {
    describe('get credentials', () => {
      it('accesses Keychain and returns undefined', async () => {
        const storage = createKeychainStorage({
          issuer: 'foo',
          clientId: 'bar',
        });
        expect(await storage.get()).toBeUndefined();
      });
      it('accesses Keychain once for many get calls', async () => {
        const storage = createKeychainStorage({
          issuer: 'foo',
          clientId: 'bar',
        });
        await storage.get();
        await storage.get();
        await storage.get();
        expect(Keychain.getInternetCredentials).toHaveBeenCalledTimes(1);
      });
    });
  });
  describe('when using set', () => {
    describe('get credentials', () => {
      it('accesses Keychain once to set, but not get', async () => {
        const storage = createKeychainStorage({
          issuer: 'foo',
          clientId: 'bar',
        });
        await storage.set({ veryLongToken });
        await storage.get();
        await storage.get();
        expect(await storage.get()).toEqual({ veryLongToken });
        expect(Keychain.setInternetCredentials).toHaveBeenCalledTimes(1);
        expect(Keychain.getInternetCredentials).toHaveBeenCalledTimes(0);
      });
    });
    describe('reset credentials', () => {
      it('accesses Keychain once to set and reset, but not get', async () => {
        const storage = createKeychainStorage({
          issuer: 'foo',
          clientId: 'bar',
        });
        await storage.set({ veryLongToken });
        expect(await storage.get()).toEqual({ veryLongToken });
        await storage.get();
        await storage.get();
        await storage.reset();
        expect(await storage.get()).toBeUndefined();
        await storage.get();
        await storage.get();
        expect(Keychain.setInternetCredentials).toHaveBeenCalledTimes(1);
        expect(Keychain.resetInternetCredentials).toHaveBeenCalledTimes(1);
        expect(Keychain.getInternetCredentials).toHaveBeenCalledTimes(0);
      });
    });
  });
});
