/* @flow */

import * as Keychain from 'react-native-keychain';
import _ from 'lodash';
import { Platform } from 'react-native';

// on Android, retrieved credentials are kept in volatile memory to minimise Keychain authorisation requests
// https://github.com/demokratie-live/democracy-client/blob/3d2aee279d4ad617240bb6cb5a23e7a8f0ee54e2/src/services/VotesLocal.js#L93

const KEYCHAIN_CHUNK_MAXSIZE = 256;
const KEYCHAIN_CHUNK_COUNT_KEY = `KEYCHAIN_CHUNK_COUNT_KEY`;
const USE_CHUNKED = Platform.select({ ios: false, android: true });

export const chunkString = (str: String, length: number) => {
  if (!_.isString(str)) {
    return [];
  }
  return str.match(new RegExp('.{1,' + length + '}', 'g'));
};

export const createKeychainStorage = ({
  issuer,
  clientId,
  accessControl,
  securityLevel,
}) => {
  let _volatileCredentials;
  let _volatileCredentialsIsInitialised = false;

  const get = async () => {
    if (_volatileCredentialsIsInitialised) {
      return _volatileCredentials;
    }
    try {
      if (USE_CHUNKED) {
        const hasChunkCount = await Keychain.hasInternetCredentials(
          `${issuer}-${KEYCHAIN_CHUNK_COUNT_KEY}`
        );
        if (hasChunkCount) {
          const { password } = await Keychain.getInternetCredentials(
            `${issuer}-${KEYCHAIN_CHUNK_COUNT_KEY}`
          );
          const chunkCount = JSON.parse(password);
          if (chunkCount > 0) {
            // reassemble chunks
            const chunks = [];
            for (let i = 0; i < chunkCount; i++) {
              const { password } = await Keychain.getInternetCredentials(
                `${issuer}-${i}`
              );
              const chunk = JSON.parse(password);
              chunks.push(chunk);
            }
            const credentials = chunks.join('');
            _volatileCredentials = credentials;
          }
        }
      } else {
        const hasInternetCredentials = await Keychain.hasInternetCredentials(
          issuer
        );
        if (hasInternetCredentials) {
          const { password } = await Keychain.getInternetCredentials(issuer);
          if (password) {
            const credentials = JSON.parse(password);
            _volatileCredentials = credentials;
          }
        }
      }
    } catch (error) {
      // ignore error - credentials missing or invalid
    }
    _volatileCredentialsIsInitialised = true;
    return _volatileCredentials;
  };

  const set = async (credentials) => {
    if (_.isEqual(credentials, _volatileCredentials)) {
      return;
    }
    const options = {
      accessControl,
      securityLevel,
    };
    if (USE_CHUNKED) {
      const chunks = chunkString(credentials, KEYCHAIN_CHUNK_MAXSIZE);
      const chunkCount = chunks.length;
      await Keychain.setInternetCredentials(
        `${issuer}-${KEYCHAIN_CHUNK_COUNT_KEY}`,
        clientId,
        JSON.stringify(chunkCount),
        options
      );
      for (let i = 0; i < chunkCount; i++) {
        const chunk = chunks[i];
        const password = JSON.stringify(chunk);
        await Keychain.setInternetCredentials(
          `${issuer}-${i}`,
          clientId,
          password,
          options
        );
      }
    } else {
      const password = JSON.stringify(credentials);
      await Keychain.setInternetCredentials(
        issuer,
        clientId,
        password,
        options
      );
    }
    _volatileCredentials = credentials;
    _volatileCredentialsIsInitialised = true;
    return _volatileCredentials;
  };

  const reset = async () => {
    if (USE_CHUNKED) {
      const hasChunkCount = await Keychain.hasInternetCredentials(
        `${issuer}-${KEYCHAIN_CHUNK_COUNT_KEY}`
      );
      if (hasChunkCount) {
        const { password } = await Keychain.getInternetCredentials(
          `${issuer}-${KEYCHAIN_CHUNK_COUNT_KEY}`
        );
        const chunkCount = JSON.parse(password);
        if (chunkCount > 0) {
          for (let i = 0; i < chunkCount; i++) {
            await Keychain.resetInternetCredentials(`${issuer}-${i}`);
          }
        }
      }
    } else {
      await Keychain.resetInternetCredentials(issuer);
    }
    _volatileCredentials = undefined;
    _volatileCredentialsIsInitialised = true;
    return _volatileCredentials;
  };

  return {
    get,
    set,
    reset,
  };
};
