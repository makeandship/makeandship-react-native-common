/* @flow */

import { Linking } from 'react-native';
import { channel, buffers } from 'redux-saga';
import {
  apply,
  put,
  select,
  all,
  fork,
  take,
  takeEvery,
  takeLatest,
  race,
} from 'redux-saga/effects';
import type { Saga } from 'redux-saga';
import { createSelector } from 'reselect';
import produce from 'immer';
import Geolocation from '@react-native-community/geolocation';

import { getAppState, ENTER_FOREGROUND } from '../system/appState';
import { showAlert } from '../ui/alert';

export const typePrefix = 'geolocation/geolocation/';
export const START_WATCHING_POSITION = `${typePrefix}START_WATCHING_POSITION`;
export const STOP_WATCHING_POSITION = `${typePrefix}STOP_WATCHING_POSITION`;
export const REQUEST_CURRENT_POSITION = `${typePrefix}REQUEST_CURRENT_POSITION`;
export const SET_ERROR = `${typePrefix}SET_ERROR`;
export const SET_POSITION = `${typePrefix}SET_POSITION`;
export const SET_WATCHING = `${typePrefix}SET_WATCHING`;
export const SET_WATCH_ID = `${typePrefix}SET_WATCH_ID`;
export const SET_PROMPT_SHOWING = `${typePrefix}SET_PROMPT_SHOWING`;
export const PROMPT_ACTION_SETTINGS = `${typePrefix}PROMPT_ACTION_SETTINGS`;
export const PROMPT_ACTION_CANCEL = `${typePrefix}PROMPT_ACTION_CANCEL`;

const actionChan = channel(buffers.expanding(10));

const onPosition = (position) => {
  actionChan.put(setPosition(position));
};

const onError = (error) => {
  actionChan.put(setError(error));
};

export const getState = (state) => state.geolocation.geolocation;

export const getPosition = createSelector(getState, (state) => state.position);

export const getHasPosition = createSelector(
  getState,
  (state) => !!state.position
);

export const getError = createSelector(getState, (state) => state.error);

export const getHasError = createSelector(getState, (state) => !!state.error);

export const getIsRequesting = createSelector(
  getState,
  (state) => state.isRequesting
);

export const getWatching = createSelector(getState, (state) => state.watching);

export const getWatchId = createSelector(getState, (state) => state.watchId);

export const getCoords = createSelector(
  getPosition,
  (position) => position && position.coords
);

export const getLatitude = createSelector(
  getCoords,
  (coords) => coords && coords.latitude
);

export const getLongitude = createSelector(
  getCoords,
  (coords) => coords && coords.longitude
);

export const getLocation = createSelector(
  getLatitude,
  getLongitude,
  (latitude, longitude) => ({
    latitude,
    longitude,
  })
);

export const getPromptShowing = createSelector(
  getState,
  (state) => state.promptShowing
);

// Action creators

export const setPosition = (payload: any) => ({
  type: SET_POSITION,
  payload,
});
export const setError = (payload: any) => ({
  type: SET_ERROR,
  payload,
});

export const setWatching = (payload: boolean) => ({
  type: SET_WATCHING,
  payload,
});

export const setWatchId = (payload: any) => ({
  type: SET_WATCH_ID,
  payload,
});

export const startWatchingPosition = () => ({
  type: START_WATCHING_POSITION,
});

export const stopWatchingPosition = () => ({
  type: STOP_WATCHING_POSITION,
});

export const requestCurrentPosition = () => ({
  type: REQUEST_CURRENT_POSITION,
});

export const setPromptShowing = (payload: any) => ({
  type: SET_PROMPT_SHOWING,
  payload,
});

// Reducer

export type State = {
  position?: any,
  error?: any,
  watchId?: number,
  watching: boolean,
  isRequesting: boolean,
  promptShowing: boolean,
};

const initialState = {
  watching: false,
  isRequesting: false,
  promptShowing: false,
};

const reducer = (state?: State = initialState, action?: Object = {}): State =>
  produce(state, (draft) => {
    switch (action.type) {
      case REQUEST_CURRENT_POSITION:
      case START_WATCHING_POSITION:
        draft.isRequesting = true;
        return;
      case STOP_WATCHING_POSITION:
        draft.isRequesting = false;
        return;
      case SET_POSITION:
        draft.isRequesting = false;
        draft.position = action.payload;
        delete draft.error;
        return;
      case SET_ERROR:
        draft.isRequesting = false;
        delete draft.position;
        draft.error = action.payload;
        return;
      case SET_WATCHING:
        draft.watching = action.payload;
        return;
      case SET_WATCH_ID:
        draft.watchId = action.payload;
        return;
      case SET_PROMPT_SHOWING:
        draft.promptShowing = action.payload;
        return;
      default:
        return;
    }
  });

export default reducer;

// Sagas

export function* actionChanWatcher(): Saga {
  while (true) {
    const action = yield take(actionChan);
    yield put(action);
  }
}

export function* requestCurrentPositionWatcher(): Saga {
  yield takeLatest(REQUEST_CURRENT_POSITION, requestCurrentPositionWorker);
}

export function* requestCurrentPositionWorker(): Saga {
  yield apply(Geolocation, Geolocation.getCurrentPosition, [
    onPosition,
    onError,
    { enableHighAccuracy: true },
  ]);
}

export function* startWatchingPositionWatcher(): Saga {
  yield takeLatest(START_WATCHING_POSITION, startWatchingPositionWorker);
}

export function* startWatchingPositionWorker(): Saga {
  let watchId = yield select(getWatchId);
  if (watchId !== undefined) {
    yield put(stopWatchingPosition());
  }
  watchId = yield apply(Geolocation, Geolocation.watchPosition, [
    onPosition,
    onError,
    { enableHighAccuracy: true, distanceFilter: 5 },
  ]);
  yield put(setWatchId(watchId));
  yield put(setWatching(true));
}

export function* stopWatchingPositionWatcher(): Saga {
  yield takeLatest(STOP_WATCHING_POSITION, stopWatchingPositionWorker);
}

export function* stopWatchingPositionWorker(): Saga {
  const watchId = yield select(getWatchId);
  if (watchId !== undefined) {
    yield apply(Geolocation, Geolocation.clearWatch, [watchId]);
    yield put(setWatchId());
  }
  yield put(setWatching(false));
}

export function* enterForegroundWatcher(): Saga {
  yield takeLatest(ENTER_FOREGROUND, enterForegroundWorker);
}

export function* enterForegroundWorker(): Saga {
  const watching = yield select(getWatching);
  if (watching) {
    yield put(startWatchingPosition());
  }
}

// error watcher

/*
{
  "code": 1,
  "message": "User denied access to location services.",
  "PERMISSION_DENIED": 1,
  "POSITION_UNAVAILABLE": 2,
  "TIMEOUT": 3
}
*/

export function* errorWatcher(): Saga {
  // takeEvery so that the prompt showing state is always resolved
  yield takeEvery(SET_ERROR, errorWorker);
}

export function* errorWorker(): Saga {
  const error = yield select(getError);
  if (error.code === 1) {
    // Permission denied - prompt
    const appState = yield select(getAppState);
    // we receive an error if watching when entering the background - don't show it
    if (appState !== 'background') {
      const promptShowing = yield select(getPromptShowing);
      if (!promptShowing) {
        yield put(setPromptShowing(true));
        yield put(
          showAlert(
            'Error',
            'Location permissions not granted. You need to give this app access to your location in Settings',
            [
              { text: 'Cancel', type: PROMPT_ACTION_CANCEL },
              { text: 'Settings', type: PROMPT_ACTION_SETTINGS },
            ]
          )
        );
        const { settings } = yield race({
          cancel: take(PROMPT_ACTION_CANCEL),
          settings: take(PROMPT_ACTION_SETTINGS),
        });
        yield put(setPromptShowing(false));
        if (settings) {
          yield apply(Linking, Linking.openURL, ['app-settings:']);
        }
      }
    }
  } else if (error.code === 2) {
    // Position unavailable
  } else if (error.code === 3) {
    // Timeout
  }
}

export function* geolocationGeolocationSaga(): Saga {
  const sagas = [
    actionChanWatcher,
    enterForegroundWatcher,
    requestCurrentPositionWatcher,
    startWatchingPositionWatcher,
    stopWatchingPositionWatcher,
    errorWatcher,
  ];
  yield all(sagas.map((saga) => fork(saga)));
}
