/* @flow */

import { put, select, all, fork, takeEvery } from 'redux-saga/effects';
import type { Saga } from 'redux-saga';
import { createSelector } from 'reselect';
import produce from 'immer';
import _ from 'lodash';
import getDistance from 'geolib/es/getDistance';
import getCenterOfBounds from 'geolib/es/getCenterOfBounds';
import isPointInPolygon from 'geolib/es/isPointInPolygon';

import { waitForChange } from 'makeandship-js-common/src/modules/utils';

import { getLocation } from './geolocation';

export const REGION_TYPE_CIRCULAR = 'REGION_TYPE_CIRCULAR';
export const REGION_TYPE_POLYGON = 'REGION_TYPE_POLYGON';

export const typePrefix = 'geolocation/regions/';
export const ADD_REGION = `${typePrefix}ADD_REGION`;
export const REMOVE_REGION = `${typePrefix}REMOVE_REGION`;
export const UPDATE_REGION = `${typePrefix}UPDATE_REGION`;
export const RESET = `${typePrefix}RESET`;

export const getState = (state) => state.geolocation.regions;

export const getAllRegions = createSelector(getState, (state) => state);

export const getRegion = (state: any, key: string) =>
  _.get(getState(state), key);

export const getIsInsideRegion = (state: any, key: string) =>
  !!_.get(getRegion(state, key), 'inside');

export const getIsInsideAnyRegion = createSelector(getAllRegions, (regions) => {
  const keys = Object.keys(regions);
  for (let i = 0; i < keys.length; i++) {
    const key = keys[i];
    const region = regions[key];
    if (region.inside) {
      return true;
    }
  }
  return false;
});

// Action creators

export const addCircularRegion = (
  key: string,
  centre: any,
  radius: number
) => ({
  type: ADD_REGION,
  payload: { type: REGION_TYPE_CIRCULAR, key, centre, radius },
});

export const addPolygonRegion = (key: string, polygon: Array<*>) => ({
  type: ADD_REGION,
  payload: { type: REGION_TYPE_POLYGON, key, polygon },
});

export const removeRegion = (payload: string) => ({
  type: REMOVE_REGION,
  payload,
});

export const updateRegion = (key: string, data: any) => ({
  type: UPDATE_REGION,
  payload: {
    key,
    data,
  },
});

export const reset = () => ({
  type: RESET,
});

// Reducer

export type State = {};

const initialState = {};

const reducer = (state?: State = initialState, action?: Object = {}): State =>
  produce(state, (draft) => {
    switch (action.type) {
      case ADD_REGION:
        {
          const { key, ...rest } = action.payload;
          draft[key] = rest;
        }
        return;
      case UPDATE_REGION:
        {
          const { key, data } = action.payload;
          Object.assign(draft[key], data);
        }
        return;
      case REMOVE_REGION:
        delete draft[action.payload];
        return;
      case RESET:
        return initialState;
      default:
        return;
    }
  });

export default reducer;

// Sagas

export function* init(): Saga {
  yield fork(updateRegionsWorker);
}

export function* regionChangeWatcher(): Saga {
  yield takeEvery(ADD_REGION, updateRegionsWorker);
}

export function* locationChangeWatcher(): Saga {
  while (true) {
    yield waitForChange(getLocation);
    yield fork(updateRegionsWorker);
  }
}

export function* updateRegionsWorker(): Saga {
  const { latitude, longitude } = yield select(getLocation);
  if (latitude === undefined || longitude === undefined) {
    return;
  }
  const regions = yield select(getAllRegions);
  const keys = Object.keys(regions);
  const point = { latitude, longitude };
  for (let i = 0; i < keys.length; i++) {
    const key = keys[i];
    const region = regions[key];
    if (region.type === REGION_TYPE_CIRCULAR) {
      const distance = getDistance(point, region.centre);
      const inside = distance <= region.radius;
      yield put(updateRegion(key, { inside, distance }));
    } else {
      const centre = getCenterOfBounds(region.polygon);
      const distance = getDistance(point, centre);
      const inside = isPointInPolygon(point, region.polygon);
      yield put(updateRegion(key, { inside, distance }));
    }
  }
}

export function* regionsSaga(): Saga {
  const sagas = [init, regionChangeWatcher, locationChangeWatcher];
  yield all(sagas.map((saga) => fork(saga)));
}
