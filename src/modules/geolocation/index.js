/* @flow */

import { combineReducers } from 'redux';
import { all, fork } from 'redux-saga/effects';

import geolocation, { geolocationGeolocationSaga } from './geolocation';
import regions, { regionsSaga } from './regions';

const reducer = combineReducers({
  geolocation,
  regions,
});

export default reducer;

const sagas = [geolocationGeolocationSaga, regionsSaga];

export function* geolocationSaga(): Generator<*, *, *> {
  yield all(sagas.map((saga) => fork(saga)));
}
