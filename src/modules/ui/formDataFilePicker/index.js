/* @flow */

import {
  race,
  all,
  fork,
  take,
  put,
  takeEvery,
  select,
} from 'redux-saga/effects';
import type { Saga } from 'redux-saga';

import {
  getFormDataForKeyPath,
  setFormDataForKeyPath,
} from 'makeandship-js-common/src/modules/form';
import {
  isDelete,
  DELETE,
} from 'makeandship-js-common/src/components/ui/form/fields/FileInputBinary/utils';
import { isPlainObject, isFile } from 'makeandship-js-common/src/utils/is';

import {
  filePickerPickFile,
  FILE_PICKER_PICK_SUCCESS,
  FILE_PICKER_PICK_FAILURE,
  FILE_PICKER_PICK_CANCEL,
  FILE_PICKER_PICK_REMOVE,
} from '../filePicker';

import {
  imagePickerPickWithPrompt,
  IMAGE_PICKER_PICK_CANCEL,
  IMAGE_PICKER_PICK_FAILURE,
  IMAGE_PICKER_PICK_SUCCESS,
  IMAGE_PICKER_PICK_REMOVE,
  IMAGE_PICKER_VIEW,
} from '../imagePicker';

export const getUri = (value: any) => {
  const uri =
    isPlainObject(value) && value.url
      ? value.url
      : isFile(value) && value.uri
      ? value.uri
      : value;
  return uri;
};

export const typePrefix = 'ui/formDataFilePicker/';
export const FORM_DATA_PICK_FILE = `${typePrefix}FORM_DATA_PICK_FILE`;

// Action creators

export const formDataPickFile = (payload: any) => ({
  type: FORM_DATA_PICK_FILE,
  payload,
});

// Sagas

export function* pickFileWatcher(): Saga {
  yield takeEvery(FORM_DATA_PICK_FILE, pickFileWorker);
}

export function* pickFileWorker(action: any): Saga {
  const {
    formKey,
    keyPath,
    originalValue,
    allowDocuments = true,
    onChange,
  } = action.payload;
  const state = yield select();
  const value = getFormDataForKeyPath(state, formKey, keyPath);
  const isDeleted = isDelete(value);
  const hasValue = !!value;
  const showRemove = hasValue
    ? isDeleted
      ? 'Cancel remove'
      : 'Remove file'
    : false;

  const showView = hasValue ? (isDeleted ? false : 'View file') : false;
  const uri = getUri(value);
  console.log({ hasValue, isDeleted, showRemove, showView, uri });

  let outcomes;
  if (allowDocuments) {
    yield put(filePickerPickFile({ showRemove }));
    outcomes = {
      success: take(FILE_PICKER_PICK_SUCCESS),
      failure: take(FILE_PICKER_PICK_FAILURE),
      cancel: take(FILE_PICKER_PICK_CANCEL),
      remove: take(FILE_PICKER_PICK_REMOVE),
    };
  } else {
    yield put(imagePickerPickWithPrompt({ showRemove, showView, uri }));
    outcomes = {
      view: take(IMAGE_PICKER_VIEW),
      success: take(IMAGE_PICKER_PICK_SUCCESS),
      failure: take(IMAGE_PICKER_PICK_FAILURE),
      cancel: take(IMAGE_PICKER_PICK_CANCEL),
      remove: take(IMAGE_PICKER_PICK_REMOVE),
    };
  }
  const { success, remove } = yield race(outcomes);
  if (success && success.payload) {
    const { type, name, uri, size } = success.payload;
    const value = { type, name, uri, size };
    if (onChange) {
      onChange(value);
    }
    yield put(setFormDataForKeyPath(formKey, keyPath, value));
  } else if (remove) {
    if (isDeleted) {
      yield put(setFormDataForKeyPath(formKey, keyPath, originalValue));
    } else {
      yield put(setFormDataForKeyPath(formKey, keyPath, DELETE));
    }
  }
}

export function* formDataFilePickerSaga(): Saga {
  const sagas = [pickFileWatcher];
  yield all(sagas.map((saga) => fork(saga)));
}
