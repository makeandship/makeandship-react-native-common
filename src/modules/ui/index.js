/* @flow */

import { combineReducers } from 'redux';
import { all, fork } from 'redux-saga/effects';

import { actionSheetSaga } from './actionSheet';
import { alertSaga } from './alert';
import dateTimePicker, { dateTimePickerSaga } from './dateTimePicker';
import { imagePickerSaga } from './imagePicker';
import { filePickerSaga } from './filePicker';
import { formDataFilePickerSaga } from './formDataFilePicker';
import {
  reducer as reactNavigation,
  saga as reactNavigationSaga,
} from './reactNavigation';

const reducer = combineReducers({
  dateTimePicker,
  reactNavigation,
});

export default reducer;

const sagas = [
  actionSheetSaga,
  alertSaga,
  dateTimePickerSaga,
  imagePickerSaga,
  filePickerSaga,
  formDataFilePickerSaga,
  reactNavigationSaga,
];

export function* uiSaga(): Generator<*, *, *> {
  yield all(sagas.map((saga) => fork(saga)));
}
