/* @flow */

/**
 * Saga wrapper around React Native's ActionSheetIOS, with an Alert-based
 * fallback for Android.
 *
 *   yield put(showActionSheet(
 *     'Title',
 *     [
 *       {
 *         text: 'Option 1',
 *         type: 'OPTION_1_PRESS',
 *         style: 'default;
 *       },
 *       {
 *         text: 'Option 2',
 *         type: 'OPTION_2_PRESS',
 *         style: 'destructive;
 *       }
 *     ]
 *   ))
 */

import { channel, buffers } from 'redux-saga';
import { all, call, fork, take, put, takeLatest } from 'redux-saga/effects';
import { ActionSheetIOS } from 'react-native';
import type { Saga } from 'redux-saga';

import { IS_ANDROID } from '../../../constants';

import { showAlert, ALERT_DISMISSED } from '../alert';

export const typePrefix = 'ui/action-sheet/';
export const ACTION_SHEET_SHOW = `${typePrefix}ACTION_SHEET_SHOW`;
export const ACTION_SHEET_DISMISSED = `${typePrefix}ACTION_SHEET_DISMISSED`;

// basic convenience action types
export const ACTION_SHEET_OK = `${typePrefix}ACTION_SHEET_OK`;
export const ACTION_SHEET_CANCEL = `${typePrefix}ACTION_SHEET_CANCEL`;
export const ACTION_SHEET_BUTTON_INDEX = `${typePrefix}ACTION_SHEET_BUTTON_INDEX`;

const buttonPressChan = channel(buffers.expanding(10));

// pass actions from buttonPressChan into main sagas

export function* buttonPressChanWatcher(): Saga {
  while (true) {
    const action = yield take(buttonPressChan);
    yield put(action);
    yield put({ type: ACTION_SHEET_DISMISSED });
  }
}

// Action creators

export const showActionSheet = (title, buttons) => ({
  type: ACTION_SHEET_SHOW,
  payload: {
    title,
    buttons,
  },
});

// Sagas

export function* showWatcher(): Saga {
  yield takeLatest(ACTION_SHEET_SHOW, showWorker);
}

// button style 'default', 'cancel' or 'destructive'
export function* showWorker(action): Saga {
  const { title, buttons } = action.payload;
  const useAlertFallback = IS_ANDROID;
  if (useAlertFallback) {
    yield put(showAlert(title, null, buttons));
    yield take(ALERT_DISMISSED);
    yield put({ type: ACTION_SHEET_DISMISSED });
  } else {
    const cancelButtonIndex = buttons.findIndex(
      (button) => button.style === 'cancel'
    );
    const destructiveButtonIndex = buttons.findIndex(
      (button) => button.style === 'destructive'
    );
    const options = {
      options: buttons.map((button) => button.text),
      cancelButtonIndex,
      destructiveButtonIndex,
    };
    if (title) {
      options.title = title;
    }
    yield call(ActionSheetIOS.showActionSheetWithOptions, options, (index) => {
      if (buttons[index].type) {
        buttonPressChan.put({
          type: buttons[index].type,
          payload: index,
        });
      } else {
        buttonPressChan.put({
          type: ACTION_SHEET_BUTTON_INDEX,
          payload: index,
        });
      }
    });
  }
}
const sagas = [showWatcher, buttonPressChanWatcher];

export function* actionSheetSaga(): Saga {
  yield all(sagas.map((saga) => fork(saga)));
}
