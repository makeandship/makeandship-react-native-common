/* @flow */

import { all, fork, put, takeLatest } from 'redux-saga/effects';
import { createSelector } from 'reselect';

export const typePrefix = 'ui/dateTimePicker/';
export const DATE_TIME_PICKER_SET_SHOWING = `${typePrefix}DATE_TIME_PICKER_SET_SHOWING`;
export const DATE_TIME_PICKER_SET_OPTIONS = `${typePrefix}DATE_TIME_PICKER_SET_OPTIONS`;
export const DATE_TIME_PICKER_SHOW = `${typePrefix}DATE_TIME_PICKER_SHOW`;
export const DATE_TIME_PICKER_CONFIRM = `${typePrefix}DATE_TIME_PICKER_CONFIRM`;
export const DATE_TIME_PICKER_CANCEL = `${typePrefix}DATE_TIME_PICKER_CANCEL`;
export const DATE_TIME_PICKER_RESET = `${typePrefix}DATE_TIME_PICKER_RESET`;

export const getState = (state: any) => state.ui.dateTimePicker;

export const getDateTimePickerIsVisible = createSelector(
  getState,
  (state) => state.isVisible
);

export const getDateTimePickerOptions = createSelector(
  getState,
  (state) => state.options
);

// Action creators

export const dateTimePickerSetShowing = (payload: boolean) => ({
  type: DATE_TIME_PICKER_SET_SHOWING,
  payload,
});

export const dateTimePickerSetOptions = (payload: any) => ({
  type: DATE_TIME_PICKER_SET_OPTIONS,
  payload,
});

// pass either Date object or options: https://github.com/mmazzarolo/react-native-modal-datetime-picker

export const dateTimePickerShow = (payload: any) => ({
  type: DATE_TIME_PICKER_SHOW,
  payload,
});

export const dateTimePickerConfirm = (payload: any) => ({
  type: DATE_TIME_PICKER_CONFIRM,
  payload,
});

export const dateTimePickerCancel = () => ({
  type: DATE_TIME_PICKER_CANCEL,
});

// Reducer

const initialState = {
  isVisible: false,
  options: {},
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case DATE_TIME_PICKER_SET_SHOWING:
      return {
        ...state,
        isVisible: action.payload,
      };
    case DATE_TIME_PICKER_SET_OPTIONS:
      return {
        ...state,
        options: action.payload,
      };
    case DATE_TIME_PICKER_RESET:
      return initialState;
    default:
      return state;
  }
}

// Sagas

export function* showWatcher(): any {
  yield takeLatest(DATE_TIME_PICKER_SHOW, function* (action) {
    if (action.payload) {
      if (action.payload instanceof Date) {
        yield put(dateTimePickerSetOptions({ date: action.payload }));
      } else {
        yield put(dateTimePickerSetOptions(action.payload));
      }
    }
    yield put(dateTimePickerSetShowing(true));
  });
}

export function* confirmWatcher(): any {
  yield takeLatest(DATE_TIME_PICKER_CONFIRM, function* () {
    yield put(dateTimePickerSetShowing(false));
  });
}

export function* cancelWatcher(): any {
  yield takeLatest(DATE_TIME_PICKER_CANCEL, function* () {
    yield put(dateTimePickerSetShowing(false));
  });
}

const sagas = [showWatcher, confirmWatcher, cancelWatcher];

export function* dateTimePickerSaga() {
  yield all(sagas.map((saga) => fork(saga)));
}
