/* @flow */

import {
  race,
  all,
  fork,
  take,
  put,
  takeEvery,
  call,
  takeLatest,
} from 'redux-saga/effects';
import type { Saga } from 'redux-saga';
import DocumentPicker from 'react-native-document-picker';
import produce from 'immer';

import extensionToMimetype from 'makeandship-js-common/src/utils/extensionToMimetype';
import {
  getFileExtension,
  setFileExtension,
} from 'makeandship-js-common/src/utils/extension';
import { isString } from 'makeandship-js-common/src/utils/is';

import * as actionSheet from '../actionSheet';
import {
  IMAGE_PICKER_PICK_FROM_CAMERA,
  IMAGE_PICKER_PICK_FROM_LIBRARY,
  IMAGE_PICKER_PICK_SUCCESS,
  IMAGE_PICKER_PICK_FAILURE,
  IMAGE_PICKER_PICK_CANCEL,
} from '../imagePicker';

export const typePrefix = 'ui/filePicker/';
export const FILE_PICKER_PICK_FILE = `${typePrefix}FILE_PICKER_PICK_FILE`;

export const FILE_PICKER_PICK_DOCUMENT = `${typePrefix}FILE_PICKER_PICK_DOCUMENT`;

export const FILE_PICKER_PICK_SUCCESS = `${typePrefix}FILE_PICKER_PICK_SUCCESS`;
export const FILE_PICKER_PICK_FAILURE = `${typePrefix}FILE_PICKER_PICK_FAILURE`;
export const FILE_PICKER_PICK_CANCEL = `${typePrefix}FILE_PICKER_PICK_CANCEL`;
export const FILE_PICKER_PICK_REMOVE = `${typePrefix}FILE_PICKER_PICK_REMOVE`;

export const FILE_PICKER_OPTION_USE_CAMERA = `${typePrefix}FILE_PICKER_OPTION_USE_CAMERA`;
export const FILE_PICKER_OPTION_CHOOSE_FROM_LIBRARY = `${typePrefix}FILE_PICKER_OPTION_CHOOSE_FROM_LIBRARY`;
export const FILE_PICKER_OPTION_CHOOSE_DOCUMENT = `${typePrefix}FILE_PICKER_OPTION_CHOOSE_DOCUMENT`;
export const FILE_PICKER_OPTION_REMOVE = `${typePrefix}FILE_PICKER_OPTION_REMOVE`;

// Action creators

export const filePickerPickFile = (payload) => ({
  type: FILE_PICKER_PICK_FILE,
  payload,
});

export const filePickerPickDocument = (payload) => ({
  type: FILE_PICKER_PICK_DOCUMENT,
  payload,
});

// Sagas

export function* pickFileWatcher(): Saga {
  yield takeEvery(FILE_PICKER_PICK_FILE, pickFileWorker);
}

export function* pickFileWorker(action): Saga {
  const { documentOptions, photoOptions, showRemove } = action.payload || {};
  const buttons = [
    {
      text: 'Take photo',
      type: FILE_PICKER_OPTION_USE_CAMERA,
    },
    {
      text: 'Choose from photos',
      type: FILE_PICKER_OPTION_CHOOSE_FROM_LIBRARY,
    },
    {
      text: 'Browse documents',
      type: FILE_PICKER_OPTION_CHOOSE_DOCUMENT,
    },
  ];
  if (showRemove) {
    buttons.push({
      text: isString(showRemove) ? showRemove : 'Remove file',
      type: FILE_PICKER_OPTION_REMOVE,
      destructive: true,
    });
  }
  buttons.push({
    text: 'Cancel',
    type: actionSheet.ACTION_SHEET_CANCEL,
    style: 'cancel',
  });
  yield put(actionSheet.showActionSheet('Choose file', buttons));
  const {
    useCamera,
    chooseFromLibrary,
    chooseDocument,
    cancel,
    remove,
  } = yield race({
    useCamera: take(FILE_PICKER_OPTION_USE_CAMERA),
    chooseFromLibrary: take(FILE_PICKER_OPTION_CHOOSE_FROM_LIBRARY),
    chooseDocument: take(FILE_PICKER_OPTION_CHOOSE_DOCUMENT),
    remove: take(FILE_PICKER_OPTION_REMOVE),
    cancel: take(actionSheet.ACTION_SHEET_CANCEL),
  });
  if (useCamera) {
    yield put({
      type: IMAGE_PICKER_PICK_FROM_CAMERA,
      payload: photoOptions,
    });
  } else if (chooseFromLibrary) {
    yield put({
      type: IMAGE_PICKER_PICK_FROM_LIBRARY,
      payload: photoOptions,
    });
  } else if (chooseDocument) {
    yield put(filePickerPickDocument(documentOptions));
  } else if (remove) {
    yield put({ type: FILE_PICKER_PICK_REMOVE });
  } else if (cancel) {
    yield put({ type: FILE_PICKER_PICK_CANCEL });
  }
}

export function* pickDocumentWatcher(): Saga {
  yield takeLatest(FILE_PICKER_PICK_DOCUMENT, pickDocumentWorker);
}

export function* pickDocumentWorker(action: any): Saga {
  try {
    const documentOptions = action.payload;
    const response = yield call(showDocumentPicker, documentOptions);
    if (response) {
      // shape the response such that it will be uploaded as a binary file by React Native
      const shapedResponse = produce(response, (draft) => {
        // sometimes the name extension differs from the uri and type, e.g. HEIC images
        // so we set it explicity it here
        const { uri, name } = draft;
        const utiExtension = getFileExtension(uri);
        draft.type = extensionToMimetype[utiExtension];
        draft.name = setFileExtension(name, utiExtension);
      });
      yield put({ type: FILE_PICKER_PICK_SUCCESS, payload: shapedResponse });
    } else {
      yield put({ type: FILE_PICKER_PICK_CANCEL });
    }
  } catch (error) {
    if (DocumentPicker.isCancel(error)) {
      yield put({ type: FILE_PICKER_PICK_CANCEL });
    } else {
      yield put({ type: FILE_PICKER_PICK_FAILURE, payload: error });
    }
  }
}

export function* imagePickerWatcher(): Saga {
  yield takeEvery(IMAGE_PICKER_PICK_SUCCESS, function* (action) {
    yield put({ type: FILE_PICKER_PICK_SUCCESS, payload: action.payload });
  });
  yield takeEvery(IMAGE_PICKER_PICK_FAILURE, function* (action) {
    yield put({ type: FILE_PICKER_PICK_FAILURE, payload: action.payload });
  });
  yield takeEvery(IMAGE_PICKER_PICK_CANCEL, function* () {
    yield put({ type: FILE_PICKER_PICK_CANCEL });
  });
}

const DEFAULT_OPTIONS = {
  type: [
    DocumentPicker.types.images,
    DocumentPicker.types.video,
    DocumentPicker.types.pdf,
    DocumentPicker.types.allFiles,
  ],
};

export const showDocumentPicker = async (options: any = DEFAULT_OPTIONS) => {
  return await DocumentPicker.pick(options);
};

export function* filePickerSaga(): Saga {
  const sagas = [pickFileWatcher, pickDocumentWatcher, imagePickerWatcher];
  yield all(sagas.map((saga) => fork(saga)));
}
