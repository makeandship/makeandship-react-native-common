/* @flow */

import {
  call,
  take,
  all,
  fork,
  put,
  race,
  takeLatest,
} from 'redux-saga/effects';
import * as ImagePicker from 'react-native-image-picker';
import produce from 'immer';

import { isString } from 'makeandship-js-common/src/utils/is';
import {
  getFileExtension,
  setFileExtension,
} from 'makeandship-js-common/src/utils/extension';
import extensionToMimetype from 'makeandship-js-common/src/utils/extensionToMimetype';
import { actions as reactNavigationActions } from '../reactNavigation';

import { showAlert } from '../alert';
import * as actionSheet from '../actionSheet';

export const typePrefix = 'ui/imagePicker/';

export const IMAGE_PICKER_PICK_WITH_PROMPT = `${typePrefix}IMAGE_PICKER_PICK_WITH_PROMPT`;

export const IMAGE_PICKER_PICK_FROM_CAMERA = `${typePrefix}IMAGE_PICKER_PICK_FROM_CAMERA`;
export const IMAGE_PICKER_PICK_FROM_LIBRARY = `${typePrefix}IMAGE_PICKER_PICK_FROM_LIBRARY`;
export const IMAGE_PICKER_PICK_SUCCESS = `${typePrefix}IMAGE_PICKER_PICK_SUCCESS`;
export const IMAGE_PICKER_PICK_FAILURE = `${typePrefix}IMAGE_PICKER_PICK_FAILURE`;
export const IMAGE_PICKER_PICK_CANCEL = `${typePrefix}IMAGE_PICKER_PICK_CANCEL`;
export const IMAGE_PICKER_PICK_REMOVE = `${typePrefix}IMAGE_PICKER_PICK_REMOVE`;

export const IMAGE_PICKER_OPTION_USE_CAMERA = `${typePrefix}IMAGE_PICKER_OPTION_USE_CAMERA`;
export const IMAGE_PICKER_OPTION_CHOOSE_FROM_LIBRARY = `${typePrefix}IMAGE_PICKER_OPTION_CHOOSE_FROM_LIBRARY`;
export const IMAGE_PICKER_OPTION_REMOVE = `${typePrefix}IMAGE_PICKER_OPTION_REMOVE`;
export const IMAGE_PICKER_OPTION_VIEW = `${typePrefix}IMAGE_PICKER_OPTION_VIEW`;

export const IMAGE_PICKER_VIEW = `${typePrefix}IMAGE_PICKER_VIEW`;
export const IMAGE_PICKER_VIEW_DONE = `${typePrefix}IMAGE_PICKER_VIEW_DONE`;

// Action creators

export const imagePickerPickWithPrompt = (payload) => ({
  type: IMAGE_PICKER_PICK_WITH_PROMPT,
  payload,
});

export const imagePickerPickFromCamera = () => ({
  type: IMAGE_PICKER_PICK_FROM_CAMERA,
});

export const imagePickerPickFromLibrary = () => ({
  type: IMAGE_PICKER_PICK_FROM_LIBRARY,
});

export const imagePickerView = (payload) => ({
  type: IMAGE_PICKER_VIEW,
  payload,
});

export const imagePickerViewDone = () => ({
  type: IMAGE_PICKER_VIEW_DONE,
});

// Sagas

export function* pickFailureWatcher(): Saga {
  yield takeLatest(IMAGE_PICKER_PICK_FAILURE, pickFailureWorker);
}

export function* pickFailureWorker(action): Saga {
  const error = action.payload;
  let message = isString(error) ? error : 'Unknown error';
  switch (error) {
    case 'camera_unavailable':
      message = 'Camera not available. You need a camera to use this feature';
      break;
    case 'permission':
      message =
        'Camera permissions not granted. You need to give this app access to your camera in Settings to use this feature';
      break;
  }
  yield put(showAlert('Error', message, [{ text: 'OK' }]));
}

export function* pickWithPromptWatcher(): Saga {
  yield takeLatest(IMAGE_PICKER_PICK_WITH_PROMPT, pickWithPromptWorker);
}

export function* pickWithPromptWorker(action): Saga {
  const buttons = [];
  const { showRemove, showView, uri } = action.payload;
  if (showView) {
    buttons.push({
      text: 'View photo',
      type: IMAGE_PICKER_OPTION_VIEW,
    });
  }
  buttons.push({
    text: 'Take photo',
    type: IMAGE_PICKER_OPTION_USE_CAMERA,
  });
  buttons.push({
    text: 'Choose from library',
    type: IMAGE_PICKER_OPTION_CHOOSE_FROM_LIBRARY,
  });
  if (showRemove) {
    buttons.push({
      text: 'Remove photo',
      type: IMAGE_PICKER_OPTION_REMOVE,
      style: 'destructive',
    });
  }
  buttons.push({
    text: 'Cancel',
    type: actionSheet.ACTION_SHEET_CANCEL,
    style: 'cancel',
  });
  yield put(
    actionSheet.showActionSheet(showView ? 'Photo' : 'Choose photo', buttons)
  );
  const { view, useCamera, chooseFromLibrary, cancel, remove } = yield race({
    view: take(IMAGE_PICKER_OPTION_VIEW),
    useCamera: take(IMAGE_PICKER_OPTION_USE_CAMERA),
    chooseFromLibrary: take(IMAGE_PICKER_OPTION_CHOOSE_FROM_LIBRARY),
    remove: take(IMAGE_PICKER_OPTION_REMOVE),
    cancel: take(actionSheet.ACTION_SHEET_CANCEL),
  });
  if (cancel) {
    yield put({ type: IMAGE_PICKER_PICK_CANCEL });
  } else if (useCamera) {
    yield put({ type: IMAGE_PICKER_PICK_FROM_CAMERA });
  } else if (view) {
    yield put(imagePickerView({ uri }));
  } else if (chooseFromLibrary) {
    yield put({ type: IMAGE_PICKER_PICK_FROM_LIBRARY });
  } else if (remove) {
    yield put({ type: IMAGE_PICKER_PICK_REMOVE });
  }
}

export function* pickFromCameraWatcher(): Saga {
  yield takeLatest(IMAGE_PICKER_PICK_FROM_CAMERA, pickFromCameraWorker);
}

export function* pickFromCameraWorker(): Saga {
  try {
    const response = yield call(launchCamera);
    if (response) {
      const shapedResponse = shapeResponse(response);
      yield put({ type: IMAGE_PICKER_PICK_SUCCESS, payload: shapedResponse });
    } else {
      yield put({ type: IMAGE_PICKER_PICK_CANCEL });
    }
  } catch (error) {
    yield put({ type: IMAGE_PICKER_PICK_FAILURE, payload: error });
  }
}

export function* pickFromLibraryWatcher(): Saga {
  yield takeLatest(IMAGE_PICKER_PICK_FROM_LIBRARY, pickFromLibraryWorker);
}

export function* pickFromLibraryWorker(): Saga {
  try {
    const response = yield call(launchImageLibrary);
    if (response) {
      const shapedResponse = shapeResponse(response);
      yield put({ type: IMAGE_PICKER_PICK_SUCCESS, payload: shapedResponse });
    } else {
      yield put({ type: IMAGE_PICKER_PICK_CANCEL });
    }
  } catch (error) {
    yield put({ type: IMAGE_PICKER_PICK_FAILURE, payload: error });
  }
}

export const DEFAULT_OPTIONS = {
  mediaType: 'photo',
  quality: 0.8,
  maxWidth: 1280,
  maxHeight: 1280,
};

// shape the response such that it will be uploaded as a binary file by React Native
export const shapeResponse = (response: any) => {
  const { fileName, uri } = response;
  return produce(response, (draft) => {
    // sometimes the name extension differs from the uri and type, e.g. HEIC images
    // so we set it explicity it here
    const utiExtension = getFileExtension(uri);
    draft.type = extensionToMimetype[utiExtension];
    draft.name = setFileExtension(fileName, utiExtension);
    draft.size = draft.fileSize;
  });
};

export const showImagePicker = async (options: any = DEFAULT_OPTIONS) => {
  return new Promise((resolve, reject) => {
    ImagePicker.showImagePicker(options, (response) => {
      if (response.didCancel) {
        resolve();
      } else if (response.error) {
        reject(response.error);
      } else {
        resolve(response);
      }
    });
  });
};

export const launchCamera = async (options: any = DEFAULT_OPTIONS) => {
  return new Promise((resolve, reject) => {
    ImagePicker.launchCamera(options, (response) => {
      if (response.didCancel) {
        resolve();
      } else if (response.errorCode) {
        reject(response.errorCode);
      } else {
        resolve(response);
      }
    });
  });
};

export const launchImageLibrary = async (options: any = DEFAULT_OPTIONS) => {
  return new Promise((resolve, reject) => {
    ImagePicker.launchImageLibrary(options, (response) => {
      if (response.didCancel) {
        resolve();
      } else if (response.errorCode) {
        reject(response.errorCode);
      } else {
        resolve(response);
      }
    });
  });
};

export function* viewWatcher(): Saga {
  yield takeLatest(IMAGE_PICKER_VIEW, viewWorker);
}

export function* viewWorker(action): Saga {
  const { uri } = action.payload;
  yield put(
    reactNavigationActions.navigate({
      name: 'PhotoInputBinaryCellImageView',
      params: {
        uri,
      },
    })
  );
}

export function* imagePickerSaga(): Saga {
  const sagas = [
    pickWithPromptWatcher,
    pickFromCameraWatcher,
    pickFromLibraryWatcher,
    pickFailureWatcher,
    viewWatcher,
  ];
  yield all(sagas.map((saga) => fork(saga)));
}
