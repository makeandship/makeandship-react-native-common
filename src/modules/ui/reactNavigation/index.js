/* @flow */

import { channel, buffers } from 'redux-saga';
import {
  all,
  fork,
  take,
  takeLatest,
  select,
  actionChannel,
  apply,
  put,
} from 'redux-saga/effects';
import type { Saga } from 'redux-saga';
import { createSelector } from 'reselect';
import { CommonActions } from '@react-navigation/native';
import _ from 'lodash';
import { createAction, createReducer } from '@reduxjs/toolkit';

import { cleanObject } from 'makeandship-js-common/src/utils/clean';

export const typePrefix = 'ui/reactNavigation/';

export const actions = {
  topLevelNavigatorRefChanged: createAction(
    `${typePrefix}TOP_LEVEL_NAVIGATOR_REF_CHANGED`
  ),
  rootStackLoaded: createAction(`${typePrefix}ROOT_STACK_LOADED`),
  navigateRoot: createAction(`${typePrefix}NAVIGATE_ROOT`),
  navigationRouteChange: createAction(`${typePrefix}NAVIGATION_ROUTE_CHANGE`),

  reset: createAction(`${typePrefix}RESET`, (...args) => ({
    payload: CommonActions.reset(...args),
  })),
  navigate: createAction(`${typePrefix}NAVIGATE`, (...args) => ({
    payload: CommonActions.navigate(...args),
  })),
  goBack: createAction(`${typePrefix}GO_BACK`, (...args) => ({
    payload: CommonActions.goBack(...args),
  })),
  setParams: createAction(`${typePrefix}SET_PARAMS`, (...args) => ({
    payload: CommonActions.setParams(...args),
  })),
};

// Selectors

const getState = (state: any) => state.ui.reactNavigation;

const getPreviousRoute = createSelector(
  getState,
  (navigation) => navigation.previousRoute
);

const getCurrentRoute = createSelector(
  getState,
  (navigation) => navigation.currentRoute
);

export const selectors = {
  getState,
  getPreviousRoute,
  getCurrentRoute,
};

// Reducer

const initialState = {
  previousRoute: undefined,
  currentRoute: undefined,
};

export const reducer = createReducer(initialState, {
  [actions.navigationRouteChange]: (state, action) => {
    Object.assign(state, action.payload);
  },
});

// set the topLevelNavigatorRef in a way that avoids sending it through redux
// since it is a large recursive Object and causes issues with debugger / logging

const navigationStateChangeChan = channel(buffers.expanding(10));

export const onNavigationStateChange = (navigationState: any) =>
  navigationStateChangeChan.put(navigationState);

const topLevelNavigatorRefChan = channel(buffers.expanding(10));

let _topLevelNavigatorRef;

export const setTopLevelNavigatorRef = (topLevelNavigatorRef: any) => {
  _topLevelNavigatorRef = topLevelNavigatorRef;
  topLevelNavigatorRefChan.put(actions.topLevelNavigatorRefChanged());
};

// pass actions from topLevelNavigatorRefChan into main sagas

export function* topLevelNavigatorRefChanWatcher(): Saga {
  // in case we were restarted
  if (_topLevelNavigatorRef) {
    yield put(actions.topLevelNavigatorRefChanged());
  }
  while (true) {
    const action = yield take(topLevelNavigatorRefChan);
    yield put(action);
  }
}

// navigate with the top-level navigator

export function* navigationWorker(): Saga {
  // buffer navigation actions until initialised
  console.log({ actions });
  const chan = yield actionChannel([
    actions.navigate,
    actions.reset,
    actions.goBack,
    actions.setParams,
  ]);
  // assume navigation might be changed more than once, and may be null
  yield takeLatest(actions.topLevelNavigatorRefChanged, function* () {
    while (_topLevelNavigatorRef) {
      try {
        const action = yield take(chan);
        const { payload } = action;
        console.log({ payload });
        yield apply(_topLevelNavigatorRef, 'dispatch', [payload]);
      } catch (error) {
        console.log('error', error);
      }
    }
  });
}

// Gets the current screen from navigation state

const getActiveRoute = (navigationState) => {
  if (!navigationState) {
    return null;
  }
  const route = navigationState.routes[navigationState.index];
  // dive into nested navigators
  if (route.state) {
    return getActiveRoute(route.state);
  }
  return route;
};

export function* stateChangeWorker(): Saga {
  while (true) {
    const navigationState = yield take(navigationStateChangeChan);
    const previousRoute = yield select(selectors.getCurrentRoute);
    const currentRouteRaw = getActiveRoute(navigationState);
    if (currentRouteRaw) {
      const currentRoute = _.omitBy(
        {
          name: currentRouteRaw.name,
          params: cleanObject(currentRouteRaw.params),
        },
        _.isNil
      );
      if (!_.isEqual(previousRoute, currentRoute)) {
        yield put(
          actions.navigationRouteChange({
            currentRoute,
            previousRoute,
          })
        );
      }
    }
  }
}

export function* saga(): Saga {
  const sagas = [
    topLevelNavigatorRefChanWatcher,
    navigationWorker,
    stateChangeWorker,
  ];
  yield all(sagas.map((saga) => fork(saga)));
}
