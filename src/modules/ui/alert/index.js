/* @flow */

/**
 * Saga wrapper around React Native's Alert. Allows triggering of alerts via
 * an action creator, the signature of which is the same as the Alert.alert
 * function. Instead of an onPress, button's should contain an action type
 * which will be dispatched when they recieve interaction. E.g.
 *
 *   yield put(showAlert(
 *     'Alert title',
 *     'Alert message',
 *     [
 *       {
 *         text: 'Option 1',
 *         type: 'OPTION_1_PRESS',
 *         style: 'default;
 *       },
 *       {
 *         text: 'Option 2',
 *         type: 'OPTION_2_PRESS',
 *         style: 'destructive;
 *       }
 *     ]
 * ))
 */

import { channel, buffers } from 'redux-saga';
import { all, call, fork, take, put, takeLatest } from 'redux-saga/effects';
import { Alert } from 'react-native';
import type { Saga } from 'redux-saga';

export const typePrefix = 'ui/alert/';
export const ALERT_SHOW = `${typePrefix}ALERT_SHOW`;
export const ALERT_DISMISSED = `${typePrefix}ALERT_DISMISSED`;

// basic convenience action types
export const ALERT_DEFAULT = `${typePrefix}ALERT_DEFAULT`;
export const ALERT_OK = `${typePrefix}ALERT_OK`;
export const ALERT_CANCEL = `${typePrefix}ALERT_CANCEL`;

const buttonPressChan = channel(buffers.expanding(10));

// pass actions from buttonPressChan into main sagas

export function* buttonPressChanWatcher(): Saga {
  while (true) {
    const action = yield take(buttonPressChan);
    yield put(action);
    yield put({ type: ALERT_DISMISSED });
  }
}

// Action creators

export const showAlert = (title, message, buttons, options) => ({
  type: ALERT_SHOW,
  payload: {
    title,
    message,
    buttons,
    options,
  },
});

// Sagas

export function* showWatcher(): Saga {
  yield takeLatest(ALERT_SHOW, showWorker);
}

// button style 'default', 'cancel' or 'destructive'
export function* showWorker(action): Saga {
  const {
    title,
    message,
    buttons,
    options = { cancelable: false },
  } = action.payload;
  yield call(
    Alert.alert,
    title,
    message,
    buttons.map(({ type, ...rest }) => ({
      onPress: () => {
        buttonPressChan.put({
          type: type || ALERT_DEFAULT,
        });
      },
      ...rest,
    })),
    options
  );
}

const sagas = [showWatcher, buttonPressChanWatcher];

export function* alertSaga() {
  yield all(sagas.map((saga) => fork(saga)));
}
