/* @flow */

const fs = require('fs');
const path = require('path');
const glob = require('glob');

const dir = path.join(__dirname, '../src/');
const files = glob.sync(`${dir}**/*.stories.js`);

let fileContents =
  '/* This file is auto-generated, see the generate-storybook script */\n\n';

files.forEach((filePath) => {
  let componentPath = filePath.substr(dir.length);
  let requirePath = `../../src/${componentPath}`;
  fileContents = fileContents + `require('${requirePath}');\n`;
});

fs.writeFileSync('storybook/stories/index.js', fileContents, 'utf8');

console.log('Written storybook/stories/index.js');
