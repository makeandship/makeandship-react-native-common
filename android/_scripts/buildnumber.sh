#!/bin/sh
# this returns the same build number as /ios/_scripts/update-build-number.sh
branch=$(expr $(git rev-parse --abbrev-ref HEAD))
buildNumber=$(expr $(git rev-list $branch --count) - $(git rev-list HEAD..$branch --count))
echo $buildNumber
